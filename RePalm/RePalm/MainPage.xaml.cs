﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using RePalm.Client.ViewModels.PalmRecognition;
using RePalm.Pages;
using Xamarin.Facebook.AppEvents;

//Facebook.AppEvents;

namespace RePalm
{
    public partial class MainPage
    {
        public MainPage()
        {

            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

        }

        public async void TestInternetConnection()
        {
            try
            {
                using (var client = new HttpClient())
                {
                  var response =  await client.GetAsync(
                          "https://google.com");
                    if(!response.IsSuccessStatusCode){
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Connection",
                    "To use this app please turn on your internet connection", "OK");
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            TestInternetConnection();
            var rootPage = Navigation.NavigationStack[0];
            if (typeof(MainPage) == rootPage.GetType()) return;
            App.Navigation.Navigation.RemovePage(rootPage);

        }
    }
}
