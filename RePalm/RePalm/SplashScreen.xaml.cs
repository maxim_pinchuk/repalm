﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashScreen 
	{
		public SplashScreen ()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		}

	    private async void DelayedNaviagition()
	    {
	        await Task.Delay(2500);
            await App.Navigation.PushAsync(new MainPage());
	    }
        protected override void OnAppearing()
	    {
            base.OnAppearing();
	        DelayedNaviagition();
        }
    }
}