﻿using Xamarin.Forms;

namespace RePalm.Models
{
    public interface IImage
    {
        ImageSource AsImageSource();
        byte[] ImageBytes { get; set; }
    }
}
