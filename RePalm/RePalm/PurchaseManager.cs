﻿using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RePalm
{
    public class PurchaseManager
    {

        public async void PurchaseSubscription(
           // Func<Task> onBeforePurchase,
            string productName,
            Func<Task> onSuccess,
            Func<Task> onPurchaseNull,
            Func<Task> onBillingNotAwailable,
            Func<InAppBillingPurchaseException, Task> OnPurchaseInAppEception,
            Func<Task> OnCommonException,
            Func<Task> onUserCancelled)
        {
            var billing = CrossInAppBilling.Current;
            try
            {
                var connected = await billing.ConnectAsync();
                if (!connected)
                {
                    App.LogFunc($"ERROR_CrossInAppBilling_Not_Connected");
                    //Couldn't connect to billing, could be offline, alert user
                    CrossInAppBilling.Dispose();
                    onBillingNotAwailable?.Invoke();
                    return;
                }
                var purchases = (await billing.GetPurchasesAsync(ItemType.Subscription)).ToList();
                var purchase = purchases.FirstOrDefault(x => x.ProductId == productName);
                if (purchase != null)
                {
                    if (purchase.State != PurchaseState.Purchased
                        || purchase.ConsumptionState == ConsumptionState.NoYetConsumed
                       )
                    {
                        await ProcessPurchase(productName, billing, onSuccess, onPurchaseNull);
                    }
                    else
                    {
                    //already purchased
                        onSuccess?.Invoke();
                    }
                }
                else{
                    await ProcessPurchase(productName, billing, onSuccess, onPurchaseNull);
                }
            }
            catch (InAppBillingPurchaseException purchaseEx)
            {
                if (purchaseEx.PurchaseError != PurchaseError.UserCancelled)
                {
                    App.LogFunc($"ERROR_InAppBillingPurchaseException_{purchaseEx.PurchaseError.ToString()}_{productName}");
                    OnPurchaseInAppEception?.Invoke(purchaseEx);
                }
                if (purchaseEx.PurchaseError == PurchaseError.GeneralError)
                {
                }
                else
                {
                    App.LogFunc($"USER_Iterrupted_purchasing_{productName}");
                    onUserCancelled?.Invoke();
                }
            }
            catch (Exception ex)
            {
                App.LogFunc($"ERROR_Common_Exception");
                OnCommonException?.Invoke();
            }
            finally
            {
                await billing.DisconnectAsync();
            }
        }

        private async Task ProcessPurchase(string productName, IInAppBilling billing, Func<Task> onSuccess, Func<Task> onPurchaseNull )
        {
            var purchase = await billing.PurchaseAsync(productName, ItemType.Subscription,
                $"apppayload{DateTime.Now}");
            if (purchase == null)
            {
                App.LogFunc($"ERROR_Purchase_{productName}_Null");
                onPurchaseNull?.Invoke();
            }
            else
            {
                App.LogFunc($"Purchase_{productName}_Success");
                onSuccess?.Invoke();
            }
        }

    }
}
