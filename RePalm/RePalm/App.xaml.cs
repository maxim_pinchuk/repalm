﻿using System;
using RePalm.Pages;
using Xamarin.Forms;
using System.Threading.Tasks;
using Plugin.InAppBilling;

namespace RePalm
{
    public partial class App : Application
    {
        public static NavigationPage Navigation = null;
        public static Func<string,bool> LogFunc;

        private async void ImageTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            var image = sender as Image;
            if (image != null)
            {

                await image.FadeTo(0.3, 150);
                await image.FadeTo(1, 150);
                await App.Navigation.PushAsync(new AboutAppPage());
            }
        }

        private async void ImageEmailTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            var image = sender as Image;
            if (image != null)
            {
                await image.FadeTo(0.3, 150);
                await image.FadeTo(1, 150);
                Device.OpenUri(new Uri("mailto:olga.kozyura@gmail.com?subject=Palm Reading Support email"));
            }
        }

        private async void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            var element = sender as VisualElement;
            if (element != null)
            {
                element.IsEnabled = false;
                await element.FadeTo(0.3, 150);
                await element.FadeTo(1, 150);
                    await App.Navigation.PopAsync();
            }
            element.IsEnabled = true;
        }

        public App()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this,false);
            // BindingContext = this;

            var page = new SplashScreen();
            Navigation = new NavigationPage(page);
            Application.Current.MainPage = Navigation;
           // MainPage = Navigation;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
