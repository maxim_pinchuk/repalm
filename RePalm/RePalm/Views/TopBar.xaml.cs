﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TopBar : Grid
    {
        // Fields
        public static readonly BindableProperty CurrentWindowTitleProperty = BindableProperty.Create("CurrentWindow", typeof(string), typeof(TopBar), null);
       

        private SemaphoreSlim thisLock = new SemaphoreSlim(1,1);

        public string CurrentWindow
        {
            get
            {
                return (string)GetValue(CurrentWindowTitleProperty);
            }
            set
            {
                SetValue(CurrentWindowTitleProperty, value);
            }
        }
        public static readonly BindableProperty PreviosWindowTitleProperty = BindableProperty.Create("PreviosWindow", typeof(string), typeof(TopBar), null);
        public string PreviosWindow
        {
            get
            {
                return (string)GetValue(PreviosWindowTitleProperty);
            }
            set
            {
                SetValue(PreviosWindowTitleProperty, value);
            }
        }

        public TopBar ()
		{
			InitializeComponent ();
		}

        private async void TopBarTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            var element = sender as VisualElement;
            if (element != null)
            {
                element.IsEnabled = false;
                await element.FadeTo(0.3, 150);
                await element.FadeTo(1, 150);
                if (Navigation.NavigationStack.Any())
                {
                    await thisLock.WaitAsync();
                    try
                    {
                        await App.Navigation.PopToRootAsync();
                    }
                    catch (Exception exception)
                    {
                        var x = exception;
                        //System.Console.WriteLine(exception);
                        //   throw;
                    }
                    finally
                    {
                        thisLock.Release();
                    }
                }
                element.IsEnabled = true;
            }
        }   
    }
}