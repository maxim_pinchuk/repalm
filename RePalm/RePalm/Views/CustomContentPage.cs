﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RePalm.Views
{
    public class CustomContentPage:ContentPage
    {
        public static readonly BindableProperty CurrentWindowTitleProperty = BindableProperty.Create("CurrentWindow", typeof(string), typeof(CustomContentPage), null);
        public static readonly BindableProperty PreviosWindowTitleProperty = BindableProperty.Create("PreviosWindow", typeof(string), typeof(CustomContentPage), null);

        public string CurrentWindow
        {
            get
            {
                return (string)GetValue(CurrentWindowTitleProperty);
            }
            set
            {
                SetValue(CurrentWindowTitleProperty, value);
            }
        }

        public string PreviosWindow
        {
            get
            {
                return (string)GetValue(PreviosWindowTitleProperty);
            }
            set
            {
                SetValue(PreviosWindowTitleProperty, value);
            }
        }

        public CustomContentPage(){
            
        }


    }
}
