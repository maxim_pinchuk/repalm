﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.Support.V4.App;
using Newtonsoft.Json;
using RePalm.Models;
using RePalm.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

using RePalm.Client.ViewModels.PalmRecognition;

namespace RePalm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Capture 
    {
        CameraPreview preview_ = null;
        // Fields
        public static readonly BindableProperty ImageOverlayProperty = BindableProperty.Create("ImageOverlay", typeof(string), typeof(Capture), null);

        public static readonly BindableProperty NextPageProperty =
            BindableProperty.Create("NextPage", typeof(Type), typeof(Capture), null);



        public string ImageOverlay
        {
            get
            {
                return (string)GetValue(ImageOverlayProperty);
            }
            set
            {
                SetValue(ImageOverlayProperty, value);
            }
        }

        public Type NextPage
        {
            get => (Type)GetValue(NextPageProperty);
            set => SetValue(NextPageProperty, value);
        }
        public PalmSnapShotViewModel ViewModel { get; }
        public PalmistryViewModel NextPageViewModel { get; internal set; }

        public Capture(PalmSnapShotViewModel viewModel)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            // The CameraPreview have to be created in the code behind.
            // The instance of camera preview uses platform-specified modules and it causes crash if it is defined in XAML.
            preview_ = new CameraPreview();
            preview_.PictureTaken += preview__PictureTaken;
            GridCameraPreview.Children.Add(preview_);
            ViewModel = viewModel;
        }


        async void preview__PictureTaken(object sender, PictureTakenEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                // create view image page
                //var page = new ViewImagePage();
                
                var source = e.Image.ImageBytes;
                this.ViewModel.SnapShot = source;
                //page.BindingContext = vm;
                // push view image page
                if (Activator.CreateInstance(NextPage, NextPageViewModel) is ContentPage contentPage)
                {
                    await App.Navigation.PushAsync(contentPage);
                    var lastPage = Navigation.NavigationStack.Last();
                    var index = Navigation.NavigationStack.IndexOf(lastPage);
                    var prelastPage = Navigation.NavigationStack[index - 1];
                    Navigation.RemovePage(prelastPage);
                }
            });
        }

        void OnCaptureButtonClicked(object sender, EventArgs e)
        {
            preview_.TakePicture();
        }
    }
}