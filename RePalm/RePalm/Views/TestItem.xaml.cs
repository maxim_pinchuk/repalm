﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestItem : ContentView
    { 
        // Fields
        public static readonly BindableProperty TitleProperty = BindableProperty.Create("Title", typeof(string), typeof(TileButton), null);
        public static readonly BindableProperty DirectToPageProperty = BindableProperty.Create("DirectToPage", typeof(Type), typeof(TileButton), null);


        public static readonly BindableProperty SubTitleProperty = BindableProperty.Create("SubTitle", typeof(string), typeof(TileButton), null);

        // Properties
        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }
        public string SubTitle
        {
            get => (string)GetValue(SubTitleProperty);
            set => SetValue(SubTitleProperty, value);
        }

        public Type DirectToPage
        {
            get => (Type)GetValue(DirectToPageProperty);
            set => SetValue(DirectToPageProperty, value);
        }

        public TestItem()
        {
            InitializeComponent();
        }

        private async void TestItemTapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (sender is StackLayout stackLayout)
            {
                stackLayout.IsEnabled = false;
                await stackLayout.FadeTo(0.3, 150);
                await stackLayout.FadeTo(1, 150);
                if (this.DirectToPage != null)
                {
                   
                    var page = Activator.CreateInstance(DirectToPage);
                    if (page != null)
                    {
                        var custom = page as CustomContentPage;
                        await App.Navigation.PushAsync(custom);
                    }
                }
                stackLayout.IsEnabled = true;
            }
        }
    }
}