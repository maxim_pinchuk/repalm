﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TileButton : ContentView
	{
	    // Fields
	    public static readonly BindableProperty TitleProperty = BindableProperty.Create("Title", typeof(string), typeof(TileButton), null);
	    public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create("ImageSource", typeof(string), typeof(TileButton), null);
	    public static readonly BindableProperty DirectToPageProperty = BindableProperty.Create("DirectToPage", typeof(Type), typeof(TileButton), null);

        // Properties
        public string Title
	    {
	        get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

	    public string ImageSource
	    {
	        get
	        {
	            return (string) GetValue(ImageSourceProperty);
	        }
	        set
	        {
	            SetValue(ImageSourceProperty, value);
	        }
	    }

	    public Type DirectToPage
	    {
	        get => (Type) GetValue(DirectToPageProperty);
	        set => SetValue(DirectToPageProperty, value);
	    }

	    public TileButton ()
		{
			InitializeComponent ();
		}

	    private async void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
	    {
            //mixit
            if (sender is StackLayout stackLayout)
            {
                stackLayout.IsEnabled = false;
                await stackLayout.FadeTo(0.3, 150);
                await stackLayout.FadeTo(1, 150);
                if (this.DirectToPage != null)
                {
                    var page = Activator.CreateInstance(DirectToPage) ;
                    if (page != null)
                    {
                        var custom = page as CustomContentPage;
                        await App.Navigation.PushAsync(custom);
                    }
                }
                stackLayout.IsEnabled = true;
            }
        }
        
	}
}