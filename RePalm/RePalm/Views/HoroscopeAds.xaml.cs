﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Pages.Horoscope;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HoroscopeAds : ContentView
    {
        public HoroscopeAds()
        {
            InitializeComponent();
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            App.LogFunc("Horoscope_advertise");
            await App.Navigation.PushAsync(new StartFillName());
        }
    }
}