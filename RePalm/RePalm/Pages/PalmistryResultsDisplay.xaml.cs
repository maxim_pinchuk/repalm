﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.PalmRecognition;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PalmistryResultsDisplay 
	{

	    private static string LifeLineHeader { get; set; }
	    private  static  string LifeLineDescription { get; set; }
        private static string HeadLineHeader { get; set; }
	    private static string HeadLineDescription { get; set; }
	    private static string HeartLineHeader { get; set; }
	    private static string HeartLineDescription { get; set; }

        public PalmistryResultsDisplay(PalmistryViewModel palmistryViewModel)
	    {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "PALMISTRY";
            App.LogFunc($"Palmistry_Display_Results");
	        this.BindingContext = palmistryViewModel;
            if (!palmistryViewModel.IsOldSession)
	        {
	            var lifeLineHeader = new string[]
	            {
	                @"— Your line indicates the ability to overcome physical problems. It is a myth that sort of your line signifies a short life. Actually, it may signify that other people can easily influence or control you. Be aware of it",
	                @"— This type of line indicates a smoother life path.",
	                @"— This type of line indicates a person with low energy and having a less adventurous life.",
	                @"— This type of line indicates struggles, losses, unexpected change or interruption in your way of living, an accident or an illness. IT signifies that you may get ill and recover quickly, That you may suffer a serious illness or disease. Also it can indicate problems in early childhood.",
	                @"— Your lide line means that you are susceptible to health or emotional problems. You may also experience a life path that takes you in many directions.",
	                @"— This type of line has various meanings depending on the fork placement on the hand. Your forks indicate that that you are surrounded by scattered or split energies. Also it indicates success and recognition",
	                @"— This type of line indicates that you are surrounded by positive energies and you have great stamina. You might possibly be a twin or have found a true partner and soul mate, or you have someone watching over you. it may also mean that you are living quite a double life.",
	                @"— This type of line indicates a high-strung, anxious and nervous individual.",
	                @"— This type of line indicates achievement and success, quite poor physical and emotional health, money problems, feelings of sadness and loss. Your life line also show an ability to recover from situations. It signifies habitually wasting energy.",
	            };

	            var headLineHeader = new string[]
	            {
	                @"— This type of line indicates intelligence and a good memory. Represents an individual who thinks things through and does not overreact. They look at many possibilities before taking action.",
	                @"— This type of line indicates a very successful individual and not the cowardly type. May have a tendency to be selfish.",
	                @"— This type of line indicates a versatile, complex individual.",
	                @"— This line indicates an individual who is realistic, down-to-earth, unimaginative, materialistic, logical, good organizational skills or a having great attention to detail.",
	                @"— This line indicates a practical and non-complex individual and someone who does not beat around the bush.",
	                @"— This line denotes an excellent memory, concentration and a sensible nature.",
	                @"— This line signifies inner conflict with an individual’s practical and emotional sides. It can also indicate an individual who is untrustworthy, restless, unstable or has a short attention span.",
	                @"— This type of line indicates a romantic and creative and idealistic individual who is open to new ideas and is not afraid to investigate concepts or beliefs. This person trusts his inklings and intuition.",
	                @"— This line signifies and inability to concentrate or a lack of common sense, a daydreamer.",
	                @"— This type of line indicates inconsistent thinking or nervousness and mental exhaustion.",
	                @"— This line indicate the vital and crucial decisions made in one’s life that can have a direct impact on your fate in your life.",
	                @"— This linw signifies an individual who is undergoing personal conflict, melancholy or confusion and can have a problem setting positive goals.",
	                @"— This type of line indicates person enjoys debate and can see both sides of an issue. This indicates great imagination and someone who uses his psychic powers and writing and speaking abilities throughout life.",
	                @"— This line indicates a self-centered, untrustworthy individual. If the hook is low in the palm, the individual can be miserly, selfish and cheap.",
	                @"— This line signifies events that are yet to come. They can also represent distractions that take an individual off his intellectual path.",
	                @"— This type of line signifies positive outcomes and success in career, academics and creativity. Sometimes indicates having big dreams without being centered.",
	                @"— This type of line signifies signs of struggle, possible depression, sorrow and distress and disappointments in certain points in life.",
	                @"— This type of line extremely rare, but it can possibly indicate laziness, sluggishness, dullness or even detachment from reality.",
	                @"— This can indicate increased brainpower. It can also represent a pleasant person or the direct opposite – a cruel person."
	            };

	            var hearLineHeaders = new string[]
	            {
	                @"— This type of line indicates a person who is open and has an overall warmth. It can also indicate having a naïve belief that there are perfect relationships.",
	                @"— This type of line when the line touches both ends of the palm, it shows signs of co-dependency toward their partners (possible promiscuity).",
	                @"— This type of line indicates a highly self-centered individual, not outgoing.",
	                @"— This type of line indicates a stressful life.",
	                @"— This type of line indicates someone with feelings of jealousy, or having a tendency to disregard authority.@",
	                @"— This type of line indicates a more passive person in love relationships, or can signal someone who is void of emotion and or whose emotions are ruled by the brain.",
	                @"— This type of line indicates an individual not particularly concerned with romance.",
	                @"— This type of line indicates an emotionally stable individual.",
	                @"— This type of line represents many love relationships, or a lack of serious relationships.",
	                @"— This type of line indicates sincerity, considerate and respectful, self-secure and at peace with their emotions.",
	                @"— This type of line represents a temperamental approach to life, which can make you either easy-going or quick-tempered.",
	                @"— This type of line represents a more removed, stoic and cold emotional state.",
	                @"— This type of line represents aloofness and places little importance on emotional life.",
	                @"— This type of line indicates a person who is often stressed emotionally, can be subject to mood swings. And suffers from emotional trauma.",
	                @"— This type of line indicates an individual who is easily hurt, has feelings of unhappiness, indecisiveness, or represents a time of depression in your life.",
	                @"— This type of line indicates that your life combines romance with practicality and common sense.",
	                @"— This type of line indicates great balancing between your logical, physical and emotional sides.",
	                @"— This type of line indicates a very physical and emotional, sensitive and intuitive individual. Can represent someone who expresses feelings easily.",
	                @"— This type of line shows ruthlessness and a person who is ruled by logic and may have a disregard for others.",
	                @"— This type of line indicates a lack in the ability of emotional growth.",
	                @"— This type of line represents a strong interest in the opposite sex and has good and positive relationships.",
	                @"— This type of line represents poor quality or unhappiness in relationships, subject to heartbreak."
	            };

	            var linesDescription = new string[]
	            {
	                @"You should be just as inclined to splurge on yourself, opting to plump up your nest or your store of personal possessions in order to feel more secure. Money problems from the past may clear up during this period if you use it appropriately, developing budgets and working out ways to better manage your resources. You may be adding to your possessions, you might receive a large item or gift, or a major purchase or sale can occur now",
	                @"You are likely to make bigger decisions about your material resources. Now this is a good time to work with financial institutions, shoot for a raise, or apply for a loan. You may need to curb an inclination to overspend - feeling good could bring you to the stores as you temporarily forget about the future!",
	                @"Your reasoning skills are sharper than usual, so take advantage! You are able to comprehend more complicated subjects and problems, and you are able to explain them in such a way that others can readily understand; thus, teaching, authoring, or otherwise delivering information may be prosperous now. ",
	                @"if you are having difficulties in varios areas of life, these problems may be magnified before they can be dealt with. So, for example, if you have difficulties with a sibling relationship (one of the things ruled by the solar third house), this might be magnified now and this way you can better deal with it and solve the problem. Taking on too many daily activities, communications, new interests, and learning endeavors or studies can be a problem now. Don't overload your plate. ",
	                @"Limitations that came from early family conditioning will be brought to the fore. At first, there may be some frustration with your life to date, and how you have limited your opportunities because of early conditioning. A feeling of being alone and unsupported in life may dominate now. You may experience some distancing or frustrations regarding your parents. Family obligations may get in the way of your experience of pleasure and of the achievement of your personal goals. There may be some emotional distancing as well in your home environment.",
	                @"Your current living conditions may feel restrictive, limiting, too small or cramped (physically and/or emotionally). Delays and slowdowns may be experienced in the areas of health, mobility, and career. However, all of these things occur in order that you face your responsibilities, become more self-sufficient, and more organized. Ultimately, you are learning to rely on yourself and to take care of things that you may have previously neglected but that have undermined your confidence in yourself and in your life. Working from home is a strong possibility during this period.",
	                @"Creatively speaking, you are a little more serious in your approach. You might be turning a hobby into a business now, or you might find that there is less time for creative hobbies, pastimes, amusements, and entertainment. Your work can require more show, drama, creativity, and entertainment value, and may be a hobby turned business. Romance and work can be tied together in some significant way. Some of you could be working hard on, and putting a lot of effort into, creative hobbies or pastimes.",
	                @"You could struggle somewhat with your own expectations in partnerships. Self-deception may very well be in play, and when others don’t quite measure up to these expectations, disappointments occur. Do your best to see people for who they are rather than what you’d really like them to be. Your own goals could be confusing, and your energy levels could swing quite dramatically at times, perhaps reflecting this inner confusion.",
	                @"You may be adding to your possessions, you might receive a large item or gift, or a major purchase or sale can occur now. A significant gift or bonus may come your way. A large ticket item that you have wanted for a long time might be purchased. You are likely to make bigger decisions about your material resources during this period - likely happy ones. This is a good time to work with financial institutions, shoot for a raise, or apply for a loan. You may need to curb an inclination to overspend - feeling good could bring you to the stores as you temporarily forget about the future!",
	                @"Opposition from others can take many forms, but the lesson here is to strike a balance. There will be times when you long for a life with few restrictions and responsibilities. However, some responsibilities and restrictions are necessary. Seek ways to balance your life in terms of pleasures and responsibilities. Don’t turn your back on commitments and traditions entirely, and aim to learn and grow as much as possible from the challenges you face.",
	                @"Your career, professional projects, reputation, and public image continue to speed up, stimulate, and open up. These areas of life can be unpredictable – the wild card area of life – but also highly stimulating and innovative. You can bring fresh insight into your work. You can also learn to let go of limiting self-consciousness. As long as you don’t push it too far, you can free yourself from some of the fears you have of not living up to others’ expectations, particularly those of parents or society in general.",
	                @"You need to learn to listen to others so that you discover the value of true communication and exchange and to see both sides of a situation. Learning to slow down, to be sensitive to social graces and networks, and to sensitize yourself to others' needs and communications, will release you from nervous tension and help you achieve more happiness and inner balance.",
	                @"Your willingness to enjoy yourself and explore your creativity now brings new opportunities, or you might find that avenues open up to you for exploring new ways of having fun. For example, you might meet a new friend who spurs you to go out more, or you could be inspired by someone else's creative projects and begin a new and rewarding hobby. Creative projects are likely to fare well during this cycle, and could possibly even bring some type of recognition or reward.",
	                @"Love and romance may come into your life or an existing romance is enhanced with good humor and warmth. You might find that possibilities for casual love relationships open up to you now, and some of you could be overwhelmed with choices! Social engagements abound. For those of you who are single, meeting a special person is highly likely, although it's more likely to be a casual relationship than a committed partnership.",
	                @"Those with artistic or athletic talent can be especially successful and prosperous at this time. Your creativity and social life will be stimulated, and plenty of opportunities to express yourself uniquely and creatively are likely to present themselves. You may find great joy and reward in your creative projects and hobbies. Pleasure-seeking activities, recreation, and amusement are increased. You are far less inhibited when it comes to expressing yourself creatively, and you are a lot more fun to be around!",
	                @"Your career, professional projects, reputation, and public image continue to speed up, stimulate, and open up. These areas of life can be unpredictable – the wild card area of life – but also highly stimulating and innovative. You can bring fresh insight into your work. You can also learn to let go of limiting self-consciousness. As long as you don’t push it too far, you can free yourself from some of the fears you have of not living up to others’ expectations, particularly those of parents or society in general.",
	                @"Changes in the way you relate to others are afoot. Power struggles are possible in close personal relationships. Your relationships may have distinct themes revolving around personal power, domination, jealousy, manipulation, or self-will. Your biggest enemy during this period is resentment, which can act to eat away your confidence and healthy state of mind. However, this is a revealing and ultimately empowering transit that sets you on a journey of self-discovery through your relationships with others.",
	                @"You may be taking on more responsibilities on the job or in another service-oriented capacity. For some of you, your job may have become rather tedious or downright boring, or working conditions may be poor, demanding, or frustrating — at least, this is your perception of things. For others, worries about your job could figure. For many, duty could come before pleasure, and a tendency to work more or harder is likely. As long as this doesn’t reach workaholic levels, you’ll be just fine.",
	                @"Do your best to avoid impulsive decision-making that is built solely upon your desire to be a free spirit. However, it’s also important to make changes that accommodate your new independent outlook – my advice is to avoid going overboard on this. You may recognize talents that you never knew you had, and this can be a sudden, exciting discovery.",
	                @"You may encounter some challenges regarding shared finances or property. A loan or other form of support may come through unexpectedly, even in a mysterious fashion. Intimate relationships can be highly colorful, perhaps with spiritual undercurrents, but possibly a little confusing. The boundaries that are normally drawn between what is yours and what is mine may be blurred somewhat. A partner could have unsettled finances, and this could impact your own finances.",
	                @"Discover more power and happiness as you embrace your independence and avoid falling back on others or relationships, particularly their opinions of you! It’s a time for learning to do what feels right without worrying so much about what others think, and for valuing yourself rather than depending on others’ assessments (or perceived assessments) of your value. Although a lot of work goes into your independence, this is one of the healthier periods of your life for mature relationships. ",
	                @"You likely have more than one source of income. You are clever when it comes to business and money-making ideas. You might worry more about money than it appears on the surface of things. You are willing to put the work in to stabilize cash flow, and while your expectations in this area of life are not huge, your work ethic contributes to gradually healthier income as you age.",
	                @"In fact, your charm and diplomacy can help attract suitable work. It’s a good time for gifts, purchases, and acknowledgment. People are happier to give you things and help you out. You can be a little indulgent now, however. This sometimes ends up being a period of more money coming in, but going out in equal measure since your appetite for comfort foods and possessions increases now.",
	                @"You are bringing wonderful warmth and charm to your communications. Your diplomacy skills are augmented even further. You may be verbalizing your affections very well now, or there can be a focus on loving chats and sweet talk. Some of you will find or enhance love through the phone or chat lines, or with fellow students or in the neighborhood. It’s also a favorable time for relationships with siblings. Your career benefits from networking and word of mouth.",
	                @"This is the position of a very hard worker, but sometimes perfectionism in this area of life leads to problems with follow-through due to fear of not performing well enough (according to your own, perhaps too rigid, high standards). Health challenges may center around the knees, bones in general including teeth, and skin. You need consistency and predictability in order to thrive with health and work routines. Maintaining a schedule helps energize you so that you can take on your work load.",
	                @"You’re likely to feel a little frustrated with some of the elements of your life that don’t have an answer, and now you’re ready to take action on them. However, you may be in limbo or at an editing or finalizing stage with significant endeavors in your life. This is not the time to begin brand-new projects. You may be feeling limited, indecisive, or ineffective just for the time being. Honor your need for more rest.",
	                @"You’re motivated to perfect and refine your work and develop your skills, which can from time to time seem to pull yourself away from the creative process. Even so, if this is done in moderation, which is highly likely with balanced energies available to you, you are in a great position to work hard and feel inspired.",
	                @"In general, you are moving towards deeper thinking and the desire for depth and maturity in your connections to others. While the year is good for your social life overall, you are likely going out a little less, favoring quality over quantity when it comes to your leisure time and romantic life, or combining work with recreation very effectively. You may not have the time, or less inclination, for recreation, entertainment, and hobbies, perhaps preferring to pour your energies into more constructive channels.",
	                @"Organizing or reorganizing your home, downsizing and streamlining where necessary, and establishing better and more efficient routines will be helpful. In the past, you’ve often weighed the pros and cons of sticking to tried and true patterns and moving forward to unexplored territory.",
	                @"You’re in a great position to get your life sorted out in key ways. Better routines and more consistent attention to health and wellness figure strongly. You can come to a better work-life balance by automating some of your work or coming up with new ways and methods to do the things you’ve previously toiled over. A partner or even a potential partnership could play a vital role in helping or motivating you to bring more order into your life. This can be a time for finding more joy in life from simplifying. Some of your wilder ideas can be brought down to reality.",
	                @"Freedom and individuality are essential ingredients in your relationships. However, later you’re likely to crave a deep connection. You enter long cycle that improves your intimate world and your relationship with yourself as you embrace your deeper needs. This can be a brilliant period for refunds, gifts, and support. It’s also powerful for counseling, psychiatry, and self-improvement efforts.",
	                @"One of the most rewarding influences assists you in finding a nice balance between innovation and tradition. This happens in your spiritual and emotional worlds. While you can be feeling some sense of lack or limit in your intimate life, you are able to find steady outlets for self-expression elsewhere, and this takes some of the pressure off relationships. There may be also some financial relief.",
	                @"You are bringing wonderful warmth and charm to your communications. You benefit from employing diplomacy now. You may be verbalizing your affections very well now, or there can be a focus on loving chats and sweet talk. Some of you will find or enhance love through the phone or chat lines, or with fellow students or in the neighborhood. It’s also a favorable time for relationships with siblings.",
	                @"You might consider yourself rather lucky when it comes to intimate relationships, money or resources through partnerships, and support from others. You need to watch for turning a blind eye to financial matters at times, simply because you’d rather not deal with the details.",
	                @"Freedom is important to you , but you’re also working to establish yourself as an authority in your field or as an effective manager. If you’ve been treated unfairly, you’ll do whatever you can to fix the imbalance. This ambition can help you to focus on skills development, courses, and training. The time is excellent for these endeavors.",
	                @"This is a powerful time for one-to-one relationships, counseling, marriage, common-law marriage, and business partnership. It’s an excellent period for bringing conflicts to resolution and for mutually beneficial arrangements. Helpful people come into your life now. In fact, the coming the nearest time is particularly strong for support from others.",
	                @"Health can dramatically improve, and you might find that you’re taking more pleasure in managing it. This is a good period for attention to physical fitness and health routines, as you are more aware of how these positively impact your energy and confidence.",
	                @"You are likely to be more sensitive to what seem like outer world pressures that force you to examine some of your deepest desires and attachments. These include your sexual relationships, your finances (especially with regards to debts and shared finances), and your need for control. You may face some difficulties satisfying your libido and/or encounter problems with intimacy.",
	                @"You can benefit greatly from making a conscious effort to deal with others in a more personal, involved, and loving manner, rather than falling back on reason and intellect which detaches you from your feelings. Learning to take personal risks, which requires a certain amount of self-confidence, will help you to achieve inner balance and happiness. Don't be afraid to get creative, go steady, and stand out!",
	                @"Emotionally, there can be a feeling that others are not quite as supportive. There can be delays or difficulties in general when it comes to getting what you want or need from others. Intimacy may seem to dry up before it gets better. However, it’s a strong time for handling financial matters, especially debts and taxes, in a logical and realistic manner. You’ll find that you waste considerably less energy by facing up to realities rather than living your life procrastinating and feeling vaguely guilty all of the time. It’s an excellent time for sorting through your finances and dealing with the power dynamics in a partnership more equitably.",
	                @"Work is especially available to you, and you come across well in job interviews if you need them. There is increased job satisfaction and vocational fulfillment, although the tendency to take on too much should be managed. Jobs involving handling others’ resources, editing, and researching can thrive in particular. ",
	                @"This is a time for taking special pride in your usefulness to others. You are taking more interest in work as well as health in the last time, and it’s an excellent time for discovering new and even enjoyable health and fitness routines. It’s also a very favorable time for pets.",
	                @"Relationships are being tested and you may feel that if you don’t have a good partnership, you’d rather not have any. Many of you will be strengthening relationships and, while it’s not necessarily easy going, you’ll reap the rewards later. Others are feeling a little pressured or lonely as you find your way and discover what it is you truly need and want from a significant other.",
	                @"new opportunities to truly enjoy yourself through recreation, hobbies, and other amusements are likely to come your way. Romantic opportunities expand as well. You’ll discover new outlets to express yourself creatively and playfully. For some of you, a working relationship could turn into something more, or co-workers can be integral in exposing you to a new hobby, different and exciting forms of recreation, or romantic opportunities. There can be exotic experiences in your life ",
	                @"this is an excellent period for stable relationships with friends and partners. There can be wonderful opportunities to enjoy relationships that are both steady and accepting. You may be balancing the stricter, more traditional, rule-oriented energy of partnerships with the progressive elements of friendship and individuality. Being able to depend on others a little more without sacrificing too much in the excitement and independence department is a theme. This is an excellent time to update and refurbish, particularly in your social life. If relationships can use some extra attention, do so.",
	                @"As well, the more integrity, directness, and honesty you bring to your services and work, the more 'payback' you will receive. Health is likely to prosper now, and medical procedures or programs, if necessary, are more apt to be successful. There may be an inclination to put on some weight, however, if you don't watch your appetite and/or choice of food. Some people get a new pet during this cycle, and this brings them much joy. These positive circumstances won't necessarily fall into your lap, and are unlikely to come all at once. You need to keep your eyes open for opportunities in these areas of life.",
	                @"Relationships with parents and other family members go well. You may find special enjoyment through getting in touch with, or researching, your roots or family traditions. There could be a family reunion, vacation, or other event that gives you a stronger sense of connection to your family. Family may help you financially or an inheritance is possible for some of you. You take more pleasure in nurturing others.",
	                @"Your willingness to enjoy yourself and explore your creativity now brings new opportunities, or you might find that avenues open up to you for exploring new ways of having fun. For example, you might meet a new friend who spurs you to go out more, or you could be inspired by someone else's creative projects and begin a new and rewarding hobby. Creative projects are likely to fare well during this cycle, and could possibly even bring some type of recognition or reward.",
	                @"Those with artistic or athletic talent can be especially successful and prosperous at this time. Your creativity and social life are stimulated , and plenty of opportunities to express yourself uniquely and creatively are likely to present themselves. You may find great joy and reward in your creative projects and hobbies. Pleasure-seeking activities, recreation, and amusement are increased. You are far less inhibited when it comes to expressing yourself creatively, and you are a lot more fun to be around!",
	                @"Love and romance may come into your life or an existing romance is enhanced with good humor and warmth. You might find that possibilities for casual love relationships open up to you now, and some of you could be overwhelmed with choices! Social engagements abound. For those of you who are single, meeting a special person is highly likely, although it's more likely to be a casual relationship than a committed partnership.",
	                @"This is a feel-good transit that offers you opportunities to rise above petty concerns and to achieve a more balanced approach to your life. Relationships with others tend to be easygoing, friendly, and positive. It's natural for you to cooperate with others, and others find it easy to cooperate with you! This is a time when you really enjoy life, appreciating the good things and rarely harping on the more inconvenient or annoying elements of life."
	            };

	            var lifeLineHeaderRandom = new Random();
	            var lineDescriptionRandom = new Random();
	            PalmResultHeaderLifeLineLabel.Text = "Life Line (green color)";
	            PalmResultHeaderLifeLineHeader.Text = lifeLineHeader[lifeLineHeaderRandom.Next(0, lifeLineHeader.Length)];
	            LifeLineHeader = PalmResultHeaderLifeLineHeader.Text;
                PalmResultHeaderLifeLineDescription.Text =
	                linesDescription[lineDescriptionRandom.Next(0, linesDescription.Length)];
	            LifeLineDescription = PalmResultHeaderLifeLineDescription.Text;

                var headlineHeaderRandom = new Random();
	            PalmResultHeaderHeadLineLabel.Text = "Head Line (purple color)";
	            PalmResultHeaderHeadLineHeader.Text = headLineHeader[headlineHeaderRandom.Next(0, headLineHeader.Length)];
	            HeadLineHeader = PalmResultHeaderHeadLineHeader.Text;
                PalmResultHeaderHeadLineDescription.Text =
	                linesDescription[lineDescriptionRandom.Next(0, linesDescription.Length)];
	            HeadLineDescription = PalmResultHeaderHeadLineDescription.Text;


                var random = new Random();
	            PalmResultHeaderHeartLineLabel.Text = "Heart Line (red color)";
	            PalmResultHeaderHeartLineHeader.Text = hearLineHeaders[random.Next(0, hearLineHeaders.Length)];
	            HeartLineHeader = PalmResultHeaderHeartLineHeader.Text;
                PalmResultHeaderHeartLineDescription.Text =
	                linesDescription[lineDescriptionRandom.Next(0, linesDescription.Length)];
	            HeartLineDescription = PalmResultHeaderHeartLineDescription.Text;
	            palmistryViewModel.IsOldSession = true;
	        }
	        else
	        {
	            PalmResultHeaderHeartLineLabel.Text = "Heart Line (red color)";
	            PalmResultHeaderHeadLineLabel.Text = "Head Line (purple color)";
	            PalmResultHeaderLifeLineLabel.Text = "Life Line (green color)";
                PalmResultHeaderLifeLineHeader.Text = LifeLineHeader;
	            PalmResultHeaderLifeLineDescription.Text = LifeLineDescription;

	            PalmResultHeaderHeadLineHeader.Text = HeadLineHeader;
	            PalmResultHeaderHeadLineDescription.Text = HeadLineDescription;

	            PalmResultHeaderHeartLineHeader.Text = HeartLineHeader;
	            PalmResultHeaderHeartLineDescription.Text = HeartLineDescription;
	        }
	    }

	    private void Button_OnClicked(object sender, EventArgs e)
	    {
	        Navigation.PopToRootAsync();
	    }
	}
}