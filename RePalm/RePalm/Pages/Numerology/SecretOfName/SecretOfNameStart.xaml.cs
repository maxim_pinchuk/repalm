﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Numerology;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.InAppBilling.Abstractions;

namespace RePalm.Pages.Numerology.SecretOfName
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SecretOfNameStart : CustomContentPage
    {
        public SecretOfNameStart()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.CurrentWindow = "SECRET CODE OF NAME";
            this.BindingContext = new SecretOfNameStartViewModel();
            App.LogFunc($"Numerology_Secret_Code_Of_Name");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            Func<Task> onContinue = async () =>
            {
                PurchaseLoader.IsRunning = true;
                PurchaseButton.IsEnabled = false;

                Func<Task> CommonErrorFunc = async () =>
                {
                    await DisplayAlert("Oops",
                            "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                            "I'll try again in 5 minutes");
                    await App.Navigation.PopToRootAsync();
                };
                new PurchaseManager().PurchaseSubscription("tests_subscrition2",
                    async () =>
                {
                    PurchaseLoader.IsRunning = false;
                    PurchaseButton.IsEnabled = true;

                    var context = this.BindingContext as SecretOfNameStartViewModel;
                    if (context != null && context.CanExecute)
                    {
                        var resultPage = new ResultCommonPage("Secret Of Name")
                        {
                            BindingContext = context.ResultViewModel
                        };

                        await App.Navigation.PushAsync(resultPage);
                    }
                },
                CommonErrorFunc,
                    CommonErrorFunc,
                    async (InAppBillingPurchaseException purchaseEx) =>
                    {
                        await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                        await App.Navigation.PopToRootAsync(true);
                    },
                    CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                );
            };

            await App.Navigation.PushAsync(
                                        new PurchaseSubscription(
                                            "Secret Of Name",
                                            " - Subscription Information: Psychology Tests;",
                                            " - Weekly Subscription;",
                                            " - $3.99 per week and access for all tests of this subscription; ",
                                            " - 3 days free trial to check out functionality;", onContinue));
            }
        }
}