﻿using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Numerology
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Numerology : CustomContentPage
    {
        public Numerology()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            base.CurrentWindow = "NUMEROLOGY";
            App.LogFunc("Numerology_Main");
        }
    }
}