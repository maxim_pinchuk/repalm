﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Numerology;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using RePalm.Views;

namespace RePalm.Pages.Numerology.CarNumber
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CarNumberStart : CustomContentPage
    {
        public CarNumberStart()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "CAR NUMBER";
            App.LogFunc($"Numerology_Сar_Number_Test");
            this.BindingContext = new CarNumberViewModel();
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {

            Func<Task> onContinue = async () =>
            {

                PurchaseLoader.IsRunning = true;
                PurchaseButton.IsEnabled = false;


                Func<Task> CommonErrorFunc = async () =>
                {
                    await DisplayAlert("Oops",
                            "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                            "I'll try again in 5 minutes");

                    await App.Navigation.PopToRootAsync();
                };

                new PurchaseManager().PurchaseSubscription("tests_subscrition2",
                    async () =>
                    {
                        var context = this.BindingContext as CarNumberViewModel;
                        if (context != null && context.CanExecute)
                        {
                            var resultPage = new ResultCommonPage("Car Number")
                            {
                                BindingContext = context.ResultViewModel
                            };

                            PurchaseLoader.IsRunning = false;
                            PurchaseButton.IsEnabled = true;
                            await App.Navigation.PushAsync(resultPage);
                        }
                    },
                CommonErrorFunc,
                    CommonErrorFunc,
                    async (InAppBillingPurchaseException purchaseEx) =>
                    {
                        await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                        await App.Navigation.PopToRootAsync(true);
                    },
                    CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                );
            };

            await App.Navigation.PushAsync(
                                        new PurchaseSubscription(
                                            "Car Number",
                                            " - Subscription Information: Psychology Tests;",
                                            " - Weekly Subscription;",
                                            " - $3.99 per week and access for all tests of this subscription; ",
                                            " - 3 days free trial to check out functionality;", onContinue));
        }
    }
}