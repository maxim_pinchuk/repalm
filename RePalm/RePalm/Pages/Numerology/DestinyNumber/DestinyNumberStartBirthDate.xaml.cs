﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Numerology;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.InAppBilling.Abstractions;

namespace RePalm.Pages.Numerology.DestinyNumber
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DestinyNumberStartBirthDate : CustomContentPage
    {
        public DestinyNumberStartBirthDate()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = new DestinyNumberViewModel();
            PreviosWindow = "DESTINY NUMBER";
            App.LogFunc($"Numerology_Destiny_Number");
        }


        private async void Button_OnClicked(object sender, EventArgs e)
        {
            Func<Task> onContinue = async () =>
            {

                PurchaseLoader.IsRunning = true;
                PurchaseButton.IsEnabled = false;

                Func<Task> CommonErrorFunc = async () =>
                {
                    await DisplayAlert("Oops",
                            "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                            "I'll try again in 5 minutes");
                    await App.Navigation.PopToRootAsync();
                };
                new PurchaseManager().PurchaseSubscription("tests_subscrition2",
                    async () =>
                    {
                        var context = this.BindingContext as DestinyNumberViewModel;
                        if (context != null && context.CanExecute)
                        {
                            PurchaseLoader.IsRunning = false;
                            PurchaseButton.IsEnabled = true;

                            await App.Navigation.PushAsync(new ResultCommonPage("Destiny Number")
                            {
                                BindingContext = context.Result
                            });
                        }
                    },
                CommonErrorFunc,
                    CommonErrorFunc,
                    async (InAppBillingPurchaseException purchaseEx) =>
                    {
                        await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                        await App.Navigation.PopToRootAsync(true);
                    },
                    CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                );
            };

            await App.Navigation.PushAsync(
                                    new PurchaseSubscription(
                                        "Destiny Number",
                                        " - Subscription Information: Psychology Tests;",
                                        " - Weekly Subscription;",
                                        " - $3.99 per week and access for all tests of this subscription; ",
                                        " - 3 days free trial to check out functionality;", onContinue));
        }
    }
}