﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Platform.Device;

namespace RePalm.Pages.Numerology
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ResultCommonPage : CustomContentPage
    {
	    public ResultCommonPage(string PreviousWindowTitle)
	    {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = PreviousWindowTitle;
            App.LogFunc($"Numerology_Result_{PreviousWindowTitle.Replace(' ','_')}");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            HomeBtn.IsEnabled = false;
            if (HomeBtn.IsEnabled == false)
            {
                await App.Navigation.PopToRootAsync(true);
            }
            HomeBtn.IsEnabled = true;
        }
	}
}