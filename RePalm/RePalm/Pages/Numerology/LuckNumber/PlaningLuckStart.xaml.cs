﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Numerology;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.InAppBilling.Abstractions;

namespace RePalm.Pages.Numerology.LuckNumber
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlaningLuckStart : CustomContentPage
    {
        public PlaningLuckStart()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = new LuckNumberViewModel();
            CurrentWindow = "PLANNING LUCK";
            App.LogFunc($"Numerology_Planing_luck_1_Start");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            var context = this.BindingContext as LuckNumberViewModel;
            if (context != null && context.CanEnterBirthDate)
            {
                var newPage = new PlaningLuckBirthDate()
                {
                    BindingContext = this.BindingContext
                };
                await App.Navigation.PushAsync(newPage);
            }
        }
    }
}