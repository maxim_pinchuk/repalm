﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Numerology;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Numerology.PythagorsSquare
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PythagorsSquareStart : CustomContentPage
    {
        public PythagorsSquareStart()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "PYTHAGORS SQUARE";
            this.BindingContext = new PythagorsSquareViewModel();
            App.LogFunc($"Numerology_pythagors_square");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {

            var context = this.BindingContext as PythagorsSquareViewModel;
            if (context != null && context.CanExecute)
            {
                var resultPage = new ResultCommonPage("")
                {
                    BindingContext = context.ResultViewModel
                };

                await App.Navigation.PushAsync(resultPage);
            }
        }
    }
}