﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Numerology;
using RePalm.Pages.Numerology.LuckNumber;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Numerology.YearlyPrediction
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class YearlyPredictionStart : CustomContentPage
    {
        public YearlyPredictionStart()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = new YearlyPredictionViewModel();
            CurrentWindow = "YEARLY PREDICTION";
            App.LogFunc($"Numerology_Yearly_Prediction_1_Start");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            var context = this.BindingContext as YearlyPredictionViewModel;
            if (context != null && context.CanExecute)
            {
                var newPage = new SelectDesiredYear()
                {
                    BindingContext = this.BindingContext
                };
                await App.Navigation.PushAsync(newPage);
            }
        }
    }
}