﻿using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Career : CustomContentPage
    {
        public Career()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "CAREER";
            App.LogFunc($"Career_Main");
        }
    }
}