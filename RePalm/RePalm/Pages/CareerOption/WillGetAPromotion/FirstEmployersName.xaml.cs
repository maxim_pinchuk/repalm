﻿using System;
using RePalm.Client.ViewModels.Career;
using RePalm.Pages.CareerOption.WillEverBeRich;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.WillGetAPromotion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FirstEmployersName
    {
		public FirstEmployersName(WillGetPromotionViewModel vm)
		{
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "WILL I GET A PROMOTION";
            BindingContext = vm;
            App.LogFunc($"Career_Will_Get_Promotion_3_EmployerName");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new FirstJobPosition(BindingContext as WillGetPromotionViewModel));
	    }
	}
}