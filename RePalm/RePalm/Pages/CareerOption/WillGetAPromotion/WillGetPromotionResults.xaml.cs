﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Career;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.WillGetAPromotion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WillGetPromotionResults : CustomContentPage
    {
		public WillGetPromotionResults (WillGetPromotionViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "WILL I GET A PROMOTION";
            BindingContext = vm.ResultViewModel;
            App.LogFunc($"Career_Will_Get_Promotion_6_Results");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PopToRootAsync();
	    }
	}
}