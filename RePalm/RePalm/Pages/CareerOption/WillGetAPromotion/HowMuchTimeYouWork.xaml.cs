﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Career;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.InAppBilling.Abstractions;

namespace RePalm.Pages.CareerOption.WillGetAPromotion
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HowMuchTimeYouWork
    {
        public HowMuchTimeYouWork(WillGetPromotionViewModel vm)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = vm;

            CurrentWindow = "WILL I GET A PROMOTION";
            AnswerOptions.ItemsSource = new List<string>()
            {
                "As little as possible","Within the job requirements","Often work overtime","As much as possible"
            };
            App.LogFunc($"Career_Will_Get_Promotion_5_WorkTime");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            Func<Task> onContinue = async () =>
            {
                PurchaseLoader.IsRunning = true;
                PurchaseButton.IsEnabled = false;
                Func<Task> CommonErrorFunc = async () =>
                {
                    await DisplayAlert("Oops",
                            "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                            "I'll try again in 5 minutes");
                    await App.Navigation.PopToRootAsync();
                };
                new PurchaseManager().PurchaseSubscription("tests_subscrition2",
                    async () =>
                    {

                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        await App.Navigation.PushAsync(new WillGetPromotionResults(BindingContext as WillGetPromotionViewModel))
                        ;
                    },
                CommonErrorFunc,
                    CommonErrorFunc,
                    async (InAppBillingPurchaseException purchaseEx) =>
                    {
                        await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                        await App.Navigation.PopToRootAsync(true);
                    },
                    CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                );
            };


            await App.Navigation.PushAsync(
                                        new PurchaseSubscription(
                                            "Will I Get A Promotion",
                                            " - Subscription Information: Psychology Tests;",
                                            " - Weekly Subscription;",
                                            " - $3.99 per week and access for all tests of this subscription; ",
                                            " - 3 days free trial to check out functionality;", onContinue));
        }
    }
}