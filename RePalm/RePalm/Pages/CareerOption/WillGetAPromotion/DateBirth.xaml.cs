﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Career;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.WillGetAPromotion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DateBirth
	{
		public DateBirth (WillGetPromotionViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
            CurrentWindow = "WILL I GET A PROMOTION";
            App.LogFunc($"Career_Will_Get_Promotion_2_Birthday");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new FirstEmployersName(BindingContext as WillGetPromotionViewModel));
	    }
	}
}