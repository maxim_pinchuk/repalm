﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Career;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Controls;

namespace RePalm.Pages.CareerOption.WillEverBeRich
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectEyesColor 
    {
        private ImageButton PrevioslyClickedButton { get; set; }
		public SelectEyesColor (WillEverBeRichViewModel vm)
		{
            InitializeComponent ();
		    CurrentWindow = "Will I EVER BE RICH";
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = vm;
            App.LogFunc($"Career_Will_I_ever_be_rich_2_Eye_Color");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        var imageButton = sender as ImageButton;
	        if (imageButton != null && PrevioslyClickedButton != imageButton)
	        {
	            if (PrevioslyClickedButton != null)
	            {
                    await PrevioslyClickedButton.ScaleTo(1, 200);

	            }
	            await imageButton.ScaleTo(1.3, 200);
	            PrevioslyClickedButton = imageButton;
	        }
	    }

        private async void Button_OnClicked1(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new DateBirth(this.BindingContext as WillEverBeRichViewModel));
        }
    }
}