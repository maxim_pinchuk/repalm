﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Career;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;

namespace RePalm.Pages.CareerOption.WillEverBeRich
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HowMuchTimeYouWork
	{
		public HowMuchTimeYouWork(Client.ViewModels.Career.WillEverBeRichViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    AnswerOptions.ItemsSource = new List<string>()
		    {
		        "As little as possible","Within the job requirements","Often work overtime","As much as possible"
		    };

            App.LogFunc($"Career_Will_I_ever_be_rich_7_WorkTime");
        }

	    private async  void Button_OnClicked(object sender, EventArgs e)
        {

            Func<Task> OnApproveSubscription = async () =>
            {
                PurchaseLoader.IsRunning = true;
                PurchaseButton.IsEnabled = false;
                Func<Task> CommonErrorFunc = async () =>
                {
                    await DisplayAlert("Oops",
                            "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                            "I'll try again in 5 minutes");
                    await App.Navigation.PopToRootAsync();
                };
                new PurchaseManager().PurchaseSubscription("tests_subscrition2",
                    async () => {

                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        await App.Navigation.PushAsync(
                                     new WillEverBeRichAnswer(BindingContext as WillEverBeRichViewModel));
                    },
                CommonErrorFunc,
                    CommonErrorFunc,
                    async (InAppBillingPurchaseException purchaseEx) =>
                    {
                        await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                        await App.Navigation.PopToRootAsync(true);
                    },
                    CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                );
            };
            await App.Navigation.PushAsync(
                                new PurchaseSubscription(
                                    "Will I Ever Be Rich",
                                    "- Subscription Information: Psychology Tests;",
                                    " - Weekly Subscription;",
                                    " - $3.99 per week and access for all tests of this subscription; ",
                                    " - 3 days free trial to check out functionality;", OnApproveSubscription));

            
	    }
	}
}