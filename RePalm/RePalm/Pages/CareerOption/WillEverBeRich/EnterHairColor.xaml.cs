﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Career;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.WillEverBeRich
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnterHairColor 
	{
		public EnterHairColor(WillEverBeRichViewModel vm)
		{
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "Will I EVER BE RICH";
            App.LogFunc($"Career_Will_I_ever_be_rich_4_Hair_Color");
            this.BindingContext = vm;
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new EnterHeight(this.BindingContext as WillEverBeRichViewModel));
	    }
	}
}