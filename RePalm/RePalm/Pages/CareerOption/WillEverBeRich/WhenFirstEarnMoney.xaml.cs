﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Career;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.WillEverBeRich
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WhenFirstEarnMoney 
	{
		public WhenFirstEarnMoney(WillEverBeRichViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "Will I EVER BE RICH";
            BindingContext = vm;
            App.LogFunc($"Career_Will_I_ever_be_rich_6_FirstMoney");
		    AnswerOptions.ItemsSource = new List<string>()
		    {
		        "10","10-15","15-20","20-25","after 25"
		    };
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new HowMuchTimeYouWork(BindingContext as WillEverBeRichViewModel));
	    }
	}
}