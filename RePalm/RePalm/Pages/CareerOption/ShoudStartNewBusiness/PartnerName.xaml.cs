﻿using System;
using RePalm.Client.ViewModels.Career;
using RePalm.Pages.CareerOption.WillGetAPromotion;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.InAppBilling.Abstractions;
using System.Threading.Tasks;

namespace RePalm.Pages.CareerOption.ShoudStartNewBusiness
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PartnersName : CustomContentPage
    {
		public PartnersName(ShouldStartNewBusinessViewModel vm)
		{
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "SHOULD I START NEW BUSINESS";
            App.LogFunc($"Career_New_Business_6_Partners_Name");
            BindingContext = vm;
		}

        private  async void Button_OnClicked(object sender, EventArgs e)
        {
            Func<Task> onContinue = async () => 
            {
                PurchaseLoader.IsRunning = true;
                PurchaseButton.IsEnabled = false;
                Func<Task> CommonErrorFunc = async () =>
                {
                    await DisplayAlert("Oops",
                            "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                            "I'll try again in 5 minutes");
                    await App.Navigation.PopToRootAsync();
                };
                new PurchaseManager().PurchaseSubscription("tests_subscrition2",
                    async () =>
               {

                   PurchaseLoader.IsRunning = false;
                   PurchaseButton.IsEnabled = true;
                   await App.Navigation.PushAsync(new ShouldStartNewBusinessResults(BindingContext as ShouldStartNewBusinessViewModel))
                ;
               },

                        CommonErrorFunc,
                    CommonErrorFunc,
                    async (InAppBillingPurchaseException purchaseEx) =>
                    {
                        await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                        await App.Navigation.PopToRootAsync(true);
                    },
                    CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                );
            };

            await App.Navigation.PushAsync(
                                    new PurchaseSubscription(
                                        "Should I Start a new Business",
                                        "- Subscription Information: Psychology Tests;",
                                        " - Weekly Subscription;",
                                        " - $3.99 per week and access for all tests of this subscription; ",
                                        " - 3 days free trial to check out functionality;", onContinue));
        }

	}
}