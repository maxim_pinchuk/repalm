﻿using System;
using RePalm.Client.ViewModels.Career;
using RePalm.Client.ViewModels.CommonViewModels;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.ShoudStartNewBusiness
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShouldStartNewBusinessResults : CustomContentPage
    {
		public ShouldStartNewBusinessResults(ShouldStartNewBusinessViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "SHOULD I START NEW BUSINESS";

            App.LogFunc($"Career_New_Business_7_Result");
            BindingContext = vm.ResultViewModel;
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PopToRootAsync();
	    }
	}
}