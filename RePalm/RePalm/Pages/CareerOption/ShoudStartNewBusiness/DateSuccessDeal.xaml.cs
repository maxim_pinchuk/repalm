﻿using System;
using RePalm.Client.ViewModels.Career;
using RePalm.Pages.CareerOption.WillEverBeRich;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.ShoudStartNewBusiness
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DateSuccessDeal : CustomContentPage
    {
		public DateSuccessDeal(ShouldStartNewBusinessViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
            CurrentWindow = "SHOULD I START NEW BUSINESS";
            App.LogFunc($"Career_New_Business_5_Success_Deal_Date");

		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new PartnersName(BindingContext as ShouldStartNewBusinessViewModel));
	    }
	}
}