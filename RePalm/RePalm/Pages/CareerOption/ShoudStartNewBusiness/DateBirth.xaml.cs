﻿using System;
using RePalm.Client.ViewModels.Career;
using RePalm.Pages.CareerOption.WillEverBeRich;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.ShoudStartNewBusiness
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DateBirth : CustomContentPage
    {
		public DateBirth (ShouldStartNewBusinessViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
            CurrentWindow = "SHOULD I START NEW BUSINESS";
            App.LogFunc($"Career_New_Business_2_Birthday");

		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new WhatAnimalYouAssociate(BindingContext as ShouldStartNewBusinessViewModel));
	    }
	}
}