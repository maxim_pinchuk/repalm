﻿using System;
using System.Collections.Generic;
using RePalm.Client.ViewModels.Career;
using RePalm.Pages.CareerOption.WillEverBeRich;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.ShoudStartNewBusiness
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WhatAnimalYouAssociate : CustomContentPage
    {
		public WhatAnimalYouAssociate(ShouldStartNewBusinessViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    CurrentWindow = "SHOULD I START NEW BUSINESS";

            App.LogFunc($"Career_New_Business_3_Animal");
            AnswerOptions.ItemsSource = new List<string>()
		    {
                "Wolf",
                "Lion",
                "Bull",
                "Shark",
                "Whale",
                "Turtle",
                "Elephant",
                "Eagle"
		    };
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new HowMuchTimeYouWork(BindingContext as ShouldStartNewBusinessViewModel));
	    }
	}
}