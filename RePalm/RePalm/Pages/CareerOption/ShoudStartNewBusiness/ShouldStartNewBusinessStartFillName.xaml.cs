﻿using System;
using RePalm.Client.ViewModels.Career;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.ShoudStartNewBusiness
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShouldStartNewBusinessStartFillName : CustomContentPage
    {
		public ShouldStartNewBusinessStartFillName()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "SHOULD I START NEW BUSINESS";
            App.LogFunc($"Career_New_Business_1_Name");
            BindingContext = new ShouldStartNewBusinessViewModel();

		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new DateBirth(BindingContext as ShouldStartNewBusinessViewModel));
	    }
	}
}