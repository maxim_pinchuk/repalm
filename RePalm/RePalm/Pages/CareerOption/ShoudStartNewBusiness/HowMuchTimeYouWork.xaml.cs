﻿using System;
using System.Collections.Generic;
using RePalm.Client.ViewModels.Career;
using RePalm.Pages.CareerOption.WillEverBeRich;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.CareerOption.ShoudStartNewBusiness
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HowMuchTimeYouWork: CustomContentPage
    {
		public HowMuchTimeYouWork(ShouldStartNewBusinessViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
            CurrentWindow = "SHOULD I START NEW BUSINESS";
            App.LogFunc($"Career_New_Business_4_Time_Work");
            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "As little as possible","Within the job requirements","Often work overtime","As much as possible"
		    };
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new DateSuccessDeal(BindingContext as ShouldStartNewBusinessViewModel));
	    }
	}
}