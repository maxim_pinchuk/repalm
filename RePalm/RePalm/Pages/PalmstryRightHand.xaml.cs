﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RePalm.Client.ViewModels.PalmRecognition;

namespace RePalm.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PalmstryRightHand 
    {
        public PalmstryRightHand(PalmistryViewModel palmistryViewModel)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "PALMISTRY";
            this.BindingContext = palmistryViewModel;
            var s = new FormattedString();
            s.Spans.Add(new Span { Text = "Take a photo of your", FontAttributes = FontAttributes.None });
            s.Spans.Add(new Span
            {
                Text = " right ",
                FontAttributes = FontAttributes.Bold,
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
            });
            s.Spans.Add(new Span() { Text = "palm," });
            palmistryViewModel.LeftImageBase64 = "";
            palmistryViewModel.RightImageBase64 = "";
            RightPalmLabel.FormattedText = s;
            App.LogFunc($"Palmistry_Right_Hand_Start");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            var viewModel = this.BindingContext as PalmistryViewModel;
            
            await App.Navigation.PushAsync(new Capture(viewModel.RightHandPictureViewModel)
            {
                ImageOverlay = "palmRight.png",
                NextPage = typeof(PalmistryGetResults),
                NextPageViewModel = viewModel
            });
        }
    }
}