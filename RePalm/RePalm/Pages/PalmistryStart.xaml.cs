﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using RePalm.Client.ViewModels.PalmRecognition;
namespace RePalm.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PalmistryStart 
    {
        public PalmistryStart()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "PALMISTRY";
            App.LogFunc($"Palmistry_Start");

        }

        private async void RightHander_OnClicked(object sender, EventArgs e)
        {
            var vm = new PalmistryViewModel()
            {
                    IsRightHand =  true
            };
            await App.Navigation.PushAsync(new PalmstryRightHand(vm));
        }

        private async void LeftHander_OnClicked(object sender, EventArgs e)
        {
            var vm = new PalmistryViewModel()
            {
            };
            vm.IsRightHand = false;
            await App.Navigation.PushAsync(new PalminstryLeftHand(vm));
        }

    }
}