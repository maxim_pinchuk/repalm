﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RelationshipStart 
	{
		public RelationshipStart ()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "RELATIONSHIP";
            App.LogFunc($"Relationship_Main");
		}
	}
}