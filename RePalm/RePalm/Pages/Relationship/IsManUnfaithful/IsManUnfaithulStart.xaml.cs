﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.IsManUnfaithful
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IsManUnfaithulStart 
	{
		public IsManUnfaithulStart ()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = new IsManUnfaitfulViewModel();
            CurrentWindow = "IS YOUR MAN UNFAITHFUL";
            App.LogFunc($"Relationship_Is_Man_Unfailthful_1_Start");

        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new EnterAge(BindingContext as IsManUnfaitfulViewModel));
	    }
	}
}