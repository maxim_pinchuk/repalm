﻿using RePalm.Client.ViewModels.Relationship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.IsManUnfaithful
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HowOftenSeeEachOther 
	{
		public HowOftenSeeEachOther (IsManUnfaitfulViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "IS YOUR MAN UNFAITHFUL";
            BindingContext = vm;
		    AnswerOptions.ItemsSource = new List<string>()
		    {
		        "Rarely","Several","Often","All time","Live together"
            };
            App.LogFunc($"Relationship_Is_Man_Unfailthful_5_OftenSeeEachOther");

        }

        private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new HowOftenSleepAlone(BindingContext as IsManUnfaitfulViewModel));
	    }
	}
}