﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.IsManUnfaithful
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnterAge 
	{
		public EnterAge (IsManUnfaitfulViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
            CurrentWindow = "IS YOUR MAN UNFAITHFUL";
            App.LogFunc($"Relationship_Is_Man_Unfailthful_2_Age");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new PartnersName(BindingContext as IsManUnfaitfulViewModel));
	    }
	}
}