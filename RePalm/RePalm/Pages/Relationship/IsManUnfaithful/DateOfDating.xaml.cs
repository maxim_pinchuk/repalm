﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.IsManUnfaithful
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DateOfDating 
	{
		public DateOfDating (IsManUnfaitfulViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "IS YOUR MAN UNFAITHFUL";
            BindingContext = vm;
            App.LogFunc($"Relationship_Is_Man_Unfailthful_8_DatingDate");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new WhoIsMoreActive(BindingContext as IsManUnfaitfulViewModel));
	    }
	}
}