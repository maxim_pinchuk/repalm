﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.IsManUnfaithful
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HowOftenSleepAlone 
	{
		public HowOftenSleepAlone (IsManUnfaitfulViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    CurrentWindow = "IS YOUR MAN UNFAITHFUL";
            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "Very rarely","Few times a week","Constantly"
            };
            App.LogFunc($"Relationship_Is_Man_Unfailthful_6_OftenSleepAlone");

        }

        private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new DoesHeTreatYou(BindingContext as IsManUnfaitfulViewModel));
	    }
	}
}