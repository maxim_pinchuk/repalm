﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.IsManUnfaithful
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DoesHeTreatYou 
	{
		public DoesHeTreatYou (IsManUnfaitfulViewModel vm)
		{
			InitializeComponent ();
            BindingContext = vm;
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "IS YOUR MAN UNFAITHFUL";
            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "Yes","Changed a bit","I don't understand him", "He is a different man not"
            };
            App.LogFunc($"Relationship_Is_Man_Unfailthful_7_HeTreatYou");

        }

        private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new DateOfDating(BindingContext as IsManUnfaitfulViewModel));
	    }
	}
}