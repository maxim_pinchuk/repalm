﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs;
using XLabs.Forms.Controls;

namespace RePalm.Pages.Relationship.HowLong
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HowOftenSeeEachOther 
	{
		public HowOftenSeeEachOther ()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = new HowLongRelationWillLastViewModel();
		    CurrentWindow = "HOW LONG RELATIONSHIP WILL..";
            AnswerOptions.ItemsSource = new List<string>()
            {
                "Rarely","Several","Often","All time","Live together"
            };
            App.LogFunc($"Relationship_HowLongRelations_1_Start");

        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new EnterName(BindingContext as HowLongRelationWillLastViewModel));
        }
	}
}