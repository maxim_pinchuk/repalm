﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.InAppBilling.Abstractions;

namespace RePalm.Pages.Relationship.HowLong
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DoUPlanLongRelationship
    {
        public DoUPlanLongRelationship(HowLongRelationWillLastViewModel vm)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "HOW LONG RELATIONSHIP WILL..";
            BindingContext = vm;
            AnswerOptions.ItemsSource = new List<string>()
            {
                "Yes","No","I don't know"
            };
            App.LogFunc($"Relationship_HowLongRelations_11_Long");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            Func<Task> onContinue = async () =>
            {

                PurchaseLoader.IsRunning = true;
                PurchaseButton.IsEnabled = false;
                Func<Task> CommonErrorFunc = async () =>
                {
                    await DisplayAlert("Oops",
                            "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                            "I'll try again in 5 minutes");
                    await App.Navigation.PopToRootAsync();
                };
                new PurchaseManager().PurchaseSubscription("tests_subscrition2",
                    async () =>
                    {

                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        await App.Navigation.PushAsync(new HowLongRelationshipResult(BindingContext as HowLongRelationWillLastViewModel))
                        ;
                    },
                CommonErrorFunc,
                    CommonErrorFunc,
                    async (InAppBillingPurchaseException purchaseEx) =>
                    {
                        await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                        await App.Navigation.PopToRootAsync(true);
                    },
                    CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                );
            };

            await App.Navigation.PushAsync(
                                        new PurchaseSubscription(
                                            "How Long Your Relationship Will Last",
                                            " - Subscription Information: Psychology Tests;",
                                            " - Weekly Subscription;",
                                            " - $3.99 per week and access for all tests of this subscription; ",
                                            " - 3 days free trial to check out functionality;", onContinue));
        }
    }
}