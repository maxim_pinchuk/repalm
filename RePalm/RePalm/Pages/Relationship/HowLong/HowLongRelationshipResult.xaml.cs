﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.HowLong
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HowLongRelationshipResult 
	{
		public HowLongRelationshipResult (HowLongRelationWillLastViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "HOW LONG RELATIONSHIP WILL..";
            BindingContext = vm.ResultViewModel;
            App.LogFunc($"Relationship_HowLongRelations_12_Result");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PopToRootAsync();
	    }
	}
}