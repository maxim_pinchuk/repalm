﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.HowLong
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HowManyPartners 
	{
		public HowManyPartners (HowLongRelationWillLastViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    CurrentWindow = "HOW LONG RELATIONSHIP WILL..";
            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "1","2-4","5-9", "10-19","20-30","More"
            };
            App.LogFunc($"Relationship_HowLongRelations_8_HowManyPartners");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new HaveYouBeenUnfaitful(BindingContext as HowLongRelationWillLastViewModel));
	    }
	}
}