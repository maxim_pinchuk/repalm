﻿using System;
using System.Collections.Generic;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.HowLong
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DoUReallyLove 
	{
		public DoUReallyLove (HowLongRelationWillLastViewModel viewModel)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = viewModel;
		    CurrentWindow = "HOW LONG RELATIONSHIP WILL..";
            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "Yes","No","I don't know"
            };
            App.LogFunc($"Relationship_HowLongRelations_7_UReallyLove");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new HowManyPartners(BindingContext as HowLongRelationWillLastViewModel));
	    }
	}
}