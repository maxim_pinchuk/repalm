﻿using RePalm.Client.ViewModels.Relationship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.HowLong
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SexualLife 
	{
		public SexualLife (HowLongRelationWillLastViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    CurrentWindow = "HOW LONG RELATIONSHIP WILL..";

            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "Unsure of  yourself","Active","Passionate", "Somewhat dissolute"
            };
            App.LogFunc($"Relationship_HowLongRelations_10_SexualLife");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new DoUPlanLongRelationship(BindingContext as HowLongRelationWillLastViewModel));

	    }
	}
}