﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.HowLong
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnterAge 
	{
		public EnterAge (HowLongRelationWillLastViewModel vm)
		{
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    AgePicker.MinimumDate = DateTime.Today.AddYears(-120);
		    CurrentWindow = "HOW LONG RELATIONSHIP WILL..";
            AgePicker.MaximumDate = DateTime.Today.AddYears(-6);
            App.LogFunc($"Relationship_HowLongRelations_3_Age");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new PartherName(BindingContext as  HowLongRelationWillLastViewModel));
	    }
	}
}