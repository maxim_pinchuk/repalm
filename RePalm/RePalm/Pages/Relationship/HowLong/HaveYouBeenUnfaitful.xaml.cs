﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.HowLong
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HaveYouBeenUnfaitful 
	{
		public HaveYouBeenUnfaitful (HowLongRelationWillLastViewModel  vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    CurrentWindow = "HOW LONG RELATIONSHIP WILL..";
            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "Never","Once","Several times", "More than 5 times"
            };
            App.LogFunc($"Relationship_HowLongRelations_9_UWasUnfaithful");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new SexualLife(BindingContext as HowLongRelationWillLastViewModel));
	    }
	}
}