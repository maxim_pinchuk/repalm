﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.HowLong
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DateOfDating 
	{
		public DateOfDating (HowLongRelationWillLastViewModel vm)
		{
            InitializeComponent ();
		    CurrentWindow = "HOW LONG RELATIONSHIP WILL..";
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = vm;
            App.LogFunc($"Relationship_HowLongRelations_6_DatingDate");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new DoUReallyLove(BindingContext as HowLongRelationWillLastViewModel));
	    }
	}
}