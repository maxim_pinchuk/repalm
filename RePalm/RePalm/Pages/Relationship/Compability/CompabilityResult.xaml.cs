﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.Compability
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompabilityResult 
    {
        public CompabilityResult(CompabilityViewModel vm)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = vm.ResultViewModel;
            CurrentWindow = "COMPATIBILITY";
            App.LogFunc($"Relationship_Compability_7_Result");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            await App.Navigation.PopToRootAsync();
        }
    }
}