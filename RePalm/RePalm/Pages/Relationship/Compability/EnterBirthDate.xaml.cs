﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.Compability
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterBirthDate 
    {
        public EnterBirthDate(CompabilityViewModel vm)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "COMPATIBILITY";
            BindingContext = vm;
            App.LogFunc($"Relationship_Compability_2_Birthday");

        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new EnterPartnerName(BindingContext as CompabilityViewModel));
        }
    }
}