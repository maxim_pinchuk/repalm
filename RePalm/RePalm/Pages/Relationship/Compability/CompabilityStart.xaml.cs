﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.Compability
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompabilityStart 
    {
        public CompabilityStart()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new CompabilityViewModel();
            CurrentWindow = "COMPATIBILITY";
            App.LogFunc($"Relationship_Compability_1_Start");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new EnterBirthDate(BindingContext as CompabilityViewModel));
        }
    }
}