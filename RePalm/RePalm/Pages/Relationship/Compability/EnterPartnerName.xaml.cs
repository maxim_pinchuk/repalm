﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.Compability
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterPartnerName 
    {
        public EnterPartnerName(CompabilityViewModel vm)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = vm;
            CurrentWindow = "COMPATIBILITY";
            App.LogFunc($"Relationship_Compability_3_PartnerName");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new EnterPartnerBirthhDate(BindingContext as CompabilityViewModel));
        }
    }
}