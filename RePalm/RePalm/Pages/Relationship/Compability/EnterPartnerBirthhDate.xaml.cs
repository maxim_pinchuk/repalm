﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.Compability
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterPartnerBirthhDate 
    {
        public EnterPartnerBirthhDate(CompabilityViewModel vm)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "COMPATIBILITY";
            BindingContext = vm;
            App.LogFunc($"Relationship_Compability_4_PartnerBday");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new HowOftenSeeEachOther(BindingContext as CompabilityViewModel));
        }
    }
}