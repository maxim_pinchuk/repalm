﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.Compability
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HowOftenSeeEachOther 
    {
        public HowOftenSeeEachOther(CompabilityViewModel vm)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = vm;
            CurrentWindow = "COMPATIBILITY";
            AnswerOptions.ItemsSource = new List<string>()
            {
                "Rarely","Several","Often","All time","Live together"
            };
            App.LogFunc($"Relationship_Compability_5_SeeEachOther");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {

            await App.Navigation.PushAsync(new DateOfDating(BindingContext as CompabilityViewModel));
        }
    }
}