﻿using System;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.FutureChoice
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnterPartnerName 
	{
		public EnterPartnerName (FutureChoiceViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "FUTURE CHOICE";
            BindingContext = vm;
            App.LogFunc($"Relationship_Future_Choice_8_PartnerName");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new IdealHeightOfPartner(BindingContext as FutureChoiceViewModel));
	    }
	}
}