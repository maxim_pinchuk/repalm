﻿using System;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.FutureChoice
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnterNameStart 
	{
		public EnterNameStart()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "FUTURE CHOICE";
            BindingContext = new FutureChoiceViewModel();
            App.LogFunc($"Relationship_Future_Choice_1_Start");

		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new DateBirth(BindingContext as FutureChoiceViewModel));
	    }
	}
}