﻿using System;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.FutureChoice
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnterHeight 
    {
		public EnterHeight (FutureChoiceViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "FUTURE CHOICE";
            BindingContext = vm;
            App.LogFunc($"Relationship_Future_Choice_3_Height");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new EnterFeetSize(BindingContext as FutureChoiceViewModel));
	    }
	}
}