﻿using System;
using System.Collections.Generic;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.FutureChoice
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HowManyPartners 
	{
		public HowManyPartners (FutureChoiceViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    CurrentWindow = "FUTURE CHOICE";
            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "1","2-4","5-9", "10-19","20-30","More"
            };
            App.LogFunc($"Relationship_Future_Choice_7_ManyPartners");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new EnterPartnerName(BindingContext as FutureChoiceViewModel));
	    }
	}
}