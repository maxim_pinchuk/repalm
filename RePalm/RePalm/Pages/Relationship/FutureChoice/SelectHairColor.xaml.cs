﻿using System;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Controls;

namespace RePalm.Pages.Relationship.FutureChoice
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectHairColor 
    {
        private ImageButton PrevioslyClickedButton { get; set; }
		public SelectHairColor(FutureChoiceViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
            CurrentWindow = "FUTURE CHOICE";
            App.LogFunc($"Relationship_Future_Choice_5_HairColor");

		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        var imageButton = sender as ImageButton;
	        if (imageButton != null && PrevioslyClickedButton != imageButton)
	        {
	            if (PrevioslyClickedButton != null)
	            {
                    await PrevioslyClickedButton.ScaleTo(1, 200);

	            }
	            await imageButton.ScaleTo(1.3, 200);
	            PrevioslyClickedButton = imageButton;
	        }
	    }

        private async void Button_OnClicked1(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new HaveYouEverMarried(BindingContext as FutureChoiceViewModel));
        }
    }
}