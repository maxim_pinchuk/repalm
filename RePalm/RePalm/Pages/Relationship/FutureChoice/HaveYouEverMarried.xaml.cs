﻿using System;
using System.Collections.Generic;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.FutureChoice
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HaveYouEverMarried 
	{
		public HaveYouEverMarried (FutureChoiceViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "FUTURE CHOICE";
            BindingContext = vm;
		    AnswerOptions.ItemsSource = new List<string>()
		    {
		        "Yes","No"
            };
            App.LogFunc($"Relationship_Future_Choice_6_EverMarried");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new HowManyPartners( BindingContext as FutureChoiceViewModel));
	    }
	}
}