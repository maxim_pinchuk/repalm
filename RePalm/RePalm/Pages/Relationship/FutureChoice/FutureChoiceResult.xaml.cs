﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.FutureChoice
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FutureChoiceResult 
	{
		public FutureChoiceResult (FutureChoiceViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm.ResultViewModel;
            CurrentWindow = "FUTURE CHOICE";
            App.LogFunc($"Relationship_Future_Choice_10_Result");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PopToRootAsync();
        }
	}
}