﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.FutureChoice
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnterFeetSize 
	{
		public EnterFeetSize (FutureChoiceViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "FUTURE CHOICE";
            BindingContext = vm;
            App.LogFunc($"Relationship_Future_Choice_4_FeetSize");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new SelectHairColor(BindingContext as FutureChoiceViewModel));
        }
	}
}