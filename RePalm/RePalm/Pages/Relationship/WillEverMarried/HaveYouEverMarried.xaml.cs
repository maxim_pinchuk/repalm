﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.WillEverMarried
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HaveYouEverMarried 
	{
		public HaveYouEverMarried (WillEverMarryViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
		    CurrentWindow = "WILL I EVER GET MARRIED";
            AnswerOptions.ItemsSource = new List<string>()
		    {
		        "Yes","No"
            };
            App.LogFunc($"Relationship_Will_I_Get_Married_3_UEverMarried");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new HowManyPartnersYouHad(BindingContext as WillEverMarryViewModel));
	    }
	}
}