﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Relationship.WillEverMarried
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnterPartnerName 
	{
		public EnterPartnerName (WillEverMarryViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "WILL I EVER GET MARRIED";
            BindingContext = vm;
            App.LogFunc($"Relationship_Will_I_Get_Married_5_PartnerName");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new SelectEyesColor(BindingContext as WillEverMarryViewModel));
	    }
	}
}