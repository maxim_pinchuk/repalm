﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.Relationship;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Controls;

namespace RePalm.Pages.Relationship.WillEverMarried
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectEyesColor 
    {
        private ImageButton PrevioslyClickedButton { get; set; }
		public SelectEyesColor (WillEverMarryViewModel vm)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    BindingContext = vm;
            CurrentWindow = "WILL I EVER GET MARRIED";
            App.LogFunc($"Relationship_Will_I_Get_Married_6_EyeColor");
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        var imageButton = sender as ImageButton;
	        if (imageButton != null && PrevioslyClickedButton != imageButton)
	        {
	            if (PrevioslyClickedButton != null)
	            {
                    await PrevioslyClickedButton.ScaleTo(1, 200);
	            }
	            await imageButton.ScaleTo(1.3, 200);
	            PrevioslyClickedButton = imageButton;
	        }
	    }

        private async void Button_OnClicked1(object sender, EventArgs e)
        {
            await App.Navigation.PushAsync(new EnterHeight(BindingContext as WillEverMarryViewModel));
        }
    }
}