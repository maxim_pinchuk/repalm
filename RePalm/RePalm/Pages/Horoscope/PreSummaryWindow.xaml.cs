﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using RePalm.Client.ViewModels.PalmRecognition;
using RePalm.Models;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Horoscope
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PreSummaryWindow : CustomContentPage
    {
        public  string SunSign { get; set; }

		public PreSummaryWindow (DateTime birthDate, string name)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    UserName.Text = name;
		    base.CurrentWindow = "PERSONAL HOROSCOPE";
            string image = "";
		    switch (birthDate.Month)
		    {
                case 1:
                    if (birthDate.Day <= 19)
                    {
                        image = "capricorn.png";
                        SunSign = "capricorn";
                        UserZodiak.Text = "Capricorn";
                    }
                    else
                    {
                        image = "aquarius.png";
                        SunSign = "aquarius";
                        UserZodiak.Text = "Aquarius";
                    }
                break;
                case 2:
                    if (birthDate.Day <= 18)
                    {

                        image = "aquarius.png";
                        SunSign = "aquarius";
                        UserZodiak.Text = "Aquarius";
                    }
                    else
                    {
                        image = "pisces.png";
                        SunSign = "pisces";
                        UserZodiak.Text = "Pisces";
                    }
                    break;
                case 3:
                    if (birthDate.Day <=20)
                    {

                        image = "pisces.png";
                        SunSign = "pisces";
                        UserZodiak.Text = "Pisces";
                    }
                    else
                    {
                        image = "aries.png";
                        SunSign = "aries";
                        UserZodiak.Text = "Aries";
                    }
                    break;
                case 4:
                    if (birthDate.Day <= 20)
                    {
                        image = "aries.png";
                        SunSign = "aries";
                        UserZodiak.Text = "Aries";
                    }
                    else
                    {
                        image = "taurus.png";
                        SunSign = "taurus";
                        UserZodiak.Text = "Taurus";
                    }
                    break;
                case 5:
                    if (birthDate.Day<= 20)
                    {
                        image = "taurus.png";
                        SunSign = "taurus";
                        UserZodiak.Text = "Taurus";
                    }
                    else
                    {
                        image = "gemini.png";
                        SunSign = "gemini";
                        UserZodiak.Text = "Gemini";
                    }
                    break;
                case 6:
                    if (birthDate.Day <= 20)
                    {
                        image = "gemini.png";
                        SunSign = "gemini";
                        UserZodiak.Text = "Gemini";
                    }
                    else
                    {
                        image = "cancer.png";
                        SunSign = "cancer";
                        UserZodiak.Text = "Cancer";
                    }
                    break;
                case 7:
                    if (birthDate.Day <= 22)
                    {
                        image = "cancer.png";
                        SunSign = "cancer";
                        UserZodiak.Text = "Cancer";
                    }
                    else
                    {
                        image = "leo.png";
                        SunSign = "leo";
                        UserZodiak.Text = "Leo";
                    }
                    break;
                case 8:
                    if (birthDate.Day <= 22)
                    {
                        image = "leo.png";
                        SunSign = "leo";

                        UserZodiak.Text = "Leo";
                    }
                    else
                    {
                        image = "virgio.png";
                        SunSign = "virgio";
                        UserZodiak.Text = "Virgio";
                    }
                    break;
		        case 9:
		            if (birthDate.Day <= 23)
		            {

		                image = "virgio.png";
		                SunSign = "virgio";
                        UserZodiak.Text = "Virgio";
                    }
		            else
		            {
		                image = "libra.png";
		                SunSign = "libra";
                        UserZodiak.Text = "Libra";
                    }
		            break;
		        case 10:
		            if (birthDate.Day <= 23)
		            {
		                image = "libra.png";
		                SunSign = "libra";
                        UserZodiak.Text = "Libra";
                    }
		            else
		            {
		                image = "scorpio.png";
		                SunSign = "scorpio";
                        UserZodiak.Text = "Scorpio";

                    }
                    break;
                case 11:
                    if (birthDate.Day <= 21)
                    {
                        image = "scorpio.png";
                        SunSign = "scorpio";
                        UserZodiak.Text = "Scorpio";
                    }
                    else
                    {
                        image = "sagittarius.png";
                        SunSign = "sagittarius";
                        UserZodiak.Text = "Sagittarius";
                    }
                    break;
                case 12:
                    if (birthDate.Day<=21)
                    {
                        image = "sagittarius.png";
                        SunSign = "sagittarius";
                        UserZodiak.Text = "Sagittarius";
                    }
                    else
                    {
                        image = "capricorn";
                        SunSign = "capricorn";
                        UserZodiak.Text = "Capricorn";
                    }
                    break;
		    }
            HoroscopeSign.Source = image;
            App.LogFunc($"Horoscope_3_ShowInfo");

		}

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            App.LogFunc($"Horoscope_4_Daily");
            string res = "";
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(
                    $"http://horoscope-api.herokuapp.com/horoscope/today/{SunSign}");

                var content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<HoroscopeResponseModel>(content);
                res = result.horoscope.Replace("['", "");
                res = res.Replace("']", "");
            }
            if (res.Length <= 5)
            {
                await DisplayAlert("Sorry",
                    "Sorry, but all our prophets are busy now. Could you try again in 10 minutes?",
                    "Ok, I'll try later");
                await App.Navigation.PopToRootAsync();
            }
            else
            {
                Func<Task> onContinue = async () =>
                {
                    PurchaseLoader.IsRunning = true;
                    PurchaseButton.IsEnabled = false;
                    Func<Task> CommonErrorFunc = async () =>
                    {
                        await DisplayAlert("Oops",
                                "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                                "I'll try again in 5 minutes");
                        await App.Navigation.PopToRootAsync();
                    };
                    new PurchaseManager().PurchaseSubscription("horoscopesubscrition3",
                        async () =>
                        {

                            PurchaseLoader.IsRunning = false;
                            PurchaseButton.IsEnabled = true;
                            await App.Navigation.PushAsync(new Results(res));
                        },
                        CommonErrorFunc,
                        CommonErrorFunc,
                        async (InAppBillingPurchaseException purchaseEx) =>
                        {
                            await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                            await App.Navigation.PopToRootAsync(true);
                        },
                        CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                    );
                };

                await App.Navigation.PushAsync(
                                            new PurchaseSubscription(
                                                "Daily Horoscope",
                                                " - Subscription Information: Psychology Tests;",
                                                " - Weekly Subscription;",
                                                " - $3.99 per week; ",
                                                " - 3 days free trial to check out functionality;", onContinue));
            }
        }

        private async void Button2_OnClicked(object sender, EventArgs e)
        {
            App.LogFunc($"Horoscope_4_Weekly");
            string res = "";
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(
                    $"http://horoscope-api.herokuapp.com/horoscope/week/{SunSign}");

                var content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<HoroscopeResponseModel>(content);
                res = result.horoscope.Replace("['", "");
                res = res.Replace("']", "");
            }
            if (res.Length <= 5)
            {
                await DisplayAlert("Sorry",
                    "Sorry, but all our prophets are busy now. Could you try again in 10 minutes?",
                    "Ok, I'll try later");
                await App.Navigation.PopToRootAsync();
            }
            else
            {
                Func<Task> onContinue = async () =>
                {
                    PurchaseLoader.IsRunning = true;
                    PurchaseButton2.IsEnabled = false;
                    Func<Task> CommonErrorFunc = async () =>
                    {
                        await DisplayAlert("Oops",
                                "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                                "I'll try again in 5 minutes");
                        await App.Navigation.PopToRootAsync();
                    };
                    new PurchaseManager().PurchaseSubscription("horoscopesubscritionweek2",
                        async () =>
                        {

                            PurchaseLoader.IsRunning = false;
                            PurchaseButton2.IsEnabled = true;
                            await App.Navigation.PushAsync(new Results(res));
                        },
                        CommonErrorFunc,
                        CommonErrorFunc,
                        async (InAppBillingPurchaseException purchaseEx) =>
                        {
                            await DisplayAlert($"Error Code { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                            await App.Navigation.PopToRootAsync(true);
                        },
                        CommonErrorFunc,
                    () =>
                    {
                        PurchaseLoader.IsRunning = false;
                        PurchaseButton.IsEnabled = true;
                        return null;
                    }
                    );
                };

                await App.Navigation.PushAsync(
                                            new PurchaseSubscription(
                                                "Weekly Horoscope",
                                                " - Subscription Information: Psychology Tests;",
                                                " - Monthly Subscription;",
                                                " - 4.49 per month; ",
                                                " - 3 days free trial to check out functionality;", onContinue));
            }
        }
    }
}