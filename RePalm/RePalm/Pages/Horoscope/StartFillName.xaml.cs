﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Horoscope
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StartFillName : CustomContentPage
    {
		public StartFillName ()
		{
            InitializeComponent ();
		    base.CurrentWindow = "PERSONAL HOROSCOPE";
            NavigationPage.SetHasNavigationBar(this, false);
            App.LogFunc($"Horoscope_1_Start");
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new FillDateBirth(NameTextBox.Text));
	    }
	}
}