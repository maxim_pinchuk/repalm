﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RePalm.Client.ViewModels.PalmRecognition;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Horoscope
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Results : CustomContentPage
    {
		public Results (string res)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    base.CurrentWindow = "PERSONAL HOROSCOPE";
            Result.Text = res;
            App.LogFunc($"Horoscope_5_Result");
		}


	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PopToRootAsync();
	    }
	}
}