﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.Horoscope
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FillDateBirth : CustomContentPage
    {
        private  string Name { get; set; }
		public FillDateBirth (string name)
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    base.CurrentWindow = "PERSONAL HOROSCOPE";

            App.LogFunc($"Horoscope_2_Birthday");
            Name = name;
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        await App.Navigation.PushAsync(new PreSummaryWindow(AgePicker.Date,Name));
	    }
	}
}