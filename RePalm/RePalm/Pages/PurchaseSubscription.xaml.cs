﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PurchaseSubscription : ContentPage
    {
        public Func<Task> OnContinue { get; }
        

        public PurchaseSubscription(string title, string subscriptionTitle, string subscriptionLength, string subscritionPrice,
                                    string trialInfo, Func<Task> onContinue)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            Title1.Text = title;
            Title2.Text = subscriptionTitle;
            SubscriptionLength.Text = subscriptionLength;
            SubscriptionPrice.Text = subscritionPrice;
            PaymentInfo.Text = " - Payment of subscription will be charged to iTunes Account after your confirmation of purchase";
            ManagementInfo.Text = " - Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period." +
                " Account will be charged for renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal";
            TrialInfo.Text = trialInfo;
            OnContinue = onContinue;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            PurchaseLoader1.IsRunning = true;
            PurchaseButton1.IsEnabled = false;
            OnContinue();
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://palmistryterms.azurewebsites.net/"));
        }

        private void Button_Clicked_2(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://palmistrypolicy.azurewebsites.net/"));
        }
    }
}