﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.DecisionMaking;
using RePalm.Client.ViewModels.Numerology;
using RePalm.Pages.Numerology;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.DecisionMaking.MagicBall
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MagicBallStart : CustomContentPage
    {
		public MagicBallStart ()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    CurrentWindow = "MAGIC BALL";
            this.BindingContext = new MagicBallViewModel();
            App.LogFunc($"Magic_ball_Start");

		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
	        var context = this.BindingContext as MagicBallViewModel;
	        if (context != null && context.CanExecute)
	        {
	            var resultPage = new MagicBallResult()
	            {
	                BindingContext = context
	            };

	            await App.Navigation.PushAsync(resultPage);
	        }
        }
	}
}