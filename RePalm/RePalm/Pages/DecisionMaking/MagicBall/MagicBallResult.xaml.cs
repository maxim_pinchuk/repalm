﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.DecisionMaking.MagicBall
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MagicBallResult : CustomContentPage
    {
		public MagicBallResult ()
		{
            InitializeComponent ();
		    CurrentWindow = "MAGIC BALL";
            NavigationPage.SetHasNavigationBar(this, false);
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
        {
            App.LogFunc($"Magic_ball_Result");
	        await App.Navigation.PopToRootAsync();
	    }
	}
}