﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using RePalm.Client.ViewModels.DecisionMaking;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.DecisionMaking.YesOrNo
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class YesOrNoResult : CustomContentPage
    {
		public YesOrNoResult ()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    var viewModel = new YesOrNoViewModel();
		    this.CurrentWindow = "YES OR NO";
            this.BindingContext = viewModel;
            App.LogFunc($"Yes_Or_No");
            viewModel.GetResultCommand.Execute(null);
		}

	    private async void Button_OnClicked(object sender, EventArgs e)
	    {
            await App.Navigation.PopToRootAsync();
	    }
	}
}