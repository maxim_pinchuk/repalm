﻿using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.DecisionMaking
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DecisionMaking : CustomContentPage
    {
        public DecisionMaking()
        {
            InitializeComponent();
            CurrentWindow = "DECISION MAKING";

            App.LogFunc($"Desicion_Making_Main");
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}