﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.DecisionMaking;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages.DecisionMaking.FromOneToTen
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OneToTenResult : CustomContentPage
    {
		public OneToTenResult ()
		{
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		    var viewModel = new OneToTenViewModel();
		    this.BindingContext = viewModel;
		    this.CurrentWindow = "FROM 1 TO 10";
            viewModel.GetResultCommand.Execute(null);
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
        {
            App.LogFunc($"From_One_To_Ten_Result");
	        await App.Navigation.PopToRootAsync();
	    }
	}
}