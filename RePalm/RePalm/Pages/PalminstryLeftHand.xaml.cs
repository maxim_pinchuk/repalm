﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using RePalm.Client.ViewModels.PalmRecognition;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PalminstryLeftHand 
    {
        public PalminstryLeftHand(PalmistryViewModel viewModel)
        {
            InitializeComponent();
            CurrentWindow = "PALMISTRY";
            NavigationPage.SetHasNavigationBar(this, false);
            var s = new FormattedString();
            s.Spans.Add(new Span { Text = "Take a photo of your", FontAttributes = FontAttributes.None });
            s.Spans.Add(new Span
            {
                Text = " left ",
                FontAttributes = FontAttributes.Bold,
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
            });
            s.Spans.Add(new Span() { Text = "palm," });
            LeftPalmLabel.FormattedText = s;
            this.BindingContext = viewModel;
            App.LogFunc($"Palmistry_Left_Hand_Start");
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            var viewModel = this.BindingContext as PalmistryViewModel;
            viewModel.LeftImageBase64 = "";
            viewModel.RightImageBase64 = "";
            await App.Navigation.PushAsync(new Capture(viewModel.LeftHandPictureViewModel)
            {
                ImageOverlay = "palmLeft.png",
                NextPage = typeof(PalmistryGetResults),
                NextPageViewModel = viewModel
            });
        }
    }
}