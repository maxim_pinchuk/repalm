﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RePalm.Client.ViewModels.PalmRecognition;
using RePalm.Pages.Horoscope;

namespace RePalm.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PalmistryGetResults 
	{
        private bool canChangeText;

        public PalmistryGetResults (PalmistryViewModel palmistryViewModel)
		{
            InitializeComponent ();
		    CurrentWindow = "PALMISTRY";
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = palmistryViewModel;
            App.LogFunc($"Palmistry_Processing_Photo");
            try
            {
                   canChangeText = true;
                M();
                ResultPalmImageBanner.Source = palmistryViewModel.IsRightHand ? ImageSource.FromFile("PalmRightResult.png")
                    : ImageSource.FromFile("PalmLeftResult.png");
                palmistryViewModel.OnGetResult();
            }
		    catch 
		    {
		        DisplayAlert("Recognition fail",
		            "Seems that our algorythm do not see the lines on your palm. Please try once again with better light conditions.",
		            "Got it");
		    }
		}

        private async void M()
        {
            await ChangeLoadingText();
        }

        async Task ChangeLoadingText()
        {
            Device.StartTimer(new TimeSpan(0, 0, 4),  () =>
            {
                try
                {
                    var loadingTexts = new[]
                    {
                            "Building personal horoscope...",
                            "Processing the lines...",
                            "Performing numerological calculations...",
                            "Consulting a palmist specialist...",
                            "Checking the results...",
                            "Checking the secure connection...",
                            "Preparing results...",
                            "Drawing a schematic lines on the hand",
                            "Almost done...",
                            "Processing algorithm to recognise the lines...",
                    };
                    if (canChangeText)
                    {
                        var random = new Random();
                        LoaderText.Text = loadingTexts[random.Next(0, loadingTexts.Length - 1)];
                    }
                    return true;
                }
                catch
                {
                    canChangeText = false;
                    return true;
                }
            });
        }

	    private async void Button_OnClicked(object sender, EventArgs e)
        {
            Func<Task> onContinue = async () =>
            {
                canChangeText = false;
                if ((BindingContext as PalmistryViewModel).IsOldSession)
                {
                    await App.Navigation.PushAsync(new PalmistryResultsDisplay(BindingContext as PalmistryViewModel));
                }
                else
                {
                    PurchaseBtn.IsEnabled = false;
                    Func<Task> CommonErrorFunc = async () =>
                    {
                        await DisplayAlert("Oops",
                                "We are so sorry, but we couldn't connect to billing or your device could be offline...",
                                "I'll try again in 5 minutes");
                        await App.Navigation.PopToRootAsync();
                    };
                    new PurchaseManager().PurchaseSubscription("palmistry_subscrition2",
                        async () =>
                        {
                            PurchaseBtn.IsEnabled = true;
                            await App.Navigation.PushAsync(new PalmistryResultsDisplay(BindingContext as PalmistryViewModel));
                        },
                    CommonErrorFunc,
                        CommonErrorFunc,
                        async (InAppBillingPurchaseException purchaseEx) =>
                        {
                            await DisplayAlert($"Error Code: { purchaseEx.PurchaseError.ToString()}", "Eror occured. Please send this screenshot to our developers", "ok");
                            await App.Navigation.PopToRootAsync(true);
                        },
                        CommonErrorFunc,
                    () =>
                    {
                        PurchaseBtn.IsEnabled = true;
                        return null;
                    }
                    );
                }
            };

            await App.Navigation.PushAsync(
                                        new PurchaseSubscription(
                                            "Palmistry",
                                            " - Subscription Information: Subscription to Palmistry;",
                                            " - Weekly Subscription;",
                                            " - $8.99 per week",
                                            "", onContinue));
        }
	}
}