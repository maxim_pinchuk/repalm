﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RePalm.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutAppPage 
    {
        public AboutAppPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWindow = "ABOUT APP";
            App.LogFunc($"About_App");
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("https://palmistrypolicy.azurewebsites.net/"));
        }

        void Handle_Clicked2(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("https://palmistryterms.azurewebsites.net/"));
        }
    }
}