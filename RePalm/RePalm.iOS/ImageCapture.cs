﻿using RePalm.Models;
using Xamarin.Forms;

public class ImageCapture : IImage
{
    public byte[] ImageBytes { get; set; }

    public ImageSource ImageSource { get; set; }
    public ImageSource AsImageSource()
    {
        return ImageSource;
    }
}