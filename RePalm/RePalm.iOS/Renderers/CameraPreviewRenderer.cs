﻿using System;
using System.IO;
using AVFoundation;
using Foundation;
using RePalm.iOS;
using RePalm.iOS.Renderers;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CameraPreview), typeof(CameraPreviewRenderer))]
namespace RePalm.iOS.Renderers
{
    public class CameraPreviewRenderer : ViewRenderer<CameraPreview, UICameraPreview>
    {
        UICameraPreview uiCameraPreview;

        protected override void OnElementChanged(ElementChangedEventArgs<CameraPreview> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                uiCameraPreview = new UICameraPreview();
                SetNativeControl(uiCameraPreview);
            }
            if (e.OldElement != null)
            {
                // Unsubscribe
                uiCameraPreview.Tapped -= OnCameraPreviewTapped;
            }
            else{
                var preview = e.NewElement;
                preview.PictureRequired += preview_PictureRequired;

            }
            if (e.NewElement != null)
            {
                // Subscribe
                uiCameraPreview.Tapped += OnCameraPreviewTapped;
            }
        }

        /// <summary>
        /// called when the picture is required
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void preview_PictureRequired(object sender, EventArgs e)
        {
            CameraPreview preview = sender as CameraPreview;
            if (preview != null)
            {
                var videoConnection = uiCameraPreview.OutPut.ConnectionFromMediaType(AVMediaType.Video);

                var sampleBuffer = await uiCameraPreview.OutPut.CaptureStillImageTaskAsync(videoConnection);
                var jpegImageAsNsData = AVCaptureStillImageOutput.JpegStillToNSData(sampleBuffer);
                var arr = jpegImageAsNsData.ToArray();
                MemoryStream ms = null;
                try
                {
                    ms = new MemoryStream(arr);
                    preview.OnPictureTaken(new ImageCapture
                    {
                        ImageSource = ImageSource.FromStream(() => ms),
                        ImageBytes = arr
                    });
                }
                catch (Exception error)
                {
                    if (ms != null)
                    {
                        ms.Dispose();
                    }
                    throw;
                }

            }
        }

        void OnCameraPreviewTapped(object sender, EventArgs e)
        {
          /*  if (uiCameraPreview.IsPreviewing)
            {
                uiCameraPreview.CaptureSession.StopRunning();
                
                uiCameraPreview.IsPreviewing = false;
            }
            else
            {
                uiCameraPreview.CaptureSession.StartRunning();
                uiCameraPreview.IsPreviewing = true;
            }
            */
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Control.CaptureSession.Dispose();
                Control.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
