using System.IO;
using RePalm.Droid.Models;
using RePalm.Models;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidImage))]

namespace RePalm.Droid.Models
{
    public class AndroidImage : IImage
    {
        public ImageSource ImageSource { get; set; }

        #region IImage �����o�[

        public ImageSource AsImageSource()
        {
            return ImageSource;
        }

        public byte[] ImageBytes { get; set; }

        #endregion
    }
}