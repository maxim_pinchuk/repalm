using System;
using Android.Hardware;

namespace RePalm.Droid.Renderers
{
    public class DelegatePictureCallback : Java.Lang.Object, Android.Hardware.Camera.IPictureCallback
    {
        public Action<byte[], Android.Hardware.Camera> PictureTaken { get; set; }

        #region IPictureCallback �����o�[

        public void OnPictureTaken(byte[] data, Android.Hardware.Camera camera)
        {
            if (PictureTaken != null)
            {
                PictureTaken(data, camera);
            }
        }
        #endregion
    }
}