using System;
using System.IO;
using System.Linq;
using Android;
using Android.App;
using Android.Content.PM;
using Android.Hardware;
using Android.Media;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Views;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using RePalm.Droid.Renderers;
using RePalm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Permission = Plugin.Permissions.Abstractions.Permission;

[assembly: ExportRenderer(typeof(CameraPreview), typeof(CameraPreviewRenderer))]

namespace RePalm.Droid.Renderers
{
    // To use generic version of the ViewRenderer, Xamarin.Forms have to be updated to 1.2.2.*
    public class CameraPreviewRenderer : ViewRenderer<CameraPreview, SurfaceView>, ISurfaceHolderCallback
    {
        Android.Hardware.Camera camera_ = null;

        protected override void OnElementChanged(ElementChangedEventArgs<CameraPreview> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                // get CamerPreview object and set event handler
                var preview = e.NewElement;
                preview.PictureRequired += preview_PictureRequired;

                // create and set surface view
                var surfaceView = new SurfaceView(Context);
                surfaceView.Holder.AddCallback(this);
                SetNativeControl(surfaceView);
            }
        }

        /// <summary>
        /// called when the picture is required
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void preview_PictureRequired(object sender, EventArgs e)
        {
            CameraPreview preview = sender as CameraPreview;
            if (camera_ != null && preview != null)
            {
                camera_.TakePicture(null, null, new DelegatePictureCallback
                {
                    PictureTaken = (data, camera) =>
                    {
                        // write jpeg data into a memory stream
                        MemoryStream ms = null;
                        try
                        {
                            ms = new MemoryStream(data.Length);
                            ms.Write(data, 0, data.Length);
                            ms.Flush();
                            ms.Seek(0, SeekOrigin.Begin);
                            // load image source from stream
                            preview.OnPictureTaken(new Models.AndroidImage
                            {
                                ImageSource = ImageSource.FromStream(() => ms),
                                ImageBytes = ms.ToArray()
                            });

                            // NOTE: Do not dispose memory stream if it succeeded.
                            // ImageSource is loaded in background so ms should not be disposed immediately.
                        }
                        catch
                        {
                            if (ms != null)
                            {
                                ms.Dispose();
                            }
                            throw;
                        }
                    }
                });
            }
        }

        #region ISurfaceHolderCallback �����o�[

        public void SurfaceChanged(ISurfaceHolder holder, Android.Graphics.Format format, int width, int height)
        {
            if (camera_ != null)
            {
                camera_.StopPreview();
                camera_.SetPreviewDisplay(holder);
                camera_.StartPreview();
            }
        }

        public async void SurfaceCreated(ISurfaceHolder holder)
        {
            try
            {
                if (await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera) != PermissionStatus.Granted)
                {
                     var res = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera });
                }
                camera_ = Android.Hardware.Camera.Open();

                
                var params_ = camera_.GetParameters();
                var size = params_.SupportedPictureSizes.FirstOrDefault(x => x.Width == 1280);
                if (size == null)
                 {
                    size = params_.SupportedPictureSizes.LastOrDefault();
                }
                params_.JpegQuality = CameraProfile.GetJpegEncodingQualityParameter(CameraQuality.Medium);
                params_.SetPictureSize(size.Width, size.Height);
                params_.FocusMode = Android.Hardware.Camera.Parameters.FocusModeContinuousPicture;
                camera_.SetParameters(params_);
                camera_.SetDisplayOrientation(90);
            }
            catch (Exception e)
            {
                camera_ = null;
            }

            if (camera_ != null)
            {
                camera_.SetPreviewDisplay(holder);
                camera_.StartPreview();
            }
        }

        public void SurfaceDestroyed(ISurfaceHolder holder)
        {
            if (camera_ != null)
            {
                camera_.StopPreview();
                camera_.Release();
                camera_.Dispose();
                camera_ = null;
            }
        }

        #endregion
    }
}