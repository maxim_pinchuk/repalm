﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Firebase.Analytics;
using Java.Security;
using Plugin.InAppBilling;
using Plugin.Permissions;
using Xamarin.Facebook;
using Xamarin.Facebook.AppEvents;
using Xamarin.Forms;
using XLabs.Forms;
using XLabs.Platform;
using Permission = Android.Content.PM.Permission;

namespace RePalm.Droid
{
    [Activity(Label = "Palmistry", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : XFormsApplicationDroid
    {
        public static FirebaseAnalytics FirebaseAnalytics;
        //   FirebaseAnalytics firebaseAnalytics;
        protected override void OnCreate(Bundle bundle)
        {
            // TabLayoutResource = Resource.Layout.Tabbar;
            // ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(bundle);
            //     firebaseAnalytics = FirebaseAnalytics.GetInstance(this);
            FacebookSdk.ApplicationId = "1422430684545790";
            AppEventsLogger.ActivateApp(this);
            App.LogFunc = (string s) =>
            {
                AppEventsLogger.NewLogger(Android.App.Application.Context).LogEvent(s);
                FirebaseAnalytics.LogEvent(s, Bundle.Empty);
                return true;
            };
            Forms.Init(this, bundle);
            LoadApplication(new App());
            FirebaseAnalytics = FirebaseAnalytics.GetInstance(this);
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        }
        
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
             base.OnActivityResult(requestCode, resultCode, data);
            InAppBillingImplementation.HandleActivityResult(requestCode, resultCode, data);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            [GeneratedEnum] Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}