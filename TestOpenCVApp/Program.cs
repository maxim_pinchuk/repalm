﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using TestOpenCVApp.PreProcessor;
using TestOpenCVApp.RoiExtracton;
using System.IO;
using Point = OpenCvSharp.Point;
using Size = OpenCvSharp.Size;
using TestOpenCVApp.LinesReading;
using ImageMagick;

namespace TestOpenCVApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MagickImage img3 = new MagickImage("test1.png");

            img3.Quantize(new QuantizeSettings()
            {
                Colors = 128
            });
            var scale = (double)img3.Height / 1000;
            img3.Resize(1000, (int)(img3.Width / scale));
            img3.Flip();
            img3.Write("test2.png");

            var rightHandImagePath = "test2.png";
            var palmIMage = new Mat(rightHandImagePath);
            var segmentedImagePath = GetSegmentedImage(palmIMage);
            var mask = new Mat(segmentedImagePath);
            var background = mask;
            IPreprocessor preprocessor = new Preprocessor();
            IRoiExtractor roiExtractor = new SquareRoiExtractor(palmIMage, background, preprocessor);
            var extractionResult = roiExtractor.DoExtraction();
            var keypoints = extractionResult.OriginalImageKeyPoints;
            using (IMagickImage image = new MagickImage("test2.png"))
            {
                double coef = 563.0 / image.Height;
                //image.BrightnessContrast(new Percentage(22), new Percentage());
               // image.Blur(180, 4);
                //--------draw heart line
                var heartLine = new MagickImage("palmRightHeartLine.png");
                heartLine.Resize(heartLine.Width,
                  (int)((keypoints.RingLittleFingerPoint.Y - keypoints.IndexMiddleFingerPoint.Y) / (1.7/coef)));
                image.Draw(new DrawableComposite(
                    keypoints.IndexMiddleFingerPoint.X + 10
                    , (int)((keypoints.IndexMiddleFingerPoint.Y + keypoints.MiddleRingFingerPoint.Y) / (2.5 - (0.9 - coef)))
                    , heartLine));


                //---------------draw life line
                var lifeLine = new MagickImage( "palmRightLifeLine.png");
                var lifeTopPoint = new Point2d(
                    (((keypoints.ThumbIndexFingerPoint.X + keypoints.TopLeftRoiPoint.X) / 2) +
                     keypoints.IndexMiddleFingerPoint.X) / 2
                    ,
                    (keypoints.ThumbIndexFingerPoint.Y + keypoints.IndexMiddleFingerPoint.Y) / 2
                );
                lifeLine.Resize((lifeLine.Width),
                    (int)((keypoints.RingLittleFingerPoint.Y - keypoints.TopLeftRoiPoint.Y) / (1.2 / coef)));
                image.Draw(new DrawableComposite(
                    lifeTopPoint.X +5 ,
                    lifeTopPoint.Y  + (coef < 1 ? 15 / coef : 1),
                    lifeLine
                ));

                var headTopPoint = new Point2d(
                   (keypoints.ThumbIndexFingerPoint.X + keypoints.TopLeftRoiPoint.X) / 2,
                   (keypoints.ThumbIndexFingerPoint.Y + keypoints.IndexMiddleFingerPoint.Y) / 2
               );

                var resizeWidth = keypoints.CenterPoint.X - headTopPoint.X;
                var headLine = new MagickImage("palmRightHeadLine.png");
                headLine.Resize(
                    (int)(resizeWidth *coef),
                    (int)((keypoints.RingLittleFingerPoint.Y - keypoints.ThumbIndexFingerPoint.Y)));
                image.Draw(new DrawableComposite(
                    headTopPoint.X + 15
                    ,(int)( headTopPoint.Y + 5 /coef)
                    , headLine));


                image.Write("test44.png");
            }   

                return;
           // string folderPath = "1ff46ce2-e038-4a02-9a6a-67daa91e911f";


           // var palmIMage = new Mat(string.Join("/", folderPath, "right.png"));
           // if (palmIMage.Width > 1600)
           // {
           //     var scale = (double)palmIMage.Width / 1600;
           //     Cv2.Resize(palmIMage, palmIMage, new Size(1600, palmIMage.Height / scale));
           //     palmIMage.SaveImage(string.Join("/", folderPath, "right.png"));
           // }
           // var backgroundImage = new Mat(new Size(palmIMage.Width, palmIMage.Height), MatType.CV_8UC3, new Scalar(0, 0, 0));
           // backgroundImage.SaveImage(string.Join("/", folderPath, "back.png"));
           // var overlayImage = new Mat("palmRight.png", ImreadModes.Unchanged);
           // var newHeight = palmIMage.Height * 0.8;
           // var scaleTimes = newHeight / overlayImage.Height;
           // Cv2.Resize(overlayImage, overlayImage, new Size(overlayImage.Width * scaleTimes, newHeight));
           // overlayImage.SaveImage(string.Join("/", folderPath, "overlay.png"));
           // Image imageBackground = Image.FromFile(string.Join("/", folderPath, "back.png"));
           // Image imageOverlay = Image.FromFile(string.Join("/", folderPath, "overlay.png"));


           // Image img = new Bitmap(imageBackground.Width, imageBackground.Height);
           // using (Graphics gr = Graphics.FromImage(img))
           // {
           //     gr.DrawImage(imageBackground, new PointF(0, 0));
           //     gr.DrawImage(imageOverlay, new System.Drawing.Point(palmIMage.Width / 2 - overlayImage.Width / 2, palmIMage.Height / 2 - overlayImage.Height / 2));
           // }
           // img.Save(string.Join("/", folderPath, "trueBack.png"), System.Drawing.Imaging.ImageFormat.Png);


           // Mat mMask = Cv2.ImRead(string.Join("/", folderPath, "trueBack.png"), 0);
           // Mat destination = new Mat();
           // palmIMage.CopyTo(destination, mMask);


           // var imgPath = string.Join("/", folderPath, "right.png");
           // var img_ = new Mat(imgPath);
           // var background = mMask;
           // IPreprocessor preprocessor = new Preprocessor();
           // IRoiExtractor roiExtractor = new SquareRoiExtractor(img_, background, preprocessor);
           // var extractionResult = roiExtractor.DoExtraction();
           // extractionResult.BinaryRoiArea.SaveImage("dst.png");
           ////var palmistryResponseService = new PalmistryResponseService(img_, imgPath, folderPath);
           // //var response = palmistryResponseService.GeneratePalmistryResponse(extractionResult);

           // return;
           // // // Mat background = new Mat("trueBack.png", 0);
           // // // Mat res = new Mat();
           // // // palmIMage.CopyTo(res,background);
           // // //res.SaveImage("dst.png");
           // // // Mat frame = new Mat();
           // // //Cv2.GaussianBlur(palmIMage, frame, new Size(5, 5),0);
           // // //Mat mask = new Mat();
           // // //Cv2.Absdiff(frame,background,mask);
           // // //mask.SaveImage("dst.png");


           // // //Cv2.CvtColor(mask,mask,ColorConversionCodes.BGR2GRAY);
           // // //Cv2.Threshold(mask, mask, 125, 255, ThresholdTypes.Binary);
           // // //var  kernel  =Cv2.GetStructuringElement(MorphShapes.Rect, new Size(5, 5));
           // // //Cv2.MorphologyEx(mask,mask,MorphTypes.Close,kernel);
           // // //mask.SaveImage("dst1.png");
           // // //Mat labImage = new Mat();
           // // //Cv2.CvtColor(palmImage,labImage,ColorConversionCodes.BGR2Lab);
           // // //var labPlanes =new Mat[3];
           // // //Cv2.Split(labImage, out labPlanes);
           // // //var clahe = Cv2.CreateCLAHE(2,new Size(9,9));
           // // ////clahe.SetClipLimit(8);
           // // //Mat dst = new Mat();
           // // //clahe.Apply(labPlanes[0],dst);

           // // //dst.CopyTo(labPlanes[0]);
           // // //Cv2.Merge(labPlanes,labImage);
           // // //Mat imageClahe = new Mat();
           // // //Cv2.CvtColor(labImage,imageClahe,ColorConversionCodes.Lab2BGR);
           // // //imageClahe.SaveImage("source11.jpg");

           // return;

           // // var img5 = new Mat("dst.png",ImreadModes.GrayScale);

           // // var hlines = Cv2.HoughLinesP(img5, 1, Math.PI / 180, 10, 5, 3);
           // // var img6 = new Mat();
           // // var sortedLines = hlines.OrderBy(l => l.P1.X);
           // // List<List<Point>> principleLines = new List<List<Point>>();

           // // var point1 = new Point(34,117);
           // // var point2 = new Point(35,118);
           // // var val2 = point1.DistanceTo(point2);
           // // var val3 = point2.DistanceTo(point1);
           // //var fgd= Math.Round(val2, 2);
           // // //2.24
           // // //1.41
           // // var val4 = point1.CrossProduct(point2);
           // // List<Point> usedPoints = new List<Point>();
           // // for (var j = 0; j <= img5.Rows; j++)
           // // {
           // //     for (int i = 0; i <= img5.Cols; i++)
           // //     {
           // //         var val = img5.At<byte>(j, i);
           // //         if (val == 255)
           // //         {
           // //             var newPoint = new Point(i, j);

           // //             //var line = principleLines.FirstOrDefault(x => x.Any(l => (Math.Abs(l.X - i) == 0 &&
           // //             //                                                       Math.Abs(l.Y - j) <=3) ||
           // //             //                                                     (Math.Abs(l.X - i) <= 3 &&
           // //             //                                                       Math.Abs(l.Y - j) == 0)));

           // //             //all possible options of neighbour points
           // //             var selectedLine = principleLines.Where(l => l.Any(p =>
           // //                 !usedPoints.Contains(p) &&
           // //                ( Math.Abs(p.DistanceTo(newPoint)) == 1 ||
           // //                 Math.Abs(p.DistanceTo(newPoint)) == 2 ||
           // //                 Math.Abs(p.DistanceTo(newPoint)) == 3 ||
           // //                 Math.Abs(Math.Round(p.DistanceTo(newPoint), 2)) == 1.41 ||
           // //                 Math.Abs(Math.Round(p.DistanceTo(newPoint), 2)) == 2.24))).OrderByDescending(x=>x.Count)
           // //                 .FirstOrDefault();

           // //             if (selectedLine != null)
           // //             {
           // //                 selectedLine.Add(newPoint);
           // //                 //foreach (var line in selectedLines)
           // //                 //{
           // //                 //    line.Add(newPoint);
           // //                 //}
           // //             }
           // //             else
           // //             {
           // //                 principleLines.Add(new List<Point>() {new Point(i, j)});
           // //             }
           // //         }
           // //     }
           // // }
           // // //var orderedPrincipleLines = principleLines.OrderByDescending(x => x.Count).ToList();
           // // //for (int i = 0; i < orderedPrincipleLines.Count-1; i++)
           // // //{
           // // //    var currentLine = orderedPrincipleLines[i];
           // // //    foreach (var point in currentLine)
           // // //    {
           // // //        
           // // //    }
           // // //}
           // // foreach (var principleLine
           // //     in principleLines.OrderByDescending(x => x.Count).Take(3))
           // // {
           // //     Scalar color = Scalar.RandomColor(); 
           // //     foreach (var point in principleLine)
           // //     {

           // //         Cv2.Circle(img6, point.X, point.Y, 0, color);
           // //         img6.SaveImage("12.png");
           // //     }
           // // }
           // // //foreach (var lineSegmentPoint in sortedLines)
           // // //{
           // // //    var line = principleLines.FirstOrDefault(x => x.Any(l => l.P2.X == lineSegmentPoint.P1.X ));
           // // //    if (line !=  null)
           // // //    {
           // // //        line.Add(lineSegmentPoint);
           // // //    }
           // // //    else
           // // //    {
           // // //        principleLines.Add(new List<LineSegmentPoint>(){lineSegmentPoint});
           // // //    }
           // // //    // Cv2.Line(img6, lineSegmentPoint.P1, lineSegmentPoint.P2, Scalar.Red, 1);
           // // //    // img6.SaveImage("11.png");
           // // //}
           // // //foreach (var item in principleLines.Where(x=>x.Count >1))
           // // //{
           // // //    foreach (var lineSegmentPoint in item)
           // // //    {
           // // //        Cv2.Line(img6, lineSegmentPoint.P1, lineSegmentPoint.P2, Scalar.Red, 2);
           // // //        img6.SaveImage("11.png");

           // // //    }
           // // //}
           // //// return;
           // // foreach (var line in hlines)
           // // {
           // //     Cv2.Line(img6, line.P1, line.P2, Scalar.White, 5);
           // //     img6.SaveImage("11.png");
           // // }
           // // var imgsd = GetImageSkeleton(img6);
           // // imgsd.SaveImage("12.png");

           // // img6.SaveImage("11.png");
           // //  return;
           // OpenCvSharp.Mat img3 = new Mat("prepreprocess.png", ImreadModes.Unchanged);

           // //  img3 =  Program.ExtractForeground(img3);
           // img3.SaveImage("dst1.png");
           // Mat temp_img = new Mat();
           // Cv2.CvtColor(img3, img3, ColorConversionCodes.BGR2GRAY);
           // // Cv2.Blur(img3, img3, new Size(1,1));
           // Cv2.AdaptiveThreshold(img3, temp_img, 255, AdaptiveThresholdTypes.MeanC, ThresholdTypes.BinaryInv, 11, 5);
           // //   Cv2.Threshold(img3, temp_img, 138, 255, ThresholdTypes.Binary);
           // // Cv2.Canny(img3, temp_img, 10,100);
           // temp_img.SaveImage("dst2.png");
           // var lines = Cv2.HoughLinesP(temp_img, 1, Math.PI / 180, 20, 3, 3);
           // // var lines = Cv2.HoughLinesP(temp_img, 3, Math.PI / 180, 100, 100, 20);
           // var img4 = new Mat("dst1.png", ImreadModes.Unchanged);
           // foreach (var line in lines)
           // {
           //     Cv2.Line(img4, line.P1, line.P2, Scalar.Red, 2);
           //     //img4.SaveImage("dst.png");
           // }
           // img4.SaveImage("dst.png");

        }

        private static string GetSegmentedImage(Mat palmIMage)
        {
            var backgroundImage = new Mat(new OpenCvSharp.Size(palmIMage.Width, palmIMage.Height), MatType.CV_8UC3, new Scalar(0, 0, 0));
            backgroundImage.SaveImage(string.Join("/", "back.png"));
            var overlayImage = new Mat(string.Join("/", "palmRight.png"), ImreadModes.Unchanged);
            var newHeight = palmIMage.Height * 0.8;
            var scaleTimes = newHeight / overlayImage.Height;
            Cv2.Resize(overlayImage, overlayImage, new OpenCvSharp.Size(overlayImage.Width * scaleTimes, newHeight));
            overlayImage.SaveImage(string.Join("/", "overlay.png"));
            var segmentedImageFolderPath = string.Join("/", "trueBack.png");
            using (IMagickImage image = new MagickImage(string.Join("/", "back.png")))
            {
                image.Draw(new DrawableComposite(palmIMage.Width / 2 - overlayImage.Width / 2, palmIMage.Height / 2 - overlayImage.Height / 2,
                    new MagickImage(string.Join("/", "overlay.png"))));
                image.Write(segmentedImageFolderPath);
            }
            return segmentedImageFolderPath;
        }

        public Mat GetBinaryImage(Mat palmIMage)
        {

            var backgroundImage = new Mat(new Size(palmIMage.Width, palmIMage.Height), MatType.CV_8UC3, new Scalar(0, 0, 0));
            backgroundImage.SaveImage("backTemp.png");
            var overlayImage = new Mat("palmLeftMask.png", ImreadModes.Unchanged);
            var newHeight = palmIMage.Height * 0.8;
            var scaleTimes = newHeight / overlayImage.Height;
            Cv2.Resize(overlayImage, overlayImage, new Size(overlayImage.Width * scaleTimes, newHeight));
            overlayImage.SaveImage("overlayTemp.png");
            Image imageBackground = Image.FromFile("backTemp.png");
            Image imageOverlay = Image.FromFile("overlayTemp.png");
            Image img = new Bitmap(imageBackground.Width, imageBackground.Height);
            using (Graphics gr = Graphics.FromImage(img))
            {
                gr.DrawImage(imageBackground, new PointF(0, 0));
                gr.DrawImage(imageOverlay, new System.Drawing.Point(palmIMage.Width / 2 - overlayImage.Width / 2, palmIMage.Height / 2 - overlayImage.Height / 2));
            }
            img.Save("trueBackTemp.png", System.Drawing.Imaging.ImageFormat.Png);
            var binaryImage = new Mat("trueBackTemp.png");
            return null;
        }

        public Mat MatExtractPalmWithOverlay(Mat palmImage, Mat binaryImage)
        {
            Mat mMask = Cv2.ImRead("trueBack.png", 0);
            Mat destination = new Mat();
            palmImage.CopyTo(destination, mMask);
            destination.SaveImage("dst.png");
            return destination;
        }

        public Mat MatProcessImageToStartPalmReading(string overlayImageName, string pathToImage, string pathToImageFolder)
        {
            var palmIMage = new Mat(pathToImage);
            var backgroundImage = new Mat(new Size(palmIMage.Width, palmIMage.Height), MatType.CV_8UC3, new Scalar(0, 0, 0));
            backgroundImage.SaveImage(string.Join("/", pathToImageFolder, "backgroundPure.png"));
            var overlayImage = new Mat(overlayImageName, ImreadModes.Unchanged);
            var newHeight = palmIMage.Height * 0.8;
            var scaleTimes = newHeight / overlayImage.Height;
            Cv2.Resize(overlayImage, overlayImage, new Size(overlayImage.Width * scaleTimes, newHeight));
            overlayImage.SaveImage(string.Join("/", pathToImageFolder, "resizedCustomOverlay.png"));
            Image imageBackground = Image.FromFile(string.Join("/", pathToImageFolder, "backgroundPure.png"));
            Image imageOverlay = Image.FromFile(string.Join("/", pathToImageFolder, "resizedCustomOverlay.png"));
            Image img = new Bitmap(imageBackground.Width, imageBackground.Height);
            using (Graphics gr = Graphics.FromImage(img))
            {
                gr.DrawImage(imageBackground, new PointF(0, 0));
                gr.DrawImage(imageOverlay, new System.Drawing.Point(palmIMage.Width / 2 - overlayImage.Width / 2, palmIMage.Height / 2 - overlayImage.Height / 2));
            }
            var background = string.Join("/", pathToImageFolder, "backgroundToCrop.png");
            img.Save(background, System.Drawing.Imaging.ImageFormat.Png);
            //return;

            Mat mMask = Cv2.ImRead(background, 0);
            Mat preprocesscedImage = new Mat();
            palmIMage.CopyTo(preprocesscedImage, mMask);
            preprocesscedImage.SaveImage(
                string.Join("/", pathToImageFolder, "prepreprocess.png"));
            return preprocesscedImage;
        }

        //private static void EdgeDetect(Mat channel)
        //{
        //    int sobelx = 0;
        //    Cv2.Sobel(channel, sobelx, MatType.CV_16S, 1, 0);
        //    Mat sobelY = new Mat();
        //    Cv2.Sobel(channel, sobelY, MatType.CV_16S, 0, 1);
        //    var sobel = System.Math.Sqrt(sobelx * sobelx + sobelY * sobelY);

        //}

        private static Mat ExtractForeground(Mat src)
        {
            OpenCvSharp.Mat src2 = new Mat();

            Cv2.CvtColor(src, src2, ColorConversionCodes.BGR2GRAY);
            Cv2.GaussianBlur(src2, src2, new Size(5, 5), 0);
            Cv2.Threshold(src2, src2, 125, 255, ThresholdTypes.Binary);
            Point[][] contours = null;
            HierarchyIndex[] ierarchy = null;
            Cv2.FindContours(src2, out contours, out ierarchy, RetrievalModes.Tree, ContourApproximationModes.ApproxNone, new Point(0, 0));
            var vectors = contours.Select(x => new VectorOfPoint(x));
            var maxContour = vectors.OrderByDescending(x => x.Size).FirstOrDefault();
            var maxVectorIndex = Array.IndexOf(contours, contours.FirstOrDefault(x => new VectorOfPoint(x).Size == maxContour.Size));
            Mat img3 = new Mat(src2.Size(), MatType.CV_8UC1, new Scalar(0));
            Cv2.DrawContours(img3, contours, maxVectorIndex, new Scalar(255), -1);
            img3.SaveImage("dst-test.png");
            src.CopyTo(img3, img3);
            return img3;
        }

        private static Mat GetImageSkeleton(Mat source)
        {
            var sourceImg = new Mat("11.png", ImreadModes.GrayScale);
            //Cv2.Blur(sourceImg, sourceImg, new Size(3, 3));
            Cv2.Threshold(sourceImg, sourceImg, 127, 255, ThresholdTypes.Binary);
            sourceImg.SaveImage("test.png");
            var skeletonImg = new Mat(sourceImg.Size(), MatType.CV_8UC1, new Scalar(0));
            var skeletonTemp = new Mat(sourceImg.Size(), MatType.CV_8UC1);
            var skeletonElement = Cv2.GetStructuringElement(MorphShapes.Cross, new Size(3, 3));
            var skeletonDone = false;
            while (!skeletonDone)
            {
                Cv2.MorphologyEx(sourceImg, skeletonTemp, MorphTypes.Open, skeletonElement);
                Cv2.BitwiseNot(skeletonTemp, skeletonTemp);
                Cv2.BitwiseAnd(sourceImg, skeletonTemp, skeletonTemp);
                Cv2.BitwiseOr(skeletonImg, skeletonTemp, skeletonImg);
                Cv2.Erode(sourceImg, sourceImg, skeletonElement);

                double min = 0;
                double max;
                Cv2.MinMaxLoc(sourceImg, out min, out max);
                skeletonDone = (max == 0);
            }
            return skeletonImg;
        }


    }
}
