﻿using OpenCvSharp;

namespace TestOpenCVApp.FeatureExtraction
{
    /// <summary>
    /// interface for representing a biometric feature
    /// 
    /// </summary>
    public interface IFeature
    {
        /// <summary>
        /// Returns the image representation of the feature
        /// </summary>
        /// <returns></returns>
        Mat GetImageRepresentation();
    }
}