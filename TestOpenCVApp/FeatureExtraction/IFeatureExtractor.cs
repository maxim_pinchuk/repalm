﻿using OpenCvSharp;

namespace TestOpenCVApp.FeatureExtraction
{
    /// <summary>
    /// The IFeatureExtractor class is an interferace, designed to do feature extraction
    /// </summary>
    public interface IFeatureExtractor
    {
        /// <summary>
        ///  Extracts the feature, based on the given ROI
        /// </summary>
        /// <param name="roi"> The extracted ROI</param>
        /// <returns></returns>
        Mat DoFeatureExtraction(Mat roi);
    }
}