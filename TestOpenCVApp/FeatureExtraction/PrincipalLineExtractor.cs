﻿using System.Collections.Generic;
using System.Linq;
using OpenCvSharp;
using TestOpenCVApp.Utility;

namespace TestOpenCVApp.FeatureExtraction
{
    public class PrincipalLineExtractor : IPrincipalLineExtractor
    {
        public const int RoiWidth = 128;
        public const int RoiHeight = 128;

        //public int RoiWidth { get; set; }
        //public  int RoiHeight { get; set; }

        public const int FirstDerivChangeWidth = 4;
        public const int SecondDerivTresholdValue = 10;
        public const int ComponentMinSize = 15;

        public Mat Roi { get; set; }
        public Mat FinalLines { get; set; }

        public Mat DoFeatureExtraction(Mat roi)
        {
            var roiAfterPreprocessing = Preprocessing(roi);
            var normalizedRoi = NormalizeImage(roiAfterPreprocessing);
            var finalLines = LocatePrincipalLines(normalizedRoi);
            return finalLines;
        }

        public Mat Preprocessing(Mat img)
        {
            // Convert to grayscale
            var grayscaleMat = new Mat(img.Size(), MatType.CV_8UC1);
            Cv2.CvtColor(img, grayscaleMat, ColorConversionCodes.BGR2GRAY);

            // Resize it to 128x128
            Cv2.Resize(grayscaleMat, grayscaleMat, new Size(RoiWidth, RoiHeight));

            return grayscaleMat;
        }

        public Mat NormalizeImage(Mat img)
        {
            /* Applying CLAHE */
            var clahe = Cv2.CreateCLAHE();
            clahe.SetClipLimit(4);
            clahe.Apply(img, img);
            /* Applying a low-pass filter */
            Cv2.Blur(img, img, new Size(3, 3), new Point(-1, -1));

            return img;
        }

        private Mat LocatePrincipalLineInGivenDirection(Mat image, List<List<double>> H1, List<List<double>> H2,
            int degree)
        {
            var H1_rows = H1.Count;
            var H1_cols = H1[0].Count;
            var img = image.Clone();

            // First derivative of the input image
            var I_der = new double[RoiHeight, RoiWidth];

            // Second derivate of the input image
            var I_der2 = new double[RoiHeight, RoiWidth];


            // Calculate first- and second-order derivatives
            for (var i = H1_rows / 2; i < RoiHeight - H1_rows / 2; ++i)
            for (var j = H1_cols / 2; j < RoiWidth - H1_cols / 2; ++j)
            for (var _i = -(H1_rows / 2); _i <= H1_rows / 2; ++_i)
            for (var _j = -(H1_cols / 2); _j <= +(H1_cols / 2); ++_j)
            {
                I_der[i, j] += img.At<byte>(i + _i, j + _j) *
                               H1[_i + H1_rows / 2][_j + H1_cols / 2];
                I_der2[i, j] += img.At<byte>(i + _i, j + _j) *
                                H2[_i + H1_rows / 2][_j + H1_cols / 2];
            }


            // Create binary image, this will contain the extracted principal lines in a given direction
            //Mat binaryImage(img.size(), CV_8UC1, Scalar(0,0,0));
            var binaryImage = new Mat(img.Size(), MatType.CV_8UC1, new Scalar(0, 0, 0));
            // Locating changes in first-order derivatives in 0 direction
            if (degree == 0)
                for (var j = 3; j < 124; ++j)
                for (var i = 1; i < 126; ++i)
                {
                    var derivChangeFound = false;

                    // Checking if the derivative's sign has changed by comparing #dw neighbors 
                    for (var dw = 1; dw <= FirstDerivChangeWidth; ++dw)
                        if (IsInsideTheBoundary(i - dw, j))
                            if (I_der[i, j] * I_der[i - dw, j] < 0)
                                derivChangeFound = true;
                    /* If first-order derivative's sign has changed and the second-order derivative's value is greater 
                           than the treshold, then setting the current pixel to 255, otherwise to 0 */
                    if ((derivChangeFound || I_der[i, j] == 0.0) && I_der2[i, j] > SecondDerivTresholdValue)
                        binaryImage.Set(i, j, 255);
                    else
                        binaryImage.Set(i, j, 0);
                }

            // Locating changes in first-order derivatives in 90 direction
            if (degree == 90)
                for (var i = 1; i < 126; ++i)
                for (var j = 3; j < 124; ++j)
                {
                    var derivChangeFound = false;
                    // Checking if the derivative's sign has changed by comparing #dw neighbors 
                    for (var dw = 1; dw <= FirstDerivChangeWidth; ++dw)
                        if (IsInsideTheBoundary(i, j - dw))
                            if (I_der[i, j] * I_der[i, j - dw] < 0)
                                derivChangeFound = true;
                    /* If first-order derivative's sign has changed and the second-order derivative's value is greater
                        than the treshold, then setting the current pixel to 255, otherwise to 0 */
                    if ((derivChangeFound || I_der[i, j] == 0) && I_der2[i, j] > SecondDerivTresholdValue)
                        binaryImage.Set(i, j, 255);
                    else
                        binaryImage.Set(i, j, 0);
                }


            // Locating changes in first-order derivatives in 45 direction
            if (degree == 45)
                for (var i = 1; i < 2 * 128 - 2; ++i)
                {
                    var k = i < 128 ? 0 : i - 128 + 1;
                    for (var j = k + 1; j <= i - k; ++j)
                    {
                        var derivChangeFound = false;
                        // Checking if the derivative's sign has changed by comparing #dw neighbors 
                        for (var dw = 1; dw <= FirstDerivChangeWidth; ++dw)
                            if (IsInsideTheBoundary(j - dw, i - j))
                                if (I_der[j, i - j] * I_der[j - dw, i - j] < 0)
                                    derivChangeFound = true;
                        /* If first-order derivative's sign has changed and the second-order derivative's value is greater
                        than the treshold, then setting the current pixel to 255, otherwise to 0 */
                        if ((derivChangeFound || I_der[j, i - j] == 0) && I_der2[j, i - j] > SecondDerivTresholdValue)
                            binaryImage.Set(i, j, 255);
                        else
                            binaryImage.Set(i, j, 0);
                    }
                }

            // Locating changes in first-order derivatives in 45 direction
            if (degree == 135)
                for (var i = 1; i < 2 * 128 - 2; ++i)
                {
                    var k = i < 128 ? 0 : i - 128 + 1;
                    for (var j = k + 1; j <= i - k; ++j)
                    {
                        var index = 128 - 1 - (i - j);
                        var derivChangeFound = false;
                        // Checking if the derivative's sign has changed by comparing #dw neighbors 
                        for (var dw = 1; dw <= FirstDerivChangeWidth; ++dw)
                            if (IsInsideTheBoundary(j - dw, index))
                                if (I_der[j, index] * I_der[j - dw, index] < 0)
                                    derivChangeFound = true;

                        /* If first-order derivative's sign has changed and the second-order derivative's value is greater
                        than the treshold, then setting the current pixel to 255, otherwise to 0 */
                        if ((derivChangeFound || I_der[j, index] == 0.0) && I_der2[j, index] > SecondDerivTresholdValue)
                            binaryImage.Set(i, j, 255);
                        else
                            binaryImage.Set(i, j, 0);
                    }
                }


            return binaryImage;
        }

        public Mat LocatePrincipalLines(Mat img)
        {
            var x = img.Height;
            // (1-D Gaussian function) X (first-order derivative of 1-D Gaussian function) 

            // Directional line detector in 0 direction
            var H1_0 = new List<List<double>>
            {
                new List<double> {0.0009, 0.0027, 0.0058, 0.0092, 0.0107, 0.0092, 0.0058, 0.0027, 0.0009},
                new List<double> {0.0065, 0.0191, 0.0412, 0.0655, 0.0764, 0.0655, 0.0412, 0.0191, 0.0065},
                new List<double> {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
                new List<double> {-0.0009, -0.0027, -0.0058, -0.0092, -0.0107, -0.0092, -0.0058, -0.0027, -0.0009},
                new List<double> {-0.0065, -0.0191, -0.0412, -0.0655, -0.0764, -0.0655, -0.0412, -0.0191, -0.0065}
            };

            // Directional line detector in 45 direction 
            var H1_45 = new List<List<double>>
            {
                new List<double>
                {
                    -0.000900,
                    -0.006500,
                    0.000000,
                    0.006500,
                    0.000900,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    -0.002700,
                    -0.019100,
                    0.000000,
                    0.019100,
                    0.002700,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    -0.005800,
                    -0.041200,
                    0.000000,
                    0.041200,
                    0.005800,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.009200,
                    -0.065500,
                    0.000000,
                    0.065500,
                    0.009200,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.010700,
                    -0.076400,
                    0.000000,
                    0.076400,
                    0.010700,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.009200,
                    -0.065500,
                    0.000000,
                    0.065500,
                    0.009200,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.005800,
                    -0.041200,
                    0.000000,
                    0.041200,
                    0.005800,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.002700,
                    -0.019100,
                    0.000000,
                    0.019100,
                    0.002700,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.000900,
                    -0.006500,
                    0.000000,
                    0.006500,
                    0.000900
                }
            };

            // Directional line detector in 90 direction
            var H1_90 = new List<List<double>>
            {
                new List<double> {-0.000900, -0.006500, 0.0, 0.006500, 0.000900},
                new List<double> {-0.002700, -0.019100, 0.0, 0.019100, 0.002700},
                new List<double> {-0.005800, -0.041200, 0.0, 0.041200, 0.005800},
                new List<double> {-0.009200, -0.065500, 0.0, 0.065500, 0.009200},
                new List<double> {-0.010700, -0.076400, 0.0, 0.076400, 0.010700},
                new List<double> {-0.009200, -0.065500, 0.0, 0.065500, 0.009200},
                new List<double> {-0.005800, -0.041200, 0.0, 0.041200, 0.005800},
                new List<double> {-0.002700, -0.019100, 0.0, 0.019100, 0.002700},
                new List<double> {-0.000900, -0.006500, 0.0, 0.006500, 0.000900}
            };

            // Directional line detector in 135 direction
            var H1_135 = new List<List<double>>
            {
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.000900,
                    -0.006500,
                    0.000000,
                    0.006500,
                    0.000900
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.002700,
                    -0.019100,
                    0.000000,
                    0.019100,
                    0.002700,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.005800,
                    -0.041200,
                    0.000000,
                    0.041200,
                    0.005800,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.009200,
                    -0.065500,
                    0.000000,
                    0.065500,
                    0.009200,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.010700,
                    -0.076400,
                    0.000000,
                    0.076400,
                    0.010700,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    -0.009200,
                    -0.065500,
                    0.000000,
                    0.065500,
                    0.009200,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    -0.005800,
                    -0.041200,
                    0.000000,
                    0.041200,
                    0.005800,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    -0.002700,
                    -0.019100,
                    0.000000,
                    0.019100,
                    0.002700,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    -0.000900,
                    -0.006500,
                    0.000000,
                    0.006500,
                    0.000900,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                }
            };

            // 1-D Gaussian function X second-order derivative of 1-D Gaussian function

            // Directional line detector in 0 direction
            var H2_0 = new List<List<double>>
            {
                new List<double> {0.0156, 0.0211, 0.0309, 0.0416, 0.0464, 0.0416, 0.0309, 0.0211, 0.0156},
                new List<double> {0.0257, 0.0510, 0.0954, 0.1441, 0.1660, 0.1441, 0.0954, 0.0510, 0.0257},
                new List<double> {-0.0298, -0.1125, -0.2582, -0.4178, -0.4896, -0.4178, -0.2582, -0.1125, -0.0298},
                new List<double> {0.0257, 0.0510, 0.0954, 0.1441, 0.1660, 0.1441, 0.0954, 0.0510, 0.0257},
                new List<double> {0.0156, 0.0211, 0.0309, 0.0416, 0.0464, 0.0416, 0.0309, 0.0211, 0.0156}
            };

            // Directional line detector in 45 direction
            var H2_45 = new List<List<double>>
            {
                new List<double>
                {
                    0.015600,
                    0.025700,
                    -0.029800,
                    0.025700,
                    0.015600,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.021100,
                    0.051000,
                    -0.112500,
                    0.051000,
                    0.021100,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.030900,
                    0.095400,
                    -0.258200,
                    0.095400,
                    0.030900,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.041600,
                    0.144100,
                    -0.417800,
                    0.144100,
                    0.041600,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.046400,
                    0.166000,
                    -0.489600,
                    0.166000,
                    0.046400,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.041600,
                    0.144100,
                    -0.417800,
                    0.144100,
                    0.041600,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.030900,
                    0.095400,
                    -0.258200,
                    0.095400,
                    0.030900,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.021100,
                    0.051000,
                    -0.112500,
                    0.051000,
                    0.021100,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.015600,
                    0.025700,
                    -0.029800,
                    0.025700,
                    0.015600
                }
            };

            // Directional line detector in 90 direction
            var H2_90 = new List<List<double>>
            {
                new List<double> {0.015600, 0.025700, -0.029800, 0.025700, 0.015600},
                new List<double> {0.021100, 0.051000, -0.112500, 0.051000, 0.021100},
                new List<double> {0.030900, 0.095400, -0.258200, 0.095400, 0.030900},
                new List<double> {0.041600, 0.144100, -0.417800, 0.144100, 0.041600},
                new List<double> {0.046400, 0.166000, -0.489600, 0.166000, 0.046400},
                new List<double> {0.041600, 0.144100, -0.417800, 0.144100, 0.041600},
                new List<double> {0.030900, 0.095400, -0.258200, 0.095400, 0.030900},
                new List<double> {0.021100, 0.051000, -0.112500, 0.051000, 0.021100},
                new List<double> {0.015600, 0.025700, -0.029800, 0.025700, 0.015600}
            };

            // Directional line detector in 135 direction
             var H2_135 = new List<List<double>>
            {
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.015600,
                    0.025700,
                    -0.029800,
                    0.025700,
                    0.015600
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.021100,
                    0.051000,
                    -0.112500,
                    0.051000,
                    0.021100,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.030900,
                    0.095400,
                    -0.258200,
                    0.095400,
                    0.030900,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.041600,
                    0.144100,
                    -0.417800,
                    0.144100,
                    0.041600,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.046400,
                    0.166000,
                    -0.489600,
                    0.166000,
                    0.046400,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.000000,
                    0.041600,
                    0.144100,
                    -0.417800,
                    0.144100,
                    0.041600,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.000000,
                    0.030900,
                    0.095400,
                    -0.258200,
                    0.095400,
                    0.030900,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.000000,
                    0.021100,
                    0.051000,
                    -0.112500,
                    0.051000,
                    0.021100,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                },
                new List<double>
                {
                    0.015600,
                    0.025700,
                    -0.029800,
                    0.025700,
                    0.015600,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000,
                    0.000000
                }
            };


            Mat linesInDir0, linesInDir90, linesInDir45, linesInDir135;

            linesInDir0 = LocatePrincipalLineInGivenDirection(img, H1_0, H2_0, 0);
            
            linesInDir90 = LocatePrincipalLineInGivenDirection(img, H1_90, H2_90, 90);

            Mat lines = linesInDir90 | linesInDir0;
            
            lines = FilterWithConnectedComponentLabeling(lines, ComponentMinSize);
            return lines;
        }

        public Mat FilterWithConnectedComponentLabeling(Mat img, int componentMinSize)
        {
            var currentLabel = 0;


            var labels = new int[128, 128];

            // Using a stack to track the components (one component at time algorithm)
            var pixelStack = new Stack<Point>();

// This List will contain the separated components
            var components = new List<List<Point>>();

            // travel the image row by row
            for (var X = 0; X < img.Rows; ++X)
            for (var Y = 0; Y < img.Cols; ++Y)
                if (IsForeground(ref img, new Point(X, Y)) && labels[X, Y] == 0)
                {
                    currentLabel++;

                    // Creating the current component
                    var currentComponent = new List<Point>();

// Labeling the current pixel
                    labels[X, Y] = currentLabel;

                    // Pushing the current pixel to the stack
                    pixelStack.Push(new Point(X, Y));

                    // Pushing the current pixel to the current component
                    currentComponent.Add(new Point(X, Y));

                    //while(!pixelStack.empty())
                    while (pixelStack.Any())
                    {
                        //Point currentPixel = pixelStack.top();
                        //pixelStack.pop();

                        var currentPixel = pixelStack.First();
                        pixelStack.Pop();

                        // Getting the neighbor pixels of the current pixel
                        var neighborPoints = GetNeighborPoints(currentPixel);

                        // Iterating over the neigbor pixels
                        foreach (var neighborPoint in
                                neighborPoints)
                            // Checking if the neighbor pixel is foreground and it is not labeled yet
                            if (IsForeground(ref img, neighborPoint) &&
                                labels[neighborPoint.X, neighborPoint.Y] == 0)
                            {
                                // Labeling the neighbor pixel
                                labels[neighborPoint.X, neighborPoint.Y] = 1;

                                // Pushing the neighbor pixel to the current component
                                currentComponent.Add(neighborPoint);

                                // Pushing the neighbor pixel to the stack
                                pixelStack.Push(neighborPoint);
                            }

                        if (!pixelStack.Any())
                            components.Add(currentComponent);
                    }
                }

            // Removing components that are smaller than @componentMinSize	
            for (var i = 0; i < components.Count; ++i)
                if (components[i].Count < componentMinSize)
                    foreach (var p in
                        components[i])
                        img.Set(p.X, p.Y, 0);

            return img;
        }

        private List<Point> GetNeighborPoints(Point currentPixel)
        {
            // Defining direction points
            var directionPoints = new List<Point>();

            directionPoints.Add(new Point(1, 0));
            directionPoints.Add(new Point(1, -1));
            directionPoints.Add(new Point(0, -1));
            directionPoints.Add(new Point(-1, -1));

            directionPoints.Add(new Point(-1, 0));
            directionPoints.Add(new Point(-1, 1));
            directionPoints.Add(new Point(0, 1));
            directionPoints.Add(new Point(1, 1));

            var neigborPoints = new List<Point>();

            foreach (var X in directionPoints)
            {
                var neigborPoint = currentPixel + X;

                if (IsInsideTheBoundary(neigborPoint.X, neigborPoint.Y))
                    neigborPoints.Add(neigborPoint);
            }

            return neigborPoints;
        }

        private bool IsForeground(ref Mat img, Point p)
        {
            if (img.At<byte>(p.X, p.Y) == 255) return true;

            return false;
        }

        private bool IsInsideTheBoundary(int i, int j)
        {
            if (i > 0 && i < RoiHeight && j > 0 && j < RoiWidth)
                return true;
            return false;
        }
    }
}