﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.RoiExtracton
{
    public class RoiExtractionResult
    {
        //black-white rectangle with extracted palm lines
        public Mat BinaryRoiArea { get; set; }
        public KeyPoints OriginalImageKeyPoints { get; set; }
    }
}
