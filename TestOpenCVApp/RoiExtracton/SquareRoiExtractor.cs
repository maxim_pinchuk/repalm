﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenCvSharp;
using TestOpenCVApp.FeatureExtraction;
using TestOpenCVApp.PreProcessor;
using TestOpenCVApp.Utility;

namespace TestOpenCVApp.RoiExtracton
{
    public class SquareRoiExtractor : ISquareRoiExtractor
    {
        public SquareRoiExtractor(Mat image, Mat backgroundMask, IPreprocessor preprocessor)
        {
            _backgroundMask = backgroundMask;
            inputImage = image;
            this.preprocessor = preprocessor;
        }

        private Mat _backgroundMask;
        private Mat inputImage { get; }
        private Mat inputImageCopy { get; set; }
        private IPreprocessor preprocessor { get; }

        public RoiExtractionResult DoExtraction()
        {
            inputImageCopy = inputImage.Clone();
            try
            {
                //---------------------
                var keypoints = FindKeypoints(ref _backgroundMask);
                var roi = this.CalcSquareRoi(ref keypoints);
                //точка между указательным и большим
                var lineExtractor = new PrincipalLineExtractor();
                //var roi = new Mat("test123.png");
                var features= lineExtractor.DoFeatureExtraction(roi);
                return new RoiExtractionResult
                {
                    BinaryRoiArea = features,
                    OriginalImageKeyPoints = keypoints
                };
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public Mat CalcSquareRoi(ref KeyPoints keypoints)
        {
            // Draw a line between the first and third keypoint (for debugging)
            Cv2.Line(inputImage, keypoints.Keypoint1, keypoints.Keypoint3, new Scalar(0, 0, 255), 1);
            // Calculating the perpendicular slope to the previous line
            var slope = 0.0;
            var perpendicular_slope = 0.0;

            if (keypoints.Keypoint3.X - keypoints.Keypoint1.X == 0)
            {
                perpendicular_slope = 0;
            }
            else
            {
                slope = (keypoints.Keypoint3.Y - keypoints.Keypoint1.Y) /
                        (double) (keypoints.Keypoint3.X - keypoints.Keypoint1.X);
                perpendicular_slope = -1 / slope;
            }

            // Calculating the ROI's top left and bottom left points, 
            // by finding the lines which goes through these points and are perpendicular to the line between Keypoint1 and Keypoint3

            Point keypoint1_1, keypoint3_1;
            var dist = Math.Sqrt(Math.Pow(keypoints.Keypoint1.X - keypoints.Keypoint3.X, 2) +
                               Math.Pow(keypoints.Keypoint1.Y - keypoints.Keypoint3.Y, 2));
            keypoint1_1.X = keypoints.Keypoint1.X + (int)dist;
            keypoint3_1.X = keypoints.Keypoint3.X + (int)dist;

            keypoint1_1.Y = (int)perpendicular_slope * (keypoint1_1.X - keypoints.Keypoint1.X) + keypoints.Keypoint1.Y;
            keypoint3_1.Y = (int)perpendicular_slope * (keypoint3_1.X - keypoints.Keypoint3.X) + keypoints.Keypoint3.Y;
            // Drawing the square ROI (for debugging)
            Cv2.Line(inputImage, keypoints.Keypoint1, keypoint1_1, new Scalar(0, 0, 255), 1);
            Cv2.Line(inputImage, keypoints.Keypoint3, keypoint3_1, new Scalar(0, 0, 255), 1);
            Cv2.Line(inputImage, keypoint1_1, keypoint3_1, new Scalar(0, 0, 255), 1);
            // Calculating the rotation angle before! cropping the square roi
            var rotAlpha = 0.0;

            if (keypoints.Keypoint3.Y - keypoints.Keypoint1.Y != 0)
                rotAlpha = Math.Tan((keypoints.Keypoint3.X - keypoints.Keypoint1.X) /
                                    (double) (keypoints.Keypoint3.Y - keypoints.Keypoint1.Y));

            // Rotating the input image
            var centerPoint = new Point2f(inputImage.Cols / 2.0F, inputImage.Rows / 2.0F);

            var rotMat = Cv2.GetRotationMatrix2D(centerPoint, -rotAlpha * (180 / 3.1415), 1.0);

            Cv2.WarpAffine(inputImage, inputImage, rotMat, inputImage.Size());
            Cv2.WarpAffine(inputImageCopy, inputImageCopy, rotMat, inputImage.Size());

            // Getting the new keypoints by rotating the old ones with rotAlpha angle

            double keypoint3x = keypoint3_1.X - inputImage.Cols / 2;
            double keypoint3y = keypoint3_1.Y - inputImage.Rows / 2;

            double keypoint1x = keypoints.Keypoint1.X - inputImage.Cols / 2;
            double keypoint1y = keypoints.Keypoint1.Y - inputImage.Rows / 2;

            var newUpperLeftCorner = new Point();
            var newBottomRightCorner = new Point
            {
                X = (int)(keypoint3x * Math.Cos(-rotAlpha) + keypoint3y * Math.Sin(-rotAlpha)),
                Y = (int)(-keypoint3x * Math.Sin(-rotAlpha) + keypoint3y * Math.Cos(-rotAlpha))
            };

            newBottomRightCorner.X = newBottomRightCorner.X + inputImage.Cols / 2;
            newBottomRightCorner.Y = newBottomRightCorner.Y + inputImage.Rows / 2;


            newUpperLeftCorner.X = (int) (keypoint1x * Math.Cos(-rotAlpha) + keypoint1y * Math.Sin(-rotAlpha));
            newUpperLeftCorner.Y = (int) (-keypoint1x * Math.Sin(-rotAlpha) + keypoint1y * Math.Cos(-rotAlpha));

            newUpperLeftCorner.X = newUpperLeftCorner.X + inputImage.Cols / 2;
            newUpperLeftCorner.Y = newUpperLeftCorner.Y + inputImage.Rows / 2;
            
            //Extract the square ROI
            return inputImageCopy.Clone(new Rect(newUpperLeftCorner, new Size(
                newBottomRightCorner.X - newUpperLeftCorner.X,
                keypoints.BottomLeftRoiPoint.Y - keypoints.TopLeftRoiPoint.Y
                )));
        }

        public KeyPoints FindKeypoints(ref Mat segmentedImage)
        {
            int colCounter = 0;
            int lastCol = segmentedImage.Cols;
            int rowCounter = (segmentedImage.Rows-1) /2;
            while (segmentedImage.At<byte>(rowCounter,lastCol) == 0)
            {
                --lastCol;
            }
            Cv2.Circle(segmentedImage,lastCol, rowCounter, 20,Scalar.Red);
            var differenceCols = (int)(segmentedImage.Cols - lastCol + segmentedImage.Cols *0.035);
            Mat leftSide = segmentedImage.Clone(new Rect(0, 0, segmentedImage.Cols - differenceCols, segmentedImage.Rows));
            List<Point> boundaryVector;
            try
            {
                // Get boundary points, by applying boundary tracking alogirthm
                boundaryVector = new BoundaryTracking().GetBoundary(ref leftSide);
            }
            catch (PPAException e)
            {
                throw;
            }

            // Find start and end points (vertically) ElementAt left side of the image

            Point startPoint = new Point(-1, -1), 
                endPoint = new Point(-1, -1);

            var foundStart = false;
            for (var i = 0; i < leftSide.Rows; ++i)
                if (foundStart)
                {
                    if (leftSide.At<byte>(i, leftSide.Cols - 1) == 0)
                    {
                        startPoint.X = leftSide.Cols - 1;
                        startPoint.Y = i;
                        break;
                    }
                }
                else
                {
                    if (leftSide.At<byte>(i, leftSide.Cols - 1) == 255)
                    {
                        foundStart = true;
                        endPoint.X = leftSide.Cols - 1;
                        endPoint.Y = i;
                    }
                }
           
            //Check if startPoint and endPoint was successfuly found
            if (startPoint.X == -1 && startPoint.Y == -1 || endPoint.X == -1 && endPoint.Y == -1)
                throw new PPAException("Keypoints not found.");

            // Find the center Point (vertically) ElementAt right side of the image
            var centerPoint = new Point(leftSide.Cols, (startPoint.Y + endPoint.Y) / 2);
            Cv2.Circle(inputImage, centerPoint, 20, Scalar.Red, 1); //differs from original
            // Calculate dinstances between the center points and every boundary Point 
            var distancesFromCenterPoint = new List<PointWithDistance>();
            for (var i = 0; i < boundaryVector.Count; ++i)
            {
                var p = new PointWithDistance
                {
                    Point = new Point(boundaryVector.ElementAt(i).X, boundaryVector.ElementAt(i).Y)
                };

                p.Distance = Math.Sqrt(Math.Pow(centerPoint.X - p.Point.X, 2) + Math.Pow(centerPoint.Y - p.Point.Y, 2));
                distancesFromCenterPoint.Add(p);
            }

            // Finding the local minimums on the Distance function, these will be keypoints
            // Applying a Mean filter on Distance function, to get rid of false local minimums

            var filtered_distances = distancesFromCenterPoint;

            var sum = 0.0;
            double kernel_size = 5;

            for (var index = 2; index < distancesFromCenterPoint.Count - 2; index++)
            {
                sum = distancesFromCenterPoint.ElementAt(index - 2).Distance +
                      distancesFromCenterPoint.ElementAt(index - 1).Distance +
                      distancesFromCenterPoint.ElementAt(index).Distance +
                      distancesFromCenterPoint.ElementAt(index + 1).Distance +
                      distancesFromCenterPoint.ElementAt(index + 2).Distance;
                filtered_distances.ElementAt(index).Distance = sum / kernel_size;
            }

            // Searcing for local minimums, with a predefined stepsize,
            // Check if the stepsize'th previous Point is bigger than the current points and the stepsize'th next Point is also bigger than the current Point
            // If the previous condition is true, then the current Point will be stored as a local minimum

            var stepsize = 50;
            var minimums = new List<PointWithDistance>();

            if (filtered_distances.Count() < 500) throw new PPAException("Boundary not found.");

            for (var i = stepsize; i < filtered_distances.Count - stepsize; ++i)
                if (filtered_distances.ElementAt(i - stepsize).Distance > filtered_distances.ElementAt(i).Distance &&
                    filtered_distances.ElementAt(i + stepsize).Distance > filtered_distances.ElementAt(i).Distance)
                {
                    var temp = new PointWithDistance();
                    temp.Point = filtered_distances.ElementAt(i).Point;
                    temp.Distance = filtered_distances.ElementAt(i).Distance;

                    minimums.Add(temp);
                }

            // Creating three clusters, these will contain local minimum points around the keypoints (valleys between fingers)
            // Iterating through the local minimums, if the Distance is higher than 10 pixels between the next and current Point, then the next Point will be classified into a new cluster,
            // otherwise, it will be classified into the current cluster

            var minClusters = new List<List<PointWithDistance>>();
            var tempRow = new List<PointWithDistance>();
            for (var i = 0; i < minimums.Count - 1; ++i)
            {
                PointWithDistance current, next;
                current = minimums.ElementAt(i);
                next = minimums.ElementAt(i + 1);
                var dist = Math.Sqrt(Math.Pow(current.Point.X - next.Point.X, 2) +
                                     Math.Pow(current.Point.Y - next.Point.Y, 2));
                if (dist > 10)
                {
                    minClusters.Add(tempRow);
                    tempRow = new List<PointWithDistance>();
                }
                else
                {
                    tempRow.Add(current);
                    
                }
            }
            if (tempRow.Count != 0) minClusters.Add(tempRow);

            // Returning this struct
            var keypoints = new KeyPoints();

            // If the number of the clusters isn't four, then something went wrong
            if (minClusters.Count != 4)
            {
                keypoints.Success = false;
                throw new PPAException("Keypoints not found.");
            }
            // Search minimums in the clusters, based on the Distance from the center Point. These will be the keypoints.
            var inaccuracy = 10;
            PointWithDistance keypoint1 = new PointWithDistance(),
                keypoint2 = new PointWithDistance(),
                keypoint3 = new PointWithDistance();

            //keypoint1 = minClusters[1].Aggregate((min, next) => min.Distance < next.Distance ? min : next);
            //Cv2.Circle(inputImage, keypoint1.Point, 2, Scalar.Red, -1);
            var lowPoint1 = minClusters[1].Aggregate((min, next) => min.Distance < next.Distance ? min : next);

            for (var i = 0; i < lowPoint1.Point.Y; ++i)
            {
                if (segmentedImage.At<byte>(i, lowPoint1.Point.X + inaccuracy) == 255)
                {
                    Cv2.Circle(inputImage, new Point(lowPoint1.Point.X + inaccuracy, i+ inaccuracy), 2, Scalar.Blue);
                    keypoint1 = new PointWithDistance()
                    {
                        Point = new Point(lowPoint1.Point.X + inaccuracy, i + inaccuracy),
                        Distance = lowPoint1.Distance
                    };
                    break;
                }
            }

            Cv2.Circle(inputImage, lowPoint1.Point, 2, Scalar.Red, -1);

            keypoint2 = minClusters[2].Aggregate((min, next) => min.Distance < next.Distance ? min : next);
            Cv2.Circle(inputImage, keypoint2.Point, 2, Scalar.Blue, -1);

            var keypoint0 = minClusters[0].Aggregate((min, next) => min.Distance < next.Distance ? min : next);
            Cv2.Circle(inputImage, keypoint0.Point, 2, Scalar.Orange, -1);

            var lowPoint3 = minClusters[3].Aggregate((min, next) => min.Distance < next.Distance ? min : next);


            for (var i = segmentedImage.Rows-1; i > lowPoint3.Point.Y; --i)
            {
                if (segmentedImage.At<byte>(i,lowPoint3.Point.X + inaccuracy) == 255)
                {
                    Cv2.Circle(inputImage, new Point(lowPoint3.Point.X + inaccuracy, i - inaccuracy), 2, Scalar.Blue);
                    keypoint3 = new PointWithDistance()
                    {
                        Point = new Point(lowPoint3.Point.X + inaccuracy, i - inaccuracy),
                        Distance = lowPoint3.Distance
                    };
                    break;
                }
            }

            Cv2.Circle(inputImage, startPoint, 2, Scalar.Yellow);
            Cv2.Circle(inputImage, endPoint, 2, Scalar.Yellow);

            keypoints.CenterPoint = centerPoint;
            keypoints.Keypoint1 = keypoint1.Point;
            keypoints.Keypoint2 = keypoint2.Point;
            keypoints.Keypoint3 = keypoint3.Point;
            keypoints.ThumbIndexFingerPoint = keypoint0.Point;
            keypoints.IndexMiddleFingerPoint = keypoint1.Point;
            keypoints.MiddleRingFingerPoint = keypoint2.Point;
            keypoints.RingLittleFingerPoint = keypoint3.Point;
            keypoints.TopLeftRoiPoint = keypoint1.Point;
            keypoints.BottomLeftRoiPoint = keypoint3.Point;
            keypoints.TopRightRoiPoint = startPoint;
            keypoints.BottomRightRoiPoint = endPoint;
            keypoints.Success = true;
            Cv2.Circle(inputImage, keypoint1.Point.X, keypoint1.Point.Y, 2, Scalar.RandomColor());
            Cv2.Circle(inputImage, keypoint2.Point.X, keypoint2.Point.Y, 2, Scalar.RandomColor());
            Cv2.Circle(inputImage, keypoint3.Point.X, keypoint3.Point.Y, 2, Scalar.RandomColor());
            Cv2.Circle(inputImage, keypoint0.Point.X, keypoint0.Point.Y, 2, Scalar.RandomColor());
            Cv2.Circle(inputImage, startPoint.X, startPoint.Y, 2, Scalar.RandomColor());
            Cv2.Circle(inputImage, endPoint.X, endPoint.Y, 2, Scalar.RandomColor());
          //  inputImage.SaveImage("test6.png");
            return keypoints;
        }
    }
}