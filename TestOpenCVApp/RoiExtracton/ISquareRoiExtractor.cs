﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.RoiExtracton
{
    public interface ISquareRoiExtractor :IRoiExtractor
    {
        /// <summary>
        /// Localizes the keypoints from the segmented image
        /// </summary>
        /// <param name="segmentedImage"></param>
        /// <returns></returns>
        KeyPoints FindKeypoints(ref Mat segmentedImage);

        /// <summary>
        /// Calculates the square ROI based on the given keypoints
        /// </summary>
        /// <param name="keypoints"></param>
        /// <returns></returns>
        Mat CalcSquareRoi(ref KeyPoints keypoints);
    }
}
