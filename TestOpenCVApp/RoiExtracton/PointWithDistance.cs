﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.RoiExtracton
{
    public class PointWithDistance
    {
        // Boundary point
        public Point Point { get; set; }

        // Distance from boundary point to the center point
        public double Distance { get; set; }
    }
}
