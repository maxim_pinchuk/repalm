﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.RoiExtracton
{
    public struct KeyPoints
    {
        /**
     * centerPoint Middle point between the top and bottom boundary of the hand (from left-most column)
     */
        public Point CenterPoint { get; set; }

        /**
         *  keypoint1 Valley-point between the index and middle finger
         */
        public Point Keypoint1 { get; set; }

        /**
         *  keypoint2 Valley-point between the middle and third finger
         */
        public Point Keypoint2 { get; set; }

        /**
         *  keypoint3 Valley-point between the third and little finger
         */
        public Point Keypoint3 { get; set; }
        /**
         *  success Indicates if the keypoints were successfuly detected or not
         */
        public  Point TopLeftRoiPoint { get; set; }
        public Point BottomLeftRoiPoint { get; set; }
        public Point TopRightRoiPoint { get; set; }
        public Point BottomRightRoiPoint { get; set; }

        //point of local minimum between fingers
        public Point ThumbIndexFingerPoint { get; set; }

        public Point IndexMiddleFingerPoint { get; set; }
        public Point MiddleRingFingerPoint { get; set; }
        public Point RingLittleFingerPoint { get; set; }
        public bool Success { get; set; }
    }
}
