﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.RoiExtracton
{
    public interface IRoiExtractor
    {
        RoiExtractionResult DoExtraction();
    }
}
