﻿using System;
using OpenCvSharp;

namespace TestOpenCVApp.Utility
{
    public static class Thinning
    {
        public static Mat DoThinning(Mat img)
        {
            var sourceImg = img.Clone();
            Cv2.Blur(sourceImg, sourceImg, new Size(3, 3));
            Cv2.Threshold(sourceImg, sourceImg, 127, 255, ThresholdTypes.Binary);
            var skeletonImg = new Mat(sourceImg.Size(), MatType.CV_8UC1, new Scalar(0));
            var skeletonTemp = new Mat(sourceImg.Size(), MatType.CV_8UC1);
            var skeletonElement = Cv2.GetStructuringElement(MorphShapes.Cross, new Size(3, 3));
            var skeletonDone = false;
            while (!skeletonDone)
            {
                Cv2.MorphologyEx(sourceImg, skeletonTemp, MorphTypes.Open, skeletonElement);
                Cv2.BitwiseNot(skeletonTemp, skeletonTemp);
                Cv2.BitwiseAnd(sourceImg, skeletonTemp, skeletonTemp);
                Cv2.BitwiseOr(skeletonImg, skeletonTemp, skeletonImg);
                Cv2.Erode(sourceImg, sourceImg, skeletonElement);

                double min = 0;
                double max;
                Cv2.MinMaxLoc(sourceImg, out min, out max);
                skeletonDone = (max == 0);
            }
            return skeletonImg;
        }

        private static void ThinningIteration(ref Mat img, int iter)
        {
            Mat marker = Mat.Zeros(img.Size(), MatType.CV_8UC1);

            for (var i = 1; i < img.Rows - 1; i++)
            for (var j = 1; j < img.Cols - 1; j++)
            {
                var p2 = img.At<byte>(i - 1, j);
                var p3 = img.At<byte>(i - 1, j + 1);
                var p4 = img.At<byte>(i, j + 1);
                var p5 = img.At<byte>(i + 1, j + 1);
                var p6 = img.At<byte>(i + 1, j);
                var p7 = img.At<byte>(i + 1, j - 1);
                var p8 = img.At<byte>(i, j - 1);
                var p9 = img.At<byte>(i - 1, j - 1);

                var a = Convert.ToInt32(p2 == 0 && p3 == 1) + Convert.ToInt32(p3 == 0 && p4 == 1) +
                        Convert.ToInt32(p4 == 0 && p5 == 1) + Convert.ToInt32(p5 == 0 && p6 == 1) +
                        Convert.ToInt32(p6 == 0 && p7 == 1) + Convert.ToInt32(p7 == 0 && p8 == 1) +
                        Convert.ToInt32(p8 == 0 && p9 == 1) + Convert.ToInt32(p9 == 0 && p2 == 1);

                var b = p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
                var m1 = iter == 0 ? p2 * p4 * p6 : p2 * p4 * p8;
                var m2 = iter == 0 ? p4 * p6 * p8 : p2 * p6 * p8;

                if (a == 1 && b >= 2 && b <= 6 && m1 == 0 && m2 == 0)
                    marker.Set(i, j, 1);
            }

            img &= ~marker;
        }
    }
}