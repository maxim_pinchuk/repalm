﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.Utility
{
    public class BoundaryTracking
    {

        public int GetNeighborhoodSearchIndex(int dir)
        {
            if (dir % 2 == 0)
            {
                return (dir + 7) % 8;
            }
            else
            {
                return (dir + 6) % 8;
            }
        }


        Point GetNextBoundaryPoint(ref Mat img, ref int dir, Point currentPoint)
        {
            
            // init directionPoints
            List<Point> directionPoints = new List<Point>();

            directionPoints.Add(new Point(1, 0));
            directionPoints.Add(new Point(1, -1));
            directionPoints.Add(new Point(0, -1));
            directionPoints.Add(new Point(-1, -1));

            directionPoints.Add(new Point(-1, 0));
            directionPoints.Add(new Point(-1, +1));
            directionPoints.Add(new Point(0, +1));
            directionPoints.Add(new Point(1, +1));

            Point nextPoint = new Point(0, 0);

            int startIndex = GetNeighborhoodSearchIndex(dir);
            for (int i = startIndex; i < startIndex + 8; ++i) {

                Point neighborPoint = currentPoint + directionPoints.ElementAt(i % 8);

                if (neighborPoint.Y >= 0 && neighborPoint.Y < img.Rows && neighborPoint.X >= 0 && neighborPoint.X < img.Cols)
                {

                    var val = img.At<byte>(neighborPoint.Y, neighborPoint.X);
                    if (img.At<byte>(neighborPoint.Y, neighborPoint.X) == 255) {
                        nextPoint = neighborPoint;
                        dir = i % 8;
                        break;
                    }
                }
            }

            return nextPoint;
        }

        public List<Point> GetBoundary(ref Mat img)
        {

            List<Point> boundaryVector = new List<Point>();


            Point startingPoint = new Point (-1, -1);
            Size size = img.Size();

            bool condition = true;
            while (condition)
            {
                for (int x = size.Width -1; x > 0 ; x--)
                {
                    for (int Y = 0; Y < size.Height; ++Y)
                    {
                        if (img.At<byte>(Y, x) == 255)
                        {
                            startingPoint.X = x;
                            startingPoint.Y = Y;
                            condition = false;
                            break;
                        }
                    }
                    if (condition == false)
                    {
                        break;
                    }
                }
            }
        
            // Check if the startingPoint was found
            if (startingPoint.X == -1 && startingPoint.Y == -1)
            {
                throw new PPAException("Boundary starting point not found");
            }

            boundaryVector.Add(startingPoint);
            Point currentPoint = startingPoint;
            int dir = 5;
            do
            {
                currentPoint = GetNextBoundaryPoint(ref img, ref dir, currentPoint);
                boundaryVector.Add(currentPoint);

           // } while (currentPoint.X != startingPoint.X -1);
            } while (currentPoint.X !=  size.Width - 1);
            return boundaryVector;
        }

    }
}
