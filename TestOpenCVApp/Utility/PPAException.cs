﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.Utility
{
    public class PPAException : Exception
    {
        public PPAException(string message) : base(message) { }
    }
}
