﻿using OpenCvSharp;
using RePalm.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestOpenCVApp.LinesReading;
using TestOpenCVApp.PreProcessor;
using TestOpenCVApp.RoiExtracton;
using  System.Drawing;
using ImageMagick;

namespace TestOpenCVApp
{
    public class PalmistryRecognitionEntryPoint
    {
        private readonly string appFolderPath;
        private readonly string rightHandImagePath;
        private readonly string userFolderPath;
        private readonly bool _isRightHand;
 //       private readonly string leftHandImagePath;

       public PalmistryRecognitionEntryPoint(
            string appFolderPath,
            string rightHandImagePath, 
            string userFolderPath,
            bool isRightHand
            //string leftHandImagePath
            )
        {
            this.appFolderPath = appFolderPath;
            this.rightHandImagePath = rightHandImagePath;
            this.userFolderPath = userFolderPath;
            _isRightHand = isRightHand;
           // this.leftHandImagePath = leftHandImagePath;
        }
        public PalmistryResponse GetRecognitionResults()
        {
            PreprocessImage();
            var palmIMage = new Mat(rightHandImagePath);
            var segmentedImagePath = GetSegmentedImage(palmIMage);
            var mask = new Mat(segmentedImagePath);
            var background = mask;
            IPreprocessor preprocessor = new Preprocessor();
            IRoiExtractor roiExtractor = new SquareRoiExtractor(palmIMage, background, preprocessor);
            var extractionResult = roiExtractor.DoExtraction();
            var palmistryResponseService = new PalmistryResponseService(rightHandImagePath,userFolderPath, appFolderPath);
            var response = palmistryResponseService.GeneratePalmistryResponse(extractionResult, _isRightHand);
            return response;
        }

        private string GetSegmentedImage(Mat palmIMage)
        {
            var backgroundImage = new Mat(new OpenCvSharp.Size(palmIMage.Width, palmIMage.Height), MatType.CV_8UC3, new Scalar(0, 0, 0));
            backgroundImage.SaveImage(string.Join("/",userFolderPath, "back.png"));
            var overlayImage = new Mat( string.Join("/",appFolderPath, "palmRight.png"), ImreadModes.Unchanged);
            var newHeight = palmIMage.Height * 0.8;
            var scaleTimes = newHeight / overlayImage.Height;
            Cv2.Resize(overlayImage, overlayImage, new OpenCvSharp.Size(overlayImage.Width * scaleTimes, newHeight));
            overlayImage.SaveImage(string.Join("/", userFolderPath, "overlay.png"));
            var segmentedImageFolderPath = string.Join("/", userFolderPath, "trueBack.png");
            using (IMagickImage image = new MagickImage(string.Join("/", userFolderPath, "back.png")))
            {
                image.Draw(new DrawableComposite(palmIMage.Width / 2 - overlayImage.Width / 2, palmIMage.Height / 2 - overlayImage.Height / 2,
                    new MagickImage(string.Join("/", userFolderPath, "overlay.png"))));
                image.Write(segmentedImageFolderPath);
            }
            return segmentedImageFolderPath;
        }

        private void PreprocessImage()
        {
            MagickImage img = new MagickImage(rightHandImagePath);
            img.Quantize(new QuantizeSettings()
            {
                Colors = 128
            });
            var scale = (double) img.Height / 1000;
            img.Resize(1000, (int) (img.Width / scale));

            if (!_isRightHand)
            {
                img.Flip();
            }
            img.Write(rightHandImagePath);
        }
    }
}

