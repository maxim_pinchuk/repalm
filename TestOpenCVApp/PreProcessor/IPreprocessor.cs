﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.PreProcessor
{
    // doing preprocessing on an image
    public interface IPreprocessor
    {
        Mat DoPreprocessing(Mat img);
    }
}
