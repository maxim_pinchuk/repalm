﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace TestOpenCVApp.PreProcessor
{
    public class Preprocessor : IPreprocessor
    {
        public Mat DoPreprocessing(Mat img)
        {
            var rgbImg = img.Clone();
            Mat ycbcrImg = new Mat();
            
            // Convert original image to YCbCr color space
            Cv2.CvtColor(rgbImg, ycbcrImg,ColorConversionCodes.RGB2YCrCb);
            Cv2.Blur(ycbcrImg, ycbcrImg, new Size(5, 5));
            // Parameters of the skin-color modell
            double c11 = 0.0479;
            double c12 = 0.0259;
            double c21 = 0.0259;
            double c22 = 0.0212;

            double k1 = 0.0;
            double k2 = 0.0;
            double x1, x2;
            double m1 = 113.9454;
            double m2 = 157.5052;
            double f1, f2;

            var thresholded =  new Mat(ycbcrImg.Size(), MatType.CV_8UC1);
            thresholded = thresholded.SetTo(new Scalar(0));
            //Segmenting the hand
            double p = 0;

            for (int i = 0; i < ycbcrImg.Rows; ++i)
            {
                for (int j = 0; j < ycbcrImg.Cols; ++j)
                {

                    x1 = (int)ycbcrImg.At<Vec3b>(i, j)[1];
                    x2 = (int)ycbcrImg.At<Vec3b>(i, j)[2];

                    f1 = -0.5 * (x1 - m1);
                    f2 = -0.5 * (x2 - m2);
                    k1 = f1 * c11 + f2 * c21;
                    k2 = f1 * c12 + f2 * c22;

                    // Probability of the pixel that belongs to a skin region
                    p = Math.Exp(k1 * (x1 - m1) + k2 * (x2 - m2));


                    if (p > 0.15)
                    {
                        var val = thresholded.At<byte>(i, j);
                        thresholded.Set(i,j,255);
                        var checkVal = thresholded.At<byte>(i, j);

                    }
                }
            }

            return thresholded;

        }
    }
}
