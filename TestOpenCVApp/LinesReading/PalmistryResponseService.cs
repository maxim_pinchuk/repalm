﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using ImageMagick;
using TestOpenCVApp.RoiExtracton;
using System.Drawing;
using System.IO;

namespace TestOpenCVApp.LinesReading
{
    public class PalmistryResponseService
    {
        private readonly string _originalImagePath;
        private readonly string _appFolderPath;
        private IMagickImageCollection _recognitionResultGif;
        private IMagickImage _recognitionResultImage;
        public PalmistryResponseService( string originalImagePath, string userFolderPath, string appFolderPath)
        {
            this._originalImagePath = originalImagePath;
            _appFolderPath = appFolderPath;
        }
        
        public PalmistryResponse GeneratePalmistryResponse(RoiExtractionResult principleLinesData, bool isRightHand)
        {
            _recognitionResultImage =
                GetResultOfRecognition(principleLinesData.OriginalImageKeyPoints, _originalImagePath, isRightHand);
            var response = new PalmistryResponse();
            response.PalmistryRightHandResultImage = _recognitionResultImage.ToBase64();
            return response;
        }

        private IMagickImage GetResultOfRecognition(KeyPoints keypoints, string imagePath,bool isRightHand)
        {
            IMagickImage resultImage;
            using (IMagickImage image = new MagickImage(imagePath))
            {
                double coef = 563.0 / image.Height;
                image.BrightnessContrast(new Percentage(20), new Percentage());
                image.Blur(180, 3);
                //--------draw heart line
                var heartLine = new MagickImage(Path.Combine(_appFolderPath, "palmRightHeartLine.png"));
                heartLine.Resize(heartLine.Width,
                 (int)((keypoints.RingLittleFingerPoint.Y - keypoints.IndexMiddleFingerPoint.Y) / (1.7 / coef)));
                image.Draw(new DrawableComposite(
                    keypoints.IndexMiddleFingerPoint.X + 10
                    , (int)((keypoints.IndexMiddleFingerPoint.Y + keypoints.MiddleRingFingerPoint.Y) / (2.5 - (0.9 - coef)))
                    , heartLine));


                //---------------draw life line
                var lifeLine = new MagickImage(Path.Combine(_appFolderPath, "palmRightLifeLine.png"));
                var lifeTopPoint = new Point2d(
                  (((keypoints.ThumbIndexFingerPoint.X + keypoints.TopLeftRoiPoint.X) / 2) +
                   keypoints.IndexMiddleFingerPoint.X) / 2
                  ,
                  (keypoints.ThumbIndexFingerPoint.Y + keypoints.IndexMiddleFingerPoint.Y) / 2
              );
                lifeLine.Resize((lifeLine.Width),
                    (int)((keypoints.RingLittleFingerPoint.Y - keypoints.TopLeftRoiPoint.Y) / (1.2 / coef)));
                image.Draw(new DrawableComposite(
                    lifeTopPoint.X + 5,
                    lifeTopPoint.Y + (coef < 1 ? 15 / coef : 1),
                    lifeLine
                ));

                //----------draw head line--------------
                var headLine = new MagickImage(Path.Combine(_appFolderPath, "palmRightHeadLine.png"));
                var headTopPoint = new Point2d(
                   (keypoints.ThumbIndexFingerPoint.X + keypoints.TopLeftRoiPoint.X) / 2,
                   (keypoints.ThumbIndexFingerPoint.Y + keypoints.IndexMiddleFingerPoint.Y) / 2
               );

                var resizeWidth = keypoints.CenterPoint.X - headTopPoint.X;
                headLine.Resize(
                    (int)(resizeWidth * coef),
                    (int)((keypoints.RingLittleFingerPoint.Y - keypoints.ThumbIndexFingerPoint.Y)));
                image.Draw(new DrawableComposite(
                    headTopPoint.X + 15
                    , (int)(headTopPoint.Y + 5 / coef)
                    , headLine));
                //----

                resultImage = new MagickImage(image);

                if (!isRightHand)
                {
                    resultImage.Flip();
                }
                resultImage.Rotate(90);
            }
            return resultImage;
        }
        
    }
}
