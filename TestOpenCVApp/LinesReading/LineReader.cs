﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace TestOpenCVApp.LinesReading
{
    public class LineReader
    {
        public LineExplanation ExplainLine(Mat image)
        {

            return new LineExplanation();
            
        }

        private List<List<Point>> ExtractPrincipleLinesFromImage(ref Mat image)
        {
            List<List<Point>> principleLines = new List<List<Point>>();
            List<Point> usedPoints = new List<Point>();
            for (var j = 0; j <= image.Rows; j++)
            {
                for (int i = 0; i <= image.Cols; i++)
                {
                    var val = image.At<byte>(j, i);
                    if (val == 255)
                    {
                        var newPoint = new Point(i, j);
                        
                        var selectedLine = principleLines.Where(l => l.Any(p =>
                            !usedPoints.Contains(p) &&
                           (Math.Abs(p.DistanceTo(newPoint)) == 1 ||
                            Math.Abs(p.DistanceTo(newPoint)) == 2 ||
                            Math.Abs(p.DistanceTo(newPoint)) == 3 ||
                            Math.Abs(Math.Round(p.DistanceTo(newPoint), 2)) == 1.41 ||
                            Math.Abs(Math.Round(p.DistanceTo(newPoint), 2)) == 2.24))).OrderByDescending(x => x.Count)
                            .FirstOrDefault();

                        if (selectedLine != null)
                        {
                            selectedLine.Add(newPoint);
                        }
                        else
                        {
                            principleLines.Add(new List<Point>() { new Point(i, j) });
                        }
                    }
                }
            }
            return principleLines.OrderByDescending(x => x.Count).Take(3).ToList();
            //foreach (var principleLine
            //    in principleLines.OrderByDescending(x => x.Count).Take(3))
            //{
            //    Scalar color = Scalar.RandomColor();
            //    foreach (var point in principleLine)
            //    {

            //        Cv2.Circle(img6, point.X, point.Y, 0, color);
            //        img6.SaveImage("12.png");
            //    }
            //}
        }
    }
}
