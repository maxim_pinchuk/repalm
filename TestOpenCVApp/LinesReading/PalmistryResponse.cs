﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOpenCVApp.LinesReading
{
    public class PalmistryResponse
    {
        public string PalmistryResultGifBase64 { get; set; }
        public string PalmistryRightHandResultImage { get; set; }
        public  string PalmistryLeftHandResultImage { get; set; }
    }
}
