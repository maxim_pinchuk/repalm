﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RePalm.Client.ViewModels.Extensions
{
    public static class NumberExtensions
    {
        public static int SumDigitsToGetOneDigit(this int num)
        {
            int sum = 0;
            while (num > 0)
            {
                sum += num % 10;
                num /= 10;
            }
            if (sum > 9)
            {
                sum = SumDigitsToGetOneDigit(sum);
            }
            return sum;
        }
    }
}
