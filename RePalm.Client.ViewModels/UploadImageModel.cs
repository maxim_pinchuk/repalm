﻿namespace RePalm.Client.ViewModels
{
    public class UploadImageModel
    {
        public string Base64File { get; set; }
        public string FileName { get; set; }
    }
}
