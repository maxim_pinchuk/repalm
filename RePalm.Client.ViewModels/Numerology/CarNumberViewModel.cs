﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;
using Xamarin.Forms;

namespace RePalm.Client.ViewModels.Numerology
{
    public class CarNumberViewModel:PredictionViewModelBase
    {
        private string _carNumber;
        public string CarNumber
        {
            get => _carNumber;
            set
            {
                SetProperty(ref _carNumber, value);
                if (this.GetResultCommand != null)
                {
                    ((Command) this.GetResultCommand).ChangeCanExecute();
                }
            }
        }

        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute {
            get { return !string.IsNullOrWhiteSpace(_carNumber); }
            set { }
        }

        public CarNumberViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }
        protected override void OnGetResult()
        {
            var answerVariants = new string[]
            {
                @"Your car is for business trips abroad and the signing of big contracts.",
                @"All matters relating to obtaining and giving loans is the sphere of activity of your car",
                @"This car is for travelling. Feel free to go on holidays by it and have fun.",
                @"This car will give you good service and unless there are other unlucky factors associated, you will feel secure and safe in it.",
                @"This car will not bring you any luck and may be taking away some of the luck that you have. If you are noticing any unexpected or regular problems associated with the car in your life then you should consider changing the car and getting one that’s compatible with you.",
                @"You can drive this car if you are not experiencing any unexpected trouble or problems in your life or in the car. However in case you’ve noticed that your mood is worse in the car than usual then consider changing it."
            };
            var random = new Random();
            ResultViewModel.Result = answerVariants[random.Next(0,answerVariants.Length)];
        }
    }
}
