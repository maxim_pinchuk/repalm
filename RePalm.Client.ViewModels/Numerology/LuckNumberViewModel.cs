﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RePalm.Client.ViewModels.CommonViewModels;
using Xamarin.Forms;

namespace RePalm.Client.ViewModels.Numerology
{
    public class LuckNumberViewModel:PredictionViewModelBase
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
                if (base.GetResultCommand != null)
                {
                    ((Command)GetResultCommand).ChangeCanExecute();
                }
            }
        }

        public BirthDateViewModel BirthDateViewModel { get; set; }
        public CommonResultViewModel ResultViewModel { get; set; }

        public LuckNumberViewModel()
        {
            BirthDateViewModel = new BirthDateViewModel();
            ResultViewModel = new CommonResultViewModel();
        }

        public bool CanEnterBirthDate => !string.IsNullOrWhiteSpace(this.Name);

        public override bool CanExecute
        {
            get { return CanEnterBirthDate; }
            set { }
        }

        protected override void OnGetResult()
        {
            var answers = new string[]
            {
                @"Day of important tasks when swiftness bears good results, decisions are quick and easy, business is profitable and successful.",
                @"Day of achievements. Lucky for undertakings, serious plans. Day of personal triumph, implemented ambition, especially for people from the art world. Opportunities for commerce.",
                @"Scheduling tasks which do not require immediate action. There may be hindrances if you show indecision. Day of contrasts, it is better to refrain from activity."
            };
            var random= new Random();
            ResultViewModel.Result = answers[random.Next(0,answers.Length)];
        }
    }
}
