﻿using System;
using System.Windows.Input;
using RePalm.Client.ViewModels.CommonViewModels;
using RePalm.Client.ViewModels.Extensions;
using Xamarin.Forms;

namespace RePalm.Client.ViewModels.Numerology
{
    public class DestinyNumberViewModel: PredictionViewModelBase
    {
        #region Properties

        public BirthDateViewModel BirthDate { get; set; }
        
        public CommonResultViewModel Result { get; set; }

        #endregion
        

        public DestinyNumberViewModel()
        {
            BirthDate = new BirthDateViewModel();
            Result = new CommonResultViewModel();
        }


        public override bool CanExecute
        {
            get { return true; }
            set { }
        }

        protected override void OnGetResult()
        {
            var num = (BirthDate.Date.Year.SumDigitsToGetOneDigit() + BirthDate.Date.Month.SumDigitsToGetOneDigit() +
                      BirthDate.Date.Day.SumDigitsToGetOneDigit()).SumDigitsToGetOneDigit();


            switch (num)
            {
                case 1:
                {
                    Result.Result =
                        @"This number shows that the person has natural, inbuilt leadership qualities and is destined to be a leader. Therefore, such a person should train himself to be innovative, determined and bold with self confidence and self control. He should not be domineering, selfish, bossy, and arrogant or a bully. He should have the ability to independently think for himself as to what is right and act accordingly paving one's own way to lead others into what he considers right. A leader should be able to know any approaching problem and learn how to avoid them and find a safe path.";
                    Result.ResultLabel = "Result: Destiny Number 1";
                        break;
                }
                case 2:
                {
                    Result.Result =
                        @"People with Number 2 as the destiny number have diplomatic capabilities. They make good peacemakers. For such a capability, one will be found to be tactful and persuasive. He should train himself to be peaceful, loving, kind, considerate, patient, gentle and understanding. He should not be very sensitive or easily carried away by other's emotions, talks or behaviour. He must take charge over moodiness and indecisiveness in himself He must know how to help out others by resolving their quarrels and differences even if he really does not wish to do so. This is the right path of self-fulfilment.";
                    Result.ResultLabel = "Result: Destiny Number 2";
                        break;
                }
                case 3:
                {
                    Result.Result =
                        @"A possessor of number 3 as the destiny number excels in creating and maintaining relationships. He is optimistic, cheerful, confident, and sociable. He should teach himself to help others learn how to be cheerful and happy. He can be a great asset to those who have lost the joy and the zest of life by teaching them to live life to the fullest with joy and laughter. This can be done by one's own lifestyle and by being articulate and creative in bringing others to what is right thus enabling them to enjoy the only life they have on earth.";
                    Result.ResultLabel = "Result: Destiny Number 3";
                        break;
                }
                case 4:
                {
                    Result.Result =
                        @"With Number 4 as the destiny number, the individual has great potential for all kinds of managerial work. He has obvious powerful capabilities to manage big organizations. He can take big responsibilities on himself, and fulfils them with precision. He is a very practical person and finds satisfaction in organizing important events, meetings, matters which will be long lasting. This person should learn to be methodical, serious, studious, disciplined, loyal, honest and practical so that others can trust him to get things done in a proper manner. He should not be rigid, stubborn, boring, melancholy and afraid of change.";
                    Result.ResultLabel = "Result: Destiny Number 4";
                        break;
                }
                case 5:
                {
                    Result.Result =
                        @"This destiny number is possessed by those who love to have change in their lives. They do not tolerate a stagnating lifestyle. They find fulfilment in having freedom. He delights in travelling, adventure and challenges in life. Such a person, therefore, should learn to bring change in his lifestyle by changing whatever is influencing his familiar, sedentary life. He must ensure not to blindly rush for a change, instead keeping alert for any good opportunity to show up for the desired change. He should teach himself to be enthusiastic and adaptable, and not over-indulgent, indifferent, vulgar, irritable or wasteful.";
                    Result.ResultLabel = "Result: Destiny Number 5";
                        break;
                }
                case 6:
                {
                    Result.Result =
                        @"The possessor of this destiny number is affectionate and homely with humanitarian capabilities. He is eager to help those who are unhappy, weak and facing adversity in their lives; he lifts them up from their fate and enables them to find happiness in their lives, and fulfilment in his own life. He should, therefore, teach himself to be dependable, kind and friendly towards others. He should not be obstinate, irresponsible, unyielding and slow in taking decisions. He can aid someone who cannot take decision for a help but should not interfere in other peopleâ€™s affairs unless asked for.";
                    Result.ResultLabel = "Result: Destiny Number 6";
                        break;
                }
                case 7:
                {
                    Result.Result =
                        @"The Number 7 in Destiny numbers means someone having teaching capabilities. He is a thinker and a learner of new things and needs good time with himself. He loves to share with others what he has himself learnt. This he does verbally or through impressive writings thus bringing satisfaction to his life. This person should teach himself to be original, analytical, independent, professional and truthful. He should not be pessimistic, sceptical, aloof, nervous and afraid, and should not find faults with others. He does well by participating in explorations of fields of knowledge like science, mysteries and discoveries.";
                    Result.ResultLabel = "Result: Destiny Number 7";
                        break;
                }
                case 8:
                {
                    Result.Result =
                        @"Destiny Number 8 belongs to people who desire respect from others and are very ambitious in their lives. Such a person makes it his aim to accomplish great things and achieve outstanding success in his life. He should learn to be self-controlled, diligent, determined to be successful in whatever he does, persevering, hard-working and philosophical. He should set goals and strive to achieve them through diligence. Financial gains will follow him but he should not make that his aim. He should not be oppressive, bully or authoritative, and not have a hunger for power, fame, money or material gains.";
                    Result.ResultLabel = "Result: Destiny Number 8";
                        break;
                }
                case 9:
                {
                    Result.Result =
                        @"The Destiny number 9 possessor has great enthusiasm for life. He brings beauty and perfection into his own life and into the lives of others. This he does through his contribution on charity, romance and art. Such a person should be of a peaceful mind, kind, generous, forgiving and compassionate at heart, an inspiration to others, strong-willed, artistic, energetic and humane. He should teach himself not to be narrow-minded or of a narrow outlook to life, emotional, impulsive, hasty in losing his temper, bitter in speech and behaviour, a bad influence and should not be quick to pick up fights.";
                    Result.ResultLabel = "Result: Destiny Number 9";
                    break;
                }
                    break;

            }
        }
    }
}
