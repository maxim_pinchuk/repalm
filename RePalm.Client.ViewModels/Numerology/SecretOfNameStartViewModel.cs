﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;
using Xamarin.Forms;

namespace RePalm.Client.ViewModels.Numerology
{
    public class SecretOfNameStartViewModel:PredictionViewModelBase
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
                if (base.GetResultCommand != null)
                {
                    ((Command)GetResultCommand).ChangeCanExecute();
                }
            }
        }

        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute { get { return true; } set{} }

        public SecretOfNameStartViewModel()
        {
            this.ResultViewModel = new CommonResultViewModel();
        }
        protected override void OnGetResult()
        {
            this.ResultViewModel.Result = string.Join(" ", Name, "Test Result");
        }
    }
}
