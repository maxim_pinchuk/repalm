﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Numerology
{
    public class PythagorsSquareViewModel : PredictionViewModelBase
    {
        public BirthDateViewModel BirthDateViewModel { get; set; }
        public CommonResultViewModel ResultViewModel { get; set; }

        public PythagorsSquareViewModel()
        {
            BirthDateViewModel = new BirthDateViewModel();
            ResultViewModel = new CommonResultViewModel();
        }

        public override bool CanExecute
        {
            get { return true; }
            set { }
        }

        protected override void OnGetResult()
        {
            this.ResultViewModel.Result = string.Join(" ", BirthDateViewModel.Date.ToString(), "Test Result");
        }
    }
}
;