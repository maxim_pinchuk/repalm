﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;
using Xamarin.Forms;

namespace RePalm.Client.ViewModels.Numerology
{
    public class WillDesireComeTrueViewModel:PredictionViewModelBase
    {
        private string _numberSequence;
        public string NumberSequence
        {
            get => _numberSequence;
            set
            {
                SetProperty(ref _numberSequence, value);
                if (this.GetResultCommand != null)
                {
                    ((Command)this.GetResultCommand).ChangeCanExecute();
                }
            }
        }
        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute { get { return !string.IsNullOrWhiteSpace(this.NumberSequence); }  set{} }

        public WillDesireComeTrueViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }
        protected override void OnGetResult()
        {
            var answerVariants = new string[]
            {
                @"You will become a leader, master of the situation, the driving force behind what is happening. The desire will be fulfilled without hindrances and problems. Any situation and circumstances will be favorable for you. You can act boldly, and without giving much thinking: you will succeed!",
                @"You may have a competitor. Carefully look at people surrounding you. Maybe you will be able to “spot” an ill-willer. If not, show extreme caution. You never know what? You can be screwed out of your luck.",
                @"The desire will be fulfilled, but later. Your own stubbornness is to blame for the delay. Perhaps you are stuck on one thing, considering it necessary for the fulfillment of the desire. In fact it is not, however, you did not make a fatal mistake, you just more complicated the situation. Soon everything will be fine."
            };
            var random = new Random();
            ResultViewModel.Result = answerVariants[random.Next(0,answerVariants.Length)];
        }

    }
}
