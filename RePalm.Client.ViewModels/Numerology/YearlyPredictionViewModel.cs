﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Numerology
{
    public class YearlyPredictionViewModel:PredictionViewModelBase
    {
        public BirthDateViewModel BirthDateViewModel { get; set; }
        private string _desiredYear;
        public string DesiredYear
        {
            get => _desiredYear;
            set
            {
                SetProperty(ref _desiredYear, value);
            }
        }
        public CommonResultViewModel ResultViewModel { get; set; }
        public override bool CanExecute { get { return true; } set{} }

        public YearlyPredictionViewModel()
        {
            BirthDateViewModel = new BirthDateViewModel();
            DesiredYear = "";
            ResultViewModel =  new CommonResultViewModel();
        }
        protected override void OnGetResult()
        {
            var answers = new string[]
            {
                @"It’s time to relax. Do not look for new projects, go on with your studies, support partnerships, be open to new love. But try to avoid the continuation of such relations, which create a problem from the beginning, for example, love affair with married partner. In such a case it is better to be alone for some time, fill your life with new experiences, go sightseeing or travel. In short: it is a year of harmony, a year for long and interesting holiday.",
                @"This is the year of renovation. Changes, perhaps not the most favorable, lie ahead of you. It is better not to conceive new projects and be busy with routine affairs. But as for love and friendship, you will find a time of grace. Familiar relations will be refreshed and add to them something new. The best months for you will be May and June, so make haste to live this favorable period to its fullest. This year also favors your health: all your ailments will leave you alone.",
                @"It’s time to start some new business. New meetings, new jobs, dating lie ahead of you, in a word, you will not get bored. However, it does not mean that life will be entirely rosy (failures are possible), but the obstacles will advance you forward. This year favors new love and new occupations and new occupations and it also favors enterprising people who rely only on themselves. If you do not reckon yourself as one of them, try to change, tangible positive results will not be slow to follow: you will notice them the following year."
            };
            var random = new Random();
            
            this.ResultViewModel.Result = answers[random.Next(0,answers.Length)];
        }
    }
}
