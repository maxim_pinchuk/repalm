﻿using System;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Relationship
{
    public class IsManUnfaitfulViewModel : PredictionViewModelBase
    {
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                OnPropertyChanged("CanExecute");
            }
        }

        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute
        {
            get => !string.IsNullOrWhiteSpace(Name);
            set { }
        }

        public IsManUnfaitfulViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }

        protected override void OnGetResult()
        {
            var random = new Random();
            var num = random.Next(1, 3);
            if (num == 1)
            {

                ResultViewModel.ResultLabel = @"He is Cheating";
                ResultViewModel.Result =
                    @"Cue the sad trombone, because your man is cheating! Then again, you probably had your suspicions about your his fidelity. The most important thing to do is not blame yourself. If your significant other doesn't realize what a catch you are, then it's his loss. There really are plenty of fish in the sea, which are probably sweeter and more beautiful than your current catch. It's time to cut the line on your beau and go fishing!";
            }
            else
            {
                ResultViewModel.ResultLabel = @"He May or May Not Be Cheating";
                ResultViewModel.Result =
                    @"Your man's fidelity is a toss up. In a situation like this it is best not to jump to conclusions or make accusations. It is time to gather more evidence! Talk to your guy about rumors you have heard and about your own gut feelings concerning his potential cheating habits. If he denies any wrongdoing, then you can get in touch with your inner spy girl and talk to his friends, check his texts and follow him when he mysteriously goes out for 'ice cream' at 2:00 a.m. and doesn't invite you. Good luck!";
            }
        }
    }
}
