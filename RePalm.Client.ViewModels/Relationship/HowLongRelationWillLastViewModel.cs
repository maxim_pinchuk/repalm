﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Relationship
{
    public class HowLongRelationWillLastViewModel:PredictionViewModelBase
    {
        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                OnPropertyChanged("CanExecute");
            }
        }
        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute
        {
            get => !string.IsNullOrWhiteSpace(Name);
            set { }
        }

        public HowLongRelationWillLastViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }

        protected override void OnGetResult()
        {
            var res1 =
                @"You are in a great relationship. You love your significant other and can't imagine ever ending it with them. You're sweet together, you respect each other, and you're ready to commit. However, your relationship won't be able to withstand the harsh realities of a marriage and you'll amicably split.";
            var res2 = @"Your current relationship is going to stand the test of time. You have a healthy friendship and connection that will be able to handle the ups and downs of a life together. You do conflict well together and you laugh and enjoy the little things together.";
            var res3 = @"You're relationship is fun, it's flirtatious, it's romantic. You love hanging out together non-stop, but you're about to finish the honeymoon stage. Once that's over, you'll realize you aren't right for each other, as so many do, and part ways.";
            var arrRes = new string[]
            {
                res1,res2,res3
            };
            var random = new Random();
            var resIndex = random.Next(0, arrRes.Length);
            switch (resIndex)
            {
                case 0:
                {
                    ResultViewModel.ResultLabel = $"{Name}, You got: Married for 10 years!";
                    break;
                }
                case 1:
                {
                    ResultViewModel.ResultLabel = $"{Name}, You got: You'll end up together!";
                    break;
                }
                case 2:
                {
                    ResultViewModel.ResultLabel = $"{Name}, You got: 6 more months! ";
                    break;
                }
            }
            ResultViewModel.Result = arrRes[resIndex];
        }
    }
}
