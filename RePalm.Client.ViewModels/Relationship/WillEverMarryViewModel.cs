﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Relationship
{
    public class WillEverMarryViewModel:PredictionViewModelBase
    {
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                OnPropertyChanged("CanExecute");
            }
        }

        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute
        {
            get => !string.IsNullOrWhiteSpace(Name);
            set { }
        }

        public WillEverMarryViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }

        protected override void OnGetResult()
        {
            var random = new Random();
            var num = random.Next(1, 3);
            if (num == 1)
            {

                ResultViewModel.ResultLabel = @"Yes";
                ResultViewModel.Result =
                    @"Wedding bells are absolutely in your future. Whether or not you've already found your soulmate, the time will come when you're wiping tears, walking down the aisle, and eating cake. Congrats in advance!";
            }
            else
            {
                ResultViewModel.ResultLabel = @"Maybe";
                ResultViewModel.Result =
                    @"Marriage sounds good in theory, but it may not be the best choice for you. You'll never settle for anything that makes you less than truly happy. And if that mean no wedding, then so be it. We're hoping this isn't the case, though!";
            }
        }
    }
}
