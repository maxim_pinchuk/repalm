﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Relationship
{
    public class CompabilityViewModel : PredictionViewModelBase
    {
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                OnPropertyChanged("CanExecute");
            }
        }

        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute
        {
            get => !string.IsNullOrWhiteSpace(Name);
            set { }
        }

        public CompabilityViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }

        protected override void OnGetResult()
        {
            var random = new Random();
            var val = random.Next(15, 100);
            ResultViewModel.ResultLabel = $"{Name}, your are about {val}%";
            if (val <= 30)
            {
                ResultViewModel.Result =
                    @"He / she kind of likes you but is unsure. They like you just not that way...";
            }
            else if (val <= 60)
            {
                ResultViewModel.Result =
                    @"He / she really likes you and enjoys your company and presence. You are good friend but not best friends.";
            }
            else
            {
                ResultViewModel.Result =
                    @"He / she really loves spending time with you and wishes they could spend more time around you. They think about you everyday and really care about you! CONGRATS!";
            }
        }
    }
}
