﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Relationship
{
    public class FutureChoiceViewModel :PredictionViewModelBase
    {
        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                OnPropertyChanged("CanExecute");
            }
        }
        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute
        {
            get => !string.IsNullOrWhiteSpace(Name);
            set { }
        }

        public FutureChoiceViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }

        protected override void OnGetResult()
        {

            var res1 = @"Not as motivated but very free spirited. His intentions are the best but sometimes he can come off as a bit too relaxed. He's caring, free spirited, and always available to reassure you. He influences those around him always wants a relaxing and stable environment.";
            var res2 = @"Your husband will be very family oriented, focused on the family and wanting to have kids, (depending on how much you agree on). Your husband will want to be around his family and is very protective. He will do whatever necessary to assure that his family is living a comfortable and stable life.";
            var res3 = @"Your husband will be focused on his career and will strive to be the best. Still focused on family,his time and efforts will be put forth to achieve the best and to guarantee the best for his family. Though most of his time will be spent in the office, you don't mind.";
            var res4 = @"Your husband is unpredictable and romantic, which keeps the relationship flowing smoothly, he's what you dreamt of and has a great sense of humor. He's the one, and you just know it. He's spontaneous and charming and always has you smiling.";
            var arrRes = new string[]
            {
                res1, res2, res3, res4
            };
            var random = new Random();
            var resIndex = random.Next(0, arrRes.Length);
            switch (resIndex)
            {
                case 0:
                {
                    ResultViewModel.ResultLabel = $"{Name}, You got: Easy going.";
                    break;
                }
                case 1:
                {
                    ResultViewModel.ResultLabel = $"{Name}, You got: Family Oriented.";
                    break;
                }
                case 2:
                {
                    ResultViewModel.ResultLabel = $"{Name}, You got: Business Oriented.";
                    break;
                }
                case 3:
                {
                    ResultViewModel.ResultLabel = $"{Name}, You got: Unpredictable.";
                    break;
                }
            }
            ResultViewModel.Result = arrRes[resIndex];
        }
    }
}
