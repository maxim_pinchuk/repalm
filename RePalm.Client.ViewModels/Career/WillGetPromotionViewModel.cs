﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Career
{
    public class WillGetPromotionViewModel : PredictionViewModelBase
    {
        public override bool CanExecute
        {
            get => !string.IsNullOrWhiteSpace(Name);
            set { }
        }

        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                OnPropertyChanged($"CanExecute");
            }
        }

        public CommonResultViewModel ResultViewModel { get; set; }

        public WillGetPromotionViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }

        protected override void OnGetResult()
        {
            var random = new Random();
            var val = random.Next(1, 4);
            ResultViewModel.ResultLabel = Name;
            if (val == 1)
            {
                ResultViewModel.Result =
                    @"You can only be demoted for your deeds.";
            }
            else if (val == 2)
            {
                ResultViewModel.Result =
                    @"It will be soon, if you make efforts.";
            }
            else
            {
                ResultViewModel.Result =
                    @"You are going to be promoted.";
            }
        }
    }
}
