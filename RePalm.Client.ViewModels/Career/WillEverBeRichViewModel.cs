﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Career
{
    public class WillEverBeRichViewModel: PredictionViewModelBase
    {
        private string _name;
        public  string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                OnPropertyChanged("CanExecute");
            }
        }
        public CommonResultViewModel ResultViewModel { get; set; }

        public override bool CanExecute
        {
            get => !string.IsNullOrWhiteSpace(Name);
            set { }
        }

        public WillEverBeRichViewModel()
        {
            ResultViewModel =  new CommonResultViewModel();
        }

        protected override void OnGetResult()
        {
            var answers = new string[]
            {
                @"You’ll get rich, if you lose everything.",
                @"Too many people spend money they earned..to buy things they don't want..to impress people that they don't like.",
                @"A wise person should have money in their head, but not in their heart.",
                @"Wealth consists not in having great possessions, but in having few wants.",
                @"An investment in knowledge pays the best interest.",
                @"Money never made a man happy yet, nor will it. The more a man has, the more he wants. Instead of filling a vacuum, it makes one."

            };
            var random = new Random();
            var resIndex = random.Next(1, answers.Length);
            ResultViewModel.ResultLabel = Name;
            ResultViewModel.Result = answers[resIndex];
        }
    }
}
