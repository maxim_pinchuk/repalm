﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.Career
{
    public class ShouldStartNewBusinessViewModel:PredictionViewModelBase
    {
        public override bool CanExecute
        {
            get => !string.IsNullOrWhiteSpace(Name); set{} }

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                OnPropertyChanged($"CanExecute");
            } }
        public CommonResultViewModel ResultViewModel { get; set; }
        public ShouldStartNewBusinessViewModel()
        {
            ResultViewModel = new CommonResultViewModel();
        }
        protected override void OnGetResult()
        {
            var random = new Random();
            var val = random.Next(1, 100);
            ResultViewModel.ResultLabel = $"{Name}, {val}%";
            if (val <= 30)
            {
                ResultViewModel.Result =
                    @"It makes no sense to expect that everything goes like clockwork. In life it happens rarely. But let no difficulties stop you and make you get off the chosen path. The results will not make you regret it.";
            }
            else if(val<=60)
            {
                ResultViewModel.Result =
                    @"Even if you have already achieved something, it should not be the reason to rest on the laurels. Be always dissatisfied with the current situation, strive forward. This is the only way you can achieve great results.";
            }
            else
            {
                ResultViewModel.Result =
                    @"You’ll have to work hard. There is no more effective way to achieve success for you in this case. Even if there is no evidence in favor of the conceived, but you are willing to work hard for achieving your goals, get down to business.";
            }
        }
    }
}
