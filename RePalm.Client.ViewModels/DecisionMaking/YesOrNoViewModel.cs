﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;

namespace RePalm.Client.ViewModels.DecisionMaking
{
    public class YesOrNoViewModel:PredictionViewModelBase
    {
        private string _answer;

        public string Answer
        {
            get => _answer;
            set => SetProperty(ref _answer, value);
        }

        public override bool CanExecute { get; set; }
        protected override void OnGetResult()
        {

            Random r = new Random();
            var num = r.Next(1, 3);
            Answer = num == 1 ? "Yes!" : "No.";
        }
    }
}
