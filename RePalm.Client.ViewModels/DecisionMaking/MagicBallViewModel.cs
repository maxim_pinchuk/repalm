﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RePalm.Client.ViewModels.CommonViewModels;
using Xamarin.Forms;

namespace RePalm.Client.ViewModels.DecisionMaking
{
    public class MagicBallViewModel:PredictionViewModelBase
    {
        private string _question;

        public string Question
        {
            get => _question;
            set
            {
                SetProperty(ref _question, value);
                if (this.GetResultCommand != null)
                {
                    ((Command) GetResultCommand).ChangeCanExecute();
                }
            }
        }

        private string _magicBallAnswerImage;

        public string MagicBallAnswerImage
        {
            get => _magicBallAnswerImage;
            set { SetProperty(ref _magicBallAnswerImage, value); }
        }

        public override bool CanExecute { get { return !string.IsNullOrWhiteSpace(this.Question); } set{} }

        public MagicBallViewModel()
        {
            
        }

        protected override void OnGetResult()
        {
            Random r = new Random();
           var num = r.Next(1, 4);
           MagicBallAnswerImage = string.Concat("magicball", num.ToString(), ".png");
        }
    }
}
