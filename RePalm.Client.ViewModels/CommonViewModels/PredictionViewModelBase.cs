﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RePalm.Client.ViewModels.CommonViewModels
{
    public abstract class PredictionViewModelBase : ViewModelBase
    {
        public ICommand GetResultCommand { get; set; }
        public abstract bool CanExecute { get; set; }
        protected abstract void OnGetResult();

        protected PredictionViewModelBase()
        {
            GetResultCommand = new Command(OnGetResult, () =>
            {
                return CanExecute;
            });
        }
    }
}
