﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RePalm.Client.ViewModels.CommonViewModels
{
    public class RangeDateViewModel: DateViewModel
    {
        public virtual DateTime MinDate { get; set; }
        public virtual DateTime MaxDate { get; set; }
    }
}
