﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RePalm.Client.ViewModels.CommonViewModels
{
    public class BirthDateViewModel:RangeDateViewModel
    {

        public override DateTime MinDate
        {
            get
            {
                return DateTime.Today.AddYears(-120);
            }
            set { }
        }

        public override DateTime MaxDate
        {
            get
            {
                return DateTime.Today.AddYears(-4);
            }
            set { }
        }


        public BirthDateViewModel()
        {
            base.Date = new DateTime(1990,02,02);
        }
    }
}
