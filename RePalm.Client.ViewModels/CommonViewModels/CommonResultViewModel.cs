﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RePalm.Client.ViewModels.CommonViewModels
{
    public class CommonResultViewModel:ViewModelBase
    {
        private string _result;

        public string Result
        {
            get => _result;
            set => SetProperty(ref _result, value);
        }
        private string _resultLabel;
        public string ResultLabel
        {
            get => _resultLabel;
            set => SetProperty(ref _resultLabel, value);
        }
    }
}
