﻿namespace RePalm.Client.ViewModels.PalmRecognition
{
    public class PalmistryResponse
    {
        public string PalmistryResultGifBase64 { get; set; }
        public  string PalmistryRightHandResultImage { get; set; }
        public string PalmistryLeftHandResultImage { get; set; }
    }
}
