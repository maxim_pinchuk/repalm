﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RePalm.Client.ViewModels.PalmRecognition
{
    public class PalmSnapShotViewModel:ViewModelBase
    {
        private byte[] _snapShot;
        public byte[] SnapShot
        {
            get => _snapShot;
            set => SetProperty(ref _snapShot, value);
        }
    }
}
