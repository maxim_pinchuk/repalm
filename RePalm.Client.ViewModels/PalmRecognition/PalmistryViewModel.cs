﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace RePalm.Client.ViewModels.PalmRecognition
{
    public class PalmistryViewModel:ViewModelBase
    {
        private PalmSnapShotViewModel _leftHandPictureViewModel;
        public PalmSnapShotViewModel LeftHandPictureViewModel
        {
            get => _leftHandPictureViewModel;
            set => SetProperty(ref _leftHandPictureViewModel, value);
        }

        private PalmSnapShotViewModel _rightHandPictureViewModel;
        public PalmSnapShotViewModel RightHandPictureViewModel
        {
            get => _rightHandPictureViewModel;
            set => SetProperty(ref _rightHandPictureViewModel, value);
        }

        private string rightImageBase64;

        public string RightImageBase64
        {
            get { return rightImageBase64; }
            set
            {
                rightImageBase64 = value;
                OnPropertyChanged("RightImageBase64");

                RightImage = Xamarin.Forms.ImageSource.FromStream(
                    () => new MemoryStream(Convert.FromBase64String(rightImageBase64)));
            }
        }
        public bool IsRightHand { get; set; }

        private Xamarin.Forms.ImageSource rightImage;
        public Xamarin.Forms.ImageSource RightImage
        {
            get { return rightImage; }
            set
            {
                rightImage = value;
                OnPropertyChanged("RightImage");
            }
        }
        private string leftImageBase64;

        public string LeftImageBase64
        {
            get { return leftImageBase64; }
            set
            {
                leftImageBase64 = value;
                OnPropertyChanged("LeftImageBase64");

                LeftImage = Xamarin.Forms.ImageSource.FromStream(
                    () => new MemoryStream(Convert.FromBase64String(leftImageBase64)));
            }
        }

        private Xamarin.Forms.ImageSource leftImage;
        public Xamarin.Forms.ImageSource LeftImage
        {
            get { return leftImage; }
            set
            {
                leftImage = value;
                OnPropertyChanged("LeftImage");
            }
        }

        private string _palmistryResultBase64;
        public string PalmistryResultBase64
        {
            get => _palmistryResultBase64;
            set => SetProperty(ref _palmistryResultBase64, value);
        }
        private PalmistryResponse _palmistryResponse;
        public  PalmistryResponse PalmistryResponse
        {
            get => _palmistryResponse;
            set => SetProperty(ref _palmistryResponse, value);
        }

        private bool _canShowResults;

        public bool CanShowResults
        {
            get => _canShowResults;
            set => SetProperty(ref _canShowResults, value);
        }


        public ICommand GetLeftHandSnap { get; set; }
        public ICommand GetRighthandSnap { get; set; }
        public ICommand GetResult { get; set; }
        public bool IsOldSession { get; set; }

        public PalmistryViewModel()
        {
            this.LeftHandPictureViewModel = new PalmRecognition.PalmSnapShotViewModel();
            this.RightHandPictureViewModel = new PalmSnapShotViewModel();
            GetResult = new Command(OnGetResult, () => true);
            _palmistryResultBase64 = "";
        }


        public async void OnGetResult()
        {
            
            CanShowResults = false;
            var palmRecognitionModel = new PalmFutureTellingModel()
            {
                LeftHandImage = !IsRightHand ? new UploadImageModel()
                {
                    Base64File = System.Convert.ToBase64String(this.LeftHandPictureViewModel.SnapShot),
                    FileName =Guid.NewGuid() + ".png"
                } : null,
                RightHandImage = IsRightHand ?  new UploadImageModel()
                {
                    
                Base64File = System.Convert.ToBase64String(this.RightHandPictureViewModel.SnapShot),
                FileName = Guid.NewGuid() + ".png"
                }:null
            };

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(palmRecognitionModel);
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(
                        "http://palmistryapi.azurewebsites.net/api/palmistry",
                        new StringContent(json, Encoding.UTF8, "application/json"));

                    var content = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<PalmistryResponse>(content);
                    PalmistryResponse = result;
                    RightImageBase64 = result.PalmistryRightHandResultImage;
                    LeftImageBase64 = result.PalmistryLeftHandResultImage;
                    CanShowResults = true;
                }
            }
            catch (Exception e)
            {
                CanShowResults = true;
                throw;
            }
          
        }

    }
}
