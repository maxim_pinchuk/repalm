﻿namespace RePalm.Client.ViewModels
{
    public class PalmFutureTellingModel
    {
        public UploadImageModel LeftHandImage { get; set; }
        public UploadImageModel RightHandImage { get; set; }

    }
}
