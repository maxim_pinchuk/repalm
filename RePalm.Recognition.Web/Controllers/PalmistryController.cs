﻿using Microsoft.AspNetCore.Mvc;
using RePalm.Shared.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using TestOpenCVApp;

namespace RePalm.Recognition.Web.Controllers
{
    [Route("api/[controller]")]
    public class PalmistryController : Controller

    {
        private readonly ILogger<PalmistryController> _logger;

        public PalmistryController(ILogger<PalmistryController> logger)
        {
           _logger = logger;
        }
        public IActionResult Post([FromBody]PalmFutureTellingModel model)
        {
             if (model != null)
             {
                 var starTime = DateTime.Now;
                _logger.LogInformation($"PALM START AT {starTime}");
                var isRightHand = model.RightHandImage != null;
                 string handFileName = "source.png";
                 var userTempFolder = Guid.NewGuid().ToString();
                 var userTempFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", userTempFolder);
                 Directory.CreateDirectory(userTempFolderPath);
                 string imagePath = "";
                if (isRightHand)
                {
                    var rightImage = System.Convert.FromBase64String(model.RightHandImage.Base64File);
                    imagePath = Path.Combine(userTempFolderPath, handFileName);
                    System.IO.File.WriteAllBytes(imagePath, rightImage);
                }
                 else
                {
                    var leftImage = System.Convert.FromBase64String(model.LeftHandImage.Base64File);
                    imagePath = Path.Combine(userTempFolderPath, handFileName);
                    System.IO.File.WriteAllBytes(imagePath, leftImage);
                }

                 _logger.LogInformation($"FILES SAVED AT {DateTime.Now - starTime}");
                var entryPoint = new PalmistryRecognitionEntryPoint(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"), imagePath, userTempFolderPath, isRightHand);
                var response = entryPoint.GetRecognitionResults();

                 _logger.LogInformation($"PALM END AT {DateTime.Now}");
                 _logger.LogInformation($"duration {DateTime.Now - starTime}");
                return Ok(response);
            }
            else
                return BadRequest();
            //var img5 = new Mat(path, ImreadModes.GrayScale);
            //img5.SaveImage(path);
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(path, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return File(memory, "png", Path.GetFileName(path));
        }
    }
}
