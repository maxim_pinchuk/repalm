﻿using System;
using System.IO;

namespace RePalm.Shared.Models
{
    public class UploadImageModel
    {
        public string Base64File { get; set; }
        public string FileName { get; set; }
    }
}
