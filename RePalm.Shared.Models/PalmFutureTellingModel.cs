﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RePalm.Shared.Models
{
    public class PalmFutureTellingModel
    {
        public UploadImageModel LeftHandImage { get; set; }
        public UploadImageModel RightHandImage { get; set; }

    }
}
